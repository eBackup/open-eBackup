# 主动依赖pydantic
pydantic==v1.10.5

# 主动依赖PyJWT
PyJWT==2.4.0

# 主动依赖pexpect
pexpect==4.9
ptyprocess==0.7.0

# 主动依赖cryptography
cryptography==39.0.1
cffi==1.15.0
pycparser==2.21

# 主动依赖gevent
gevent==22.10.2
setuptools==65.6.3
zope.event==4.5.0
zope.interface==5.4.0

# 主动依赖kubernetes
kubernetes==18.20.0
google-auth==2.6.6
cachetools==5.1.0
pyasn1-modules==0.2.8
pyasn1==0.4.8
rsa==4.8
python-dateutil==2.8.2
requests-oauthlib==1.3.1
oauthlib==3.2.2
websocket-client==1.3.2

# 主动依赖connexion
connexion==2.14.2
importlib-metadata==6.7.0
MarkupSafe==2.1.3
Werkzeug==2.2.3
clickclick==20.10.2
click==8.1.3
zipp==3.15.0
itsdangerous==2.1.2
Jinja2==3.1.2
inflection==0.5.1
jsonschema==4.17.3
attrs==23.1.0
importlib-resources==5.7.1
pyrsistent==0.19.3
typing-extensions==4.2.0
openapi-spec-validator==0.3.1
openapi-schema-validator==0.2.3
six==1.16.0
certifi==2023.5.7
urllib3==2.0.3
pkgutil-resolve-name==1.3.10
packaging==23.1

# 主动依赖PyYAML
PyYAML==6.0.1

# 主动依赖flask
flask==2.2.5

# 主动依赖requests
requests==2.31.0
idna==3.4
charset-normalizer==3.1.0