<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Step 2: Adding a Storage Device">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="nas_s_0011.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="nas_s_0013">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Step 2: Adding a Storage Device</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="nas_s_0013"></a><a name="nas_s_0013"></a>
  <h1 class="topictitle1">Step 2: Adding a Storage Device</h1>
  <div>
   <p>Before backing up or restoring a NAS file system, add a storage device where the NAS file system to be protected resides to the <span>product</span>.</p>
   <div class="section">
    <h4 class="sectiontitle">Context</h4>
    <p>The <span>product</span> can protect a NAS file system on the following storage devices:</p>
    <ul id="nas_s_0013__ul1969142191214">
     <li id="nas_s_0013__li3415133617174">OceanStor Dorado V7</li>
     <li id="nas_s_0013__li1739551514266">OceanStor V7</li>
     <li id="nas_s_0013__li984654311123">OceanStor Dorado 6.x</li>
     <li id="nas_s_0013__li151737411958">OceanStor 6.x</li>
     <li id="nas_s_0013__li72988389413">OceanProtect 1.3.0 and later versions</li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Precautions</h4>
    <ul>
     <li>If a storage device is earlier than OceanStor Dorado 6.1.3, a NAS file system cannot be discovered or protected by adding the storage device. Instead, a NAS share needs to be registered to protect the NAS file system. For details about how to register a NAS share, see <a href="nas_s_0024.html">Step 5: Registering a NAS Share (Applicable to Storage Devices Except OceanStor V5/OceanStor Pacific/NetApp ONTAP)</a>.</li>
     <li>When a storage device is OceanStor Dorado 6.1.3 or later, or OceanStor 6.1.3 or later, ensure that the activated license of the storage device contains "HyperReplication".</li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li id="nas_s_0013__li0198134611381"><span>Choose <span class="uicontrol" id="nas_s_0013__en-us_topic_0000001839142377_uicontrol14672036027"><b><span id="nas_s_0013__en-us_topic_0000001839142377_text19671936024"><strong>Protection</strong></span> &gt; File Systems &gt; Storage Devices</b></span>.</span><p></p>
      <div class="note" id="nas_s_0013__en-us_topic_0000001839142377_note172851315173">
       <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
       <div class="notebody">
        <p id="nas_s_0013__en-us_topic_0000001839142377_p77283132179">For 1.5.0, choose <span class="uicontrol" id="nas_s_0013__en-us_topic_0000001839142377_uicontrol13191103461714"><b><span id="nas_s_0013__en-us_topic_0000001839142377_text121911134171715"><strong>Protection</strong></span> &gt; File Services &gt; Storage Device</b></span>.</p>
       </div>
      </div> <p></p></li>
     <li id="nas_s_0013__li17571149114819"><span>Click <span class="uicontrol" id="nas_s_0013__uicontrol264533912494"><b><span id="nas_s_0013__text6739926144512"><strong>Add Device</strong></span></b></span> to add a storage device.</span></li>
     <li><span>Select a device type.</span><p></p>
      <ul id="nas_s_0013__ul10900122216496">
       <li id="nas_s_0013__nas_s_0013_li3415133617174">OceanStor Dorado V7</li>
       <li id="nas_s_0013__nas_s_0013_li1739551514266">OceanStor V7</li>
       <li id="nas_s_0013__nas_s_0013_li984654311123">OceanStor Dorado 6.x</li>
       <li id="nas_s_0013__nas_s_0013_li151737411958">OceanStor 6.x</li>
       <li id="nas_s_0013__nas_s_0013_li72988389413">OceanProtect 1.3.0 and later versions</li>
      </ul> <p></p></li>
     <li><span>Configure parameters as required. <a href="#nas_s_0013__table20829151954610">Table 1</a> describes the related parameters.</span><p></p>
      <div class="tablenoborder">
       <a name="nas_s_0013__table20829151954610"></a><a name="table20829151954610"></a>
       <table cellpadding="4" cellspacing="0" summary="" id="nas_s_0013__table20829151954610" frame="border" border="1" rules="all">
        <caption>
         <b>Table 1 </b>Parameters for adding a device
        </caption>
        <colgroup>
         <col style="width:25.629999999999995%">
         <col style="width:74.37%">
        </colgroup>
        <thead align="left">
         <tr>
          <th align="left" class="cellrowborder" valign="top" width="25.629999999999995%" id="mcps1.3.4.2.4.2.1.2.3.1.1"><p>Parameter</p></th>
          <th align="left" class="cellrowborder" valign="top" width="74.37%" id="mcps1.3.4.2.4.2.1.2.3.1.2"><p>Description</p></th>
         </tr>
        </thead>
        <tbody>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.4.2.4.2.1.2.3.1.1 "><p><span><strong>Device Name</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.4.2.4.2.1.2.3.1.2 "><p>Name of a user-defined device.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.4.2.4.2.1.2.3.1.1 "><p><span><strong>IP Address</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.4.2.4.2.1.2.3.1.2 "><p>Management IP address of the storage device.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.4.2.4.2.1.2.3.1.1 "><p><span><strong>Port</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.4.2.4.2.1.2.3.1.2 "><p>Number of the port for communication between the <span>product</span> and the management plane of the storage device.</p> <p>The default port number is <strong>8088</strong>.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.4.2.4.2.1.2.3.1.1 "><p><span><strong>Username</strong></span></p></td>
          <td class="cellrowborder" rowspan="2" valign="top" width="74.37%" headers="mcps1.3.4.2.4.2.1.2.3.1.2 "><p id="nas_s_0013__p16707151151">Username and password of a storage device administrator. Ensure that the RESTful login mode is available for the user and the password has been initialized.</p>
           <div class="note" id="nas_s_0013__note6722104712910">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p id="nas_s_0013__p1722194714299">Use an administrator account. Otherwise, resources on the device may fail to be backed up.</p>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.4.2.1.2.3.1.1 "><p><span><strong>Password</strong></span></p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.4.2.4.2.1.2.3.1.1 "><p><span><strong>Certificate verification</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.4.2.4.2.1.2.3.1.2 "><p id="nas_s_0013__p14829151914610">Enabled by default. Import the storage device certificate obtained in <a href="nas_s_0012.html">Step 1: Obtaining the CA Certificate of a Storage Device</a> to ensure the security of interactions between the <span id="nas_s_0013__text1420811157135">product</span> and storage device. After this function is disabled, security risks may exist in the communication between the system and the storage device. Exercise caution when performing this operation.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.4.2.4.2.1.2.3.1.1 "><p><strong>Specify Replication Ports</strong></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.4.2.4.2.1.2.3.1.2 "><p>This function is disabled by default. When it is enabled, you can select logical replication ports for NAS file system backup. If HyperMetro is configured for the storage device, this function must be enabled and logical ports that are being used by HyperMetro cannot be selected. Otherwise, NAS file system backup will fail.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.4.2.4.2.1.2.3.1.1 "><p><strong id="nas_s_0013__en-us_topic_0000001607702602_b255338171611">Select Replication Ports</strong></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.4.2.4.2.1.2.3.1.2 "><p>If <strong id="nas_s_0013__en-us_topic_0000001607702602_b13992311191812">Specify Replication Ports</strong> is enabled, you can select a maximum of eight replication logical ports per controller for NAS file system backup.</p></td>
         </tr>
        </tbody>
       </table>
      </div> <p></p></li>
     <li><span>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</span><p></p><p>When a storage device is added successfully, information about the added storage device is displayed on the <span class="uicontrol"><b><span><strong>Storage Device</strong></span></b></span> page.</p> <p>The <span>product</span> automatically scans NAS file systems in the storage device and displays them on the <span class="uicontrol"><b><span id="nas_s_0013__en-us_topic_0000001839142377_text847555644011"><strong>Protection</strong></span> &gt; File Systems &gt; NAS File Systems</b></span> page. For 1.5.0, the information is displayed on the <span class="uicontrol"><b><span id="nas_s_0013__en-us_topic_0000001839142377_text51105144188"><strong>Protection</strong></span> &gt; File Services &gt; NAS File Systems</b></span> page. If a file system is added to the registered device, locate the row of the target device on the <strong>Storage Devices</strong> page, and choose <span><strong>More</strong></span> &gt; <span><strong>Resource Scan</strong></span> to update the file system resources.</p> <p></p></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="nas_s_0011.html">Backing Up a NAS File System</a>
    </div>
   </div>
  </div>
 </body>
</html>