<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Managing Clusters">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="kubernetes_gud_00058.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="kubernetes_gud_00060">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Managing Clusters</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="kubernetes_gud_00060"></a><a name="kubernetes_gud_00060"></a>

<h1 class="topictitle1">Managing Clusters</h1>
<div><p>After a Kubernetes cluster is registered with the <span>OceanProtect</span>, you can perform related operations on the Kubernetes cluster.</p>
<div class="section"><h4 class="sectiontitle">Related Operations</h4><p>You need to log in to the WebUI and choose <span class="uicontrol"><b><span id="kubernetes_gud_00060__en-us_topic_0000001839142377_text194759416387"><strong>Protection</strong></span> &gt; Containers &gt; Kubernetes FlexVolume</b></span> to go to the <span><strong>Clusters</strong></span> tab page.</p>
<div class="p"><a href="#kubernetes_gud_00060__table51069596392">Table 1</a> describes the related operations.
<div class="tablenoborder"><a name="kubernetes_gud_00060__table51069596392"></a><a name="table51069596392"></a><table cellpadding="4" cellspacing="0" summary="" id="kubernetes_gud_00060__table51069596392" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Cluster-related operations</caption><colgroup><col style="width:21.279999999999998%"><col style="width:47.27%"><col style="width:31.45%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="21.279999999999998%" id="mcps1.3.2.3.2.2.4.1.1"><p>Operation</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="47.27%" id="mcps1.3.2.3.2.2.4.1.2"><p>Description</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="31.45%" id="mcps1.3.2.3.2.2.4.1.3"><p>Navigation Path</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="21.279999999999998%" headers="mcps1.3.2.3.2.2.4.1.1 "><p>Scanning for cluster resources</p>
</td>
<td class="cellrowborder" valign="top" width="47.27%" headers="mcps1.3.2.3.2.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>The <span>OceanProtect</span> automatically scans Kubernetes clusters every hour. If namespaces or StatefulSets in a registered Kubernetes cluster have changed, the <span>OceanProtect</span> automatically updates the namespaces or StatefulSets after scanning. This operation enables you to immediately update changes to the <span>OceanProtect</span>.</p>
<p><strong>Note</strong></p>
<p>If you do not select to apply SLA policies to newly created StatefulSets when creating a periodic backup job, the system will not associate the newly discovered StatefulSets with an SLA after scanning.</p>
</td>
<td class="cellrowborder" valign="top" width="31.45%" headers="mcps1.3.2.3.2.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Resource Scan</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.279999999999998%" headers="mcps1.3.2.3.2.2.4.1.1 "><p>Testing cluster connectivity</p>
</td>
<td class="cellrowborder" valign="top" width="47.27%" headers="mcps1.3.2.3.2.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>Manual connectivity test of the registered Kubernetes cluster is used to check the connectivity between the cluster and the agent host and between the cluster and the production environment.</p>
</td>
<td class="cellrowborder" valign="top" width="31.45%" headers="mcps1.3.2.3.2.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Test Connectivity</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.279999999999998%" headers="mcps1.3.2.3.2.2.4.1.1 "><div class="p">Authorizing resources<div class="note" id="kubernetes_gud_00060__en-us_topic_0000001839223137_note1819152144411"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="kubernetes_gud_00060__en-us_topic_0000001839223137_p72172194413">Only version 1.5.0 supports this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="47.27%" headers="mcps1.3.2.3.2.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>By default, only the system administrator has the permission to protect resources. If you want to assign this permission to a data protection administrator, authorize the data protection administrator as the system administrator to protect resources.</p>
<p><strong>Note</strong></p>
<ul><li id="kubernetes_gud_00060__en-us_topic_0000001839223137_li1491782615119">The protection permissions on a resource that has been associated with an SLA cannot be granted. Before permission granting, remove the protection.</li><li id="kubernetes_gud_00060__en-us_topic_0000001839223137_li14274329155112">The protection permissions on a single resource can be granted only to a single data protection administrator.</li></ul>
</td>
<td class="cellrowborder" valign="top" width="31.45%" headers="mcps1.3.2.3.2.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Authorize Resource</strong></span></b></span> and select a data protection administrator you want to authorize.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.279999999999998%" headers="mcps1.3.2.3.2.2.4.1.1 "><div class="p">Reclaiming resources<div class="note" id="kubernetes_gud_00060__en-us_topic_0000001839223137_note9135337194416"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="kubernetes_gud_00060__en-us_topic_0000001839223137_en-us_topic_0000001839223137_p72172194413">Only version 1.5.0 supports this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="47.27%" headers="mcps1.3.2.3.2.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to reclaim resources authorized to the data protection administrator. The data protection administration is not permitted to protect the resources that have been reclaimed.</p>
<p><strong>Note</strong></p>
<p>The protection permissions on a resource that has been associated with an SLA cannot be reclaimed. Before reclamation, remove the protection.</p>
</td>
<td class="cellrowborder" valign="top" width="31.45%" headers="mcps1.3.2.3.2.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Reclaim Resource</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.279999999999998%" headers="mcps1.3.2.3.2.2.4.1.1 "><p>Modifying the cluster registration information</p>
</td>
<td class="cellrowborder" valign="top" width="47.27%" headers="mcps1.3.2.3.2.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to change the registered Kubernetes cluster name or to synchronize the changes of the registered Kubernetes configuration file, management IP address of storage resources, username, and password.</p>
<p><strong>Note</strong></p>
<p>Modify the registration information when no backup or restoration job is running. Otherwise, the job will fail.</p>
</td>
<td class="cellrowborder" valign="top" width="31.45%" headers="mcps1.3.2.3.2.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify</strong></span></b></span>.</p>
<p><a href="kubernetes_gud_00012.html#kubernetes_gud_00012__table613227155112">Table 1</a> describes the parameters of Kubernetes cluster registration information.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.279999999999998%" headers="mcps1.3.2.3.2.2.4.1.1 "><p>Deleting a cluster</p>
</td>
<td class="cellrowborder" valign="top" width="47.27%" headers="mcps1.3.2.3.2.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>If you no longer need to protect a Kubernetes cluster or use it as a restore target, you can perform this operation to delete the Kubernetes cluster.</p>
<p><strong>Note</strong></p>
<p>If a namespace or StatefulSet in a cluster has been associated with an SLA, the cluster cannot be deleted. Disassociate the namespace or StatefulSet from the SLA before deletion.</p>
</td>
<td class="cellrowborder" valign="top" width="31.45%" headers="mcps1.3.2.3.2.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Delete</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.279999999999998%" headers="mcps1.3.2.3.2.2.4.1.1 "><div class="p">Adding a tag<div class="note" id="kubernetes_gud_00060__en-us_topic_0000001792344090_note161679211561"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="kubernetes_gud_00060__en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="47.27%" headers="mcps1.3.2.3.2.2.4.1.2 "><p><strong id="kubernetes_gud_00060__en-us_topic_0000001792344090_b1495945913127">Scenario</strong></p>
<p>This operation enables you to quickly filter and manage resources.</p>
</td>
<td class="cellrowborder" valign="top" width="31.45%" headers="mcps1.3.2.3.2.2.4.1.3 "><div class="p">Choose <span class="uicontrol" id="kubernetes_gud_00060__en-us_topic_0000001792344090_uicontrol84421111904"><b><span id="kubernetes_gud_00060__en-us_topic_0000001792344090_text6442710012"><strong>More</strong></span> &gt; Add Tag</b></span>.<div class="note" id="kubernetes_gud_00060__en-us_topic_0000001792344090_note164681316118"><span class="notetitle"> NOTE: </span><div class="notebody"><ul id="kubernetes_gud_00060__en-us_topic_0000001792344090_ul22969251338"><li id="kubernetes_gud_00060__en-us_topic_0000001792344090_li142961925139">On the displayed <strong id="kubernetes_gud_00060__en-us_topic_0000001792344090_b171967427617">Add Tag</strong> dialog box, you can select existing tags or click <strong id="kubernetes_gud_00060__en-us_topic_0000001792344090_b1711194810818">Create</strong> to create a tag.</li><li id="kubernetes_gud_00060__en-us_topic_0000001792344090_li135161927738">To modify or delete a tag, click <strong id="kubernetes_gud_00060__en-us_topic_0000001792344090_b17675195912164">Go to Tag Management</strong> on the <strong id="kubernetes_gud_00060__en-us_topic_0000001792344090_b13536183171711">Add Tag</strong> page to go to the <strong id="kubernetes_gud_00060__en-us_topic_0000001792344090_b1386161351713">Tag Management</strong> page.</li></ul>
</div></div>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.279999999999998%" headers="mcps1.3.2.3.2.2.4.1.1 "><div class="p">Removing a tag<div class="note" id="kubernetes_gud_00060__en-us_topic_0000001792344090_note4957114117575"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="kubernetes_gud_00060__en-us_topic_0000001792344090_en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="47.27%" headers="mcps1.3.2.3.2.2.4.1.2 "><p><strong id="kubernetes_gud_00060__en-us_topic_0000001792344090_b472911291311">Scenario</strong></p>
<p>This operation enables you to remove the tag of a resource when it is no longer needed.</p>
</td>
<td class="cellrowborder" valign="top" width="31.45%" headers="mcps1.3.2.3.2.2.4.1.3 "><p>Choose <span class="uicontrol" id="kubernetes_gud_00060__en-us_topic_0000001792344090_uicontrol4993205113111"><b><span id="kubernetes_gud_00060__en-us_topic_0000001792344090_text6993751201118"><strong>More</strong></span> &gt; Remove Tag</b></span>.</p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="kubernetes_gud_00058.html">Cluster, Namespace, and StatefulSet</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>