<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Step 1: Configuring the XBSA Library Path">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="informix-0010.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="informix-0011">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Step 1: Configuring the XBSA Library Path</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="informix-0011"></a><a name="informix-0011"></a>

<h1 class="topictitle1">Step 1: Configuring the XBSA Library Path</h1>
<div><p>Before backing up the Informix/GBase 8s database, you need to specify the path and file name of the XBSA shared library.</p>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li><span>Use PuTTY to log in to the host where the Informix/GBase 8s database is located as user <strong>root</strong>.</span></li><li><span>For the Informix database, run the <strong>su - informix</strong> command to switch to the Informix database user. For the GBase 8s database, run the <strong>su - gbasedbt</strong> command to switch to the GBase 8s database user.</span></li><li><span>Run the following command to check whether the instance is configured:</span><p><pre class="screen">cat /<em>Database installation directory</em>/etc/onconfig |grep ^BAR_BSALIB_PATH</pre>
<p>If the following information is displayed, the instance has been configured and no further action is required. Otherwise, go to the next step to configure the instance.</p>
<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p>Replace <strong>onconfig</strong> with the actual name of the instance configuration file.</p>
</div></div>
<p>For 1.5.0, information similar to the following is displayed:</p>
<pre class="screen">BAR_BSALIB_PATH /usr/openv/lib/libxbsa64iif.so</pre>
<p>For 1.6.0 and later versions, information similar to the following is displayed:</p>
<pre class="screen">BAR_BSALIB_PATH /<em>Parent directory where the agent is installed</em>/DataBackup/ProtectClient/interfaces/xbsa/lib/libxbsa64iif.so</pre>
</p></li><li><span>Run the following command to open the instance configuration file and configure the database instance:</span><p><pre class="screen">vi /<em>Database installation directory</em>/etc/onconfig</pre>
</p></li><li><span>Change the value of <em>BAR_BSALIB_PATH</em>.</span><p><ul><li>For 1.5.0, change the value of <em>BAR_BSALIB_PATH</em> to <strong>/usr/openv/lib/libxbsa64iif.so</strong>.<p>Example: <strong>BAR_BSALIB_PATH /usr/openv/lib/libxbsa64iif.so</strong>.</p>
</li><li>For 1.6.0 and later versions, change the value of <em>BAR_BSALIB_PATH</em> to <strong>/</strong><em>Parent directory for installing the agent</em><strong>/DataBackup/ProtectClient/interfaces/xbsa/lib/libxbsa64iif.so</strong>.<p>Example: <strong>BAR_BSALIB_PATH /opt/DataBackup/ProtectClient/interfaces/xbsa/lib/libxbsa64iif.so</strong>.</p>
</li></ul>
</p></li><li><span>For the GBase 8s database, perform this step. Otherwise, skip this step.</span><p><ol type="a"><li>Run the following command to view the server ID:<pre class="screen">cat /<em>Database installation directory</em>/etc/onconfig |grep SERVERNUM</pre>
<p><span><img src="en-us_image_0000002121246957.png"></span></p>
<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p>Replace <strong>onconfig</strong> with the actual name of the instance configuration file.</p>
</div></div>
</li><li id="informix-0011__li68634264103"><a name="informix-0011__li68634264103"></a><a name="li68634264103"></a>Run the following command to check whether the instance is configured:<pre class="screen">cat /<em>Database installation directory</em>/etc/onconfig |grep ^BAR_IXBAR_PATH</pre>
<p>If the following information is displayed, the instance has been configured and no further action is required. Otherwise, go to the next step to configure the instance.</p>
<pre class="screen">BAR_IXBSA_PATH /<em>Database installation directory</em>/backups/ixbar.<em>Server ID</em></pre>
</li><li>Run the following command to configure the database instance:<pre class="screen">mkdir /<em>Database installation directory</em>/backups; onmode -wf BAR_IXBAR_PATH=/<em>Database installation directory</em>/backups/ixbar.<em>Server ID</em></pre>
</li><li>Perform <a href="#informix-0011__li68634264103">6.b</a> to verify that the instance is successfully configured.</li></ol>
</p></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="informix-0010.html">Backing Up Informix/GBase 8s</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>