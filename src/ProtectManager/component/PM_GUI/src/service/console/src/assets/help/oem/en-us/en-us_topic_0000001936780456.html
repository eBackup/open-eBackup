<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="How Do I Install the OceanStor DataTurbo Client?">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="EN-US_TOPIC_0000001936780456">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>How Do I Install the OceanStor DataTurbo Client?</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="EN-US_TOPIC_0000001936780456"></a><a name="EN-US_TOPIC_0000001936780456"></a>
  <h1 class="topictitle1">How Do I Install the OceanStor DataTurbo Client?</h1>
  <div id="body0000001936780456">
   <p id="EN-US_TOPIC_0000001936780456__p1524212155175">If the source deduplication function needs to be enabled after ProtectAgent is installed, you can refer to this section to install the OceanStor DataTurbo client and enable the source deduplication function.</p>
   <div class="section" id="EN-US_TOPIC_0000001936780456__section95474531308">
    <h4 class="sectiontitle">Prerequisites</h4>
    <ul id="EN-US_TOPIC_0000001936780456__ul64861219103114">
     <li id="EN-US_TOPIC_0000001936780456__li14138142963115">Ensure that the free memory of the agent host is greater than 2 GB.</li>
     <li id="EN-US_TOPIC_0000001936780456__li54865196319">Ensure that the OS of the agent host supports source deduplication. For details about the OSs that support source deduplication, refer to <a href="https://info.support.huawei.com/storage/comp/#/oceanprotect" target="_blank" rel="noopener noreferrer">Compatibility Query Tool</a>.</li>
    </ul>
   </div>
   <div class="section" id="EN-US_TOPIC_0000001936780456__section154841528202212">
    <h4 class="sectiontitle">Procedure</h4>
    <ol id="EN-US_TOPIC_0000001936780456__ol580681671713">
     <li id="EN-US_TOPIC_0000001936780456__li6806121618171"><span>Download the ProtectAgent software package. For details, see <a href="en-us_topic_0000001839189405.html">Downloading the ProtectAgent Software Package</a>.</span></li>
     <li id="EN-US_TOPIC_0000001936780456__li1848191511184"><span>Open the downloaded ProtectAgent software package and obtain the OceanStor DataTurbo client installation package <strong id="EN-US_TOPIC_0000001936780456__b5402165894116">dataturbo.zip</strong> from the <strong id="EN-US_TOPIC_0000001936780456__b139388211488">..\DataProtect_</strong><em id="EN-US_TOPIC_0000001936780456__i13411121104117">XXX</em><strong id="EN-US_TOPIC_0000001936780456__b645711674816">_client_general_</strong><em id="EN-US_TOPIC_0000001936780456__i6918552184016">XXX</em><strong id="EN-US_TOPIC_0000001936780456__b166891910174814">.zip\DataProtect_</strong><em id="EN-US_TOPIC_0000001936780456__i119727144114">XXX</em><strong id="EN-US_TOPIC_0000001936780456__b27461414174812">_client_general_</strong><em id="EN-US_TOPIC_0000001936780456__i613111154114">XXX</em><strong id="EN-US_TOPIC_0000001936780456__b1466531711481">\third_party_software</strong> directory.</span><p></p>
      <div class="note" id="EN-US_TOPIC_0000001936780456__note131823521140">
       <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
       <div class="notebody">
        <p id="EN-US_TOPIC_0000001936780456__en-us_topic_0000001839189365_p1818285216413">The name of the ProtectAgent software package varies depending on the version, agent type, and OS.</p>
       </div>
      </div> <p></p></li>
     <li id="EN-US_TOPIC_0000001936780456__li0981442134217"><span>Upload the OceanStor DataTurbo client installation package to the agent host.</span></li>
     <li id="EN-US_TOPIC_0000001936780456__li194074021818"><span>Install the OceanStor DataTurbo client and start the service. For details, see "Installation" and "Starting the Service" in the <a href="https://support.huawei.com/enterprise/en/flash-storage/oceanprotect-x8000-pid-251807257?category=configuration-commissioning&amp;subcategory=configuration-guide" target="_blank" rel="noopener noreferrer">SourceDedupe User Guide</a> of the corresponding version.</span><p></p>
      <div class="note" id="EN-US_TOPIC_0000001936780456__note1481014881116">
       <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
       <div class="notebody">
        <ul id="EN-US_TOPIC_0000001936780456__ul1372552715717">
         <li id="EN-US_TOPIC_0000001936780456__li172522714573">For the Linux OS, <span id="EN-US_TOPIC_0000001936780456__ph4509191324018">after source deduplication is enabled, add the source deduplication user to <strong id="EN-US_TOPIC_0000001936780456__b2028692319361">sudoers</strong> before the first backup. For details, see <a href="en-us_topic_0000001792390228.html">How Can I Add a Source Deduplication User to sudoers? (Applicable to the Linux OS)</a>.</span></li>
         <li id="EN-US_TOPIC_0000001936780456__li6345154135720">For the Windows OS, <span id="EN-US_TOPIC_0000001936780456__ph154629106403">after source deduplication is enabled, add the SID (S-1-5-18) of user <strong id="EN-US_TOPIC_0000001936780456__b360104212442">system</strong> to the <span class="filepath" id="EN-US_TOPIC_0000001936780456__filepath8365201855911"><b>...\oceanstor\dataturbo\conf \whitelist</b></span> whitelist and run the <strong id="EN-US_TOPIC_0000001936780456__en-us_topic_0000001607704830_b16198172917816">sc stop dataturbo</strong> and <strong id="EN-US_TOPIC_0000001936780456__b1516780194917">sc start dataturbo</strong> commands in sequence to restart the DataTurbo service.</span></li>
        </ul>
       </div>
      </div> <p></p></li>
    </ol>
   </div>
  </div>
 </body>
</html>