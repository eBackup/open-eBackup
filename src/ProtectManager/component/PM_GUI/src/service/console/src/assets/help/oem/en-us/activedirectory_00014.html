<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Step 5: Performing Backup">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="activedirectory_00010.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="activedirectory_00014">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Step 5: Performing Backup</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="activedirectory_00014"></a><a name="activedirectory_00014"></a>
  <h1 class="topictitle1">Step 5: Performing Backup</h1>
  <div>
   <p>Before performing backup, you need to associate the resources to be protected with a specified SLA. The system protects the resources and periodically executes backup jobs based on the SLA. You can perform a backup job immediately through manual backup.</p>
   <div class="section">
    <h4 class="sectiontitle">Context</h4>
    <ul>
     <li>During manual backup, the parameters (except <span class="uicontrol"><b><span><strong>Automatic Retry</strong></span></b></span>) defined in the SLA, such as <span class="uicontrol"><b><span><strong>Rate Limiting Policies</strong></span></b></span>, are applied to the manual backup job.</li>
     <li>Copies generated by manual backup are retained for the duration defined in the SLA.</li>
     <li>If a replication policy has been defined in the SLA, the <span>product</span> will perform replication once based on the SLA when you perform manual backup.</li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Prerequisites</h4>
    <p>Before the backup, ensure that the Active Directory service is running properly.</p>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li><span>Choose <span class="uicontrol" id="activedirectory_00014__en-us_topic_0000001839142377_uicontrol676612011546"><b><span id="activedirectory_00014__en-us_topic_0000001839142377_text1876611013540"><strong>Protection</strong></span> &gt; Applications &gt; Active Directory</b></span>.</span></li>
     <li><span>Select the Active Directory domain controller to be protected and click <span class="uicontrol"><b><span><strong>Protect</strong></span></b></span>. </span><p></p><p>You can also select multiple Active Directory domain controllers for batch protection.</p> <p></p></li>
     <li><span>Select an SLA. </span><p></p><p>You can also click <span class="uicontrol"><b><span><strong>Create</strong></span></b></span> to create an SLA.</p> <p></p></li>
     <li><span>Configure advanced parameters. </span><p></p>
      <div class="p">
       <a href="#activedirectory_00014__en-us_topic_0000001349147581_en-us_topic_0000001263775382_table354613271506">Table 1</a> describes the related parameters. 
       <div class="tablenoborder">
        <a name="activedirectory_00014__en-us_topic_0000001349147581_en-us_topic_0000001263775382_table354613271506"></a><a name="en-us_topic_0000001349147581_en-us_topic_0000001263775382_table354613271506"></a>
        <table cellpadding="4" cellspacing="0" summary="" id="activedirectory_00014__en-us_topic_0000001349147581_en-us_topic_0000001263775382_table354613271506" frame="border" border="1" rules="all">
         <caption>
          <b>Table 1 </b>Advanced parameters of resource protection
         </caption>
         <colgroup>
          <col style="width:23.400000000000002%">
          <col style="width:76.6%">
         </colgroup>
         <thead align="left">
          <tr>
           <th align="left" class="cellrowborder" valign="top" width="23.400000000000002%" id="mcps1.3.4.2.4.2.1.2.2.3.1.1"><p>Parameter</p></th>
           <th align="left" class="cellrowborder" valign="top" width="76.6%" id="mcps1.3.4.2.4.2.1.2.2.3.1.2"><p>Description</p></th>
          </tr>
         </thead>
         <tbody>
          <tr>
           <td class="cellrowborder" valign="top" width="23.400000000000002%" headers="mcps1.3.4.2.4.2.1.2.2.3.1.1 "><p>Object-Level Backup</p></td>
           <td class="cellrowborder" valign="top" width="76.6%" headers="mcps1.3.4.2.4.2.1.2.2.3.1.2 "><p>This function is enabled by default. After this function is enabled, the generated backup copies can be used for object-level restoration.</p></td>
          </tr>
         </tbody>
        </table>
       </div>
      </div> <p></p></li>
     <li><span>Click <span class="uicontrol" id="activedirectory_00014__en-us_topic_0000001964713008_uicontrol23691650677"><b>OK</b></span>.</span><p></p><p id="activedirectory_00014__en-us_topic_0000001964713008_en-us_topic_0000001656691397_p25041111535">If the current system time is later than the start time of the first backup specified in the SLA, you can select <strong id="activedirectory_00014__en-us_topic_0000001964713008_b9624185216713">Execute manual backup now</strong> in the dialog box that is displayed or choose to perform automatic backup periodically based on the backup policy set in the SLA.</p> <p></p></li>
     <li><strong>Optional: </strong><span>Perform manual backup.</span><p></p><p>If you want to execute a backup job immediately, perform manual backup through the following operations. Otherwise, skip this step.</p>
      <ol type="a">
       <li>In the row that contains the Active Directory domain controller you want to back up, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Manual Backup</strong></span></b></span>.
        <div class="note">
         <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
         <div class="notebody">
          <p>You can select multiple Active Directory domain controllers for manual backup in batches. Specifically, select multiple Active Directory domain controllers and choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Manual Backup</strong></span></b></span> in the upper left corner of the page.</p>
         </div>
        </div></li>
       <li>Set the name of the manually generated backup.<p id="activedirectory_00014__en-us_topic_0000001792503838_p444210492470">If this parameter is left unspecified, the system sets the copy name to <strong id="activedirectory_00014__en-us_topic_0000001792503838_b1469854421614">backup_</strong><em id="activedirectory_00014__en-us_topic_0000001792503838_i14698444161618">Timestamp</em> by default.</p></li>
       <li>Select <span><strong>Full backup</strong></span> for <strong>Protection Policy</strong>.
        <div class="note">
         <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
         <div class="notebody">
          <p id="activedirectory_00014__en-us_topic_0000001964713008_p13522118194919"><span id="activedirectory_00014__en-us_topic_0000001964713008_ph19374310343">For 1.6.0 and later versions, if the selected protection policy is different from that configured in the associated SLA, the WORM configuration does not take effect.</span></p>
         </div>
        </div></li>
       <li>Click <span class="uicontrol"><b>OK</b></span>.</li>
      </ol> <p></p></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="activedirectory_00010.html">Backing Up Active Directory</a>
    </div>
   </div>
  </div>
 </body>
</html>