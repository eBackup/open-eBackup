<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Step 2: Obtaining the CA Certificate of the Storage Resource (Applicable to Snapshot-based Backup at the Storage Layer)">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="oracle_gud_0013.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="oracle_gud_ca">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Step 2: Obtaining the CA Certificate of the Storage Resource (Applicable to Snapshot-based Backup at the Storage Layer)</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="oracle_gud_ca"></a><a name="oracle_gud_ca"></a>
  <h1 class="topictitle1">Step 2: Obtaining the CA Certificate of the Storage Resource (Applicable to Snapshot-based Backup at the Storage Layer)</h1>
  <div>
   <p>For 1.6.0 and later versions, when adding a storage resource during registration of an Oracle database, you can import the CA certificate of the storage resource to improve access security between devices. Otherwise, the system cannot verify information about the accessed devices, causing security risks. This operation is optional.</p>
   <p>You can obtain the CA certificate from the administrator or download it by yourself.</p>
   <div class="section">
    <h4 class="sectiontitle">Downloading the CA Certificate of the Storage Resource</h4>
    <p>If OceanStor Dorado 6.1.5 or later is used as the production storage, the following methods cannot be used to obtain the CA certificate. For details about how to obtain the CA certificate, see "Issuing Certificates from the Built-in CA of the Storage System" in the <a href="https://support.huawei.com/enterprise/en/flash-storage/oceanstor-dorado-series-pid-262794301" target="_blank" rel="noopener noreferrer">OceanStor Dorado Product Documentation</a>.</p>
    <ul>
     <li id="oracle_gud_ca__hcs_gud_0012_li95354413619">Method 1:
      <ol id="oracle_gud_ca__hcs_gud_0012_ol069212181467">
       <li id="oracle_gud_ca__hcs_gud_0012_li46921618969">Log in to the <a href="https://support.huawei.com/pki" target="_blank" rel="noopener noreferrer">website</a> for downloading a PKI CA certificate.</li>
       <li id="oracle_gud_ca__hcs_gud_0012_li269291814611">Click <strong id="oracle_gud_ca__hcs_gud_0012_b89016591537">Level-2 CA</strong>.</li>
       <li id="oracle_gud_ca__hcs_gud_0012_li36921181162">Enter <strong id="oracle_gud_ca__hcs_gud_0012_b99915819418">Huawei IT Product CA</strong> in the search box and click <strong id="oracle_gud_ca__hcs_gud_0012_b194091613148">Search</strong>.</li>
       <li id="oracle_gud_ca__hcs_gud_0012_li36926181620">In the query result area, click <span><img id="oracle_gud_ca__hcs_gud_0012_image177413913493" src="en-us_image_0000001956171205.png"></span> in the row of the target <strong id="oracle_gud_ca__hcs_gud_0012_b36711810754">Huawei IT Product CA certificate</strong> to download the certificate and save it locally.
        <div class="note" id="oracle_gud_ca__hcs_gud_0012_note192173131862">
         <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
         <div class="notebody">
          <p id="oracle_gud_ca__hcs_gud_0012_p388713467614">The certificate file must be in .pem format. If the format of the saved certificate file does not meet the requirements, manually change the file name extension to .pem.</p>
         </div>
        </div></li>
      </ol></li>
     <li id="oracle_gud_ca__hcs_gud_0012_li14508185519517">Method 2:
      <div class="p" id="oracle_gud_ca__hcs_gud_0012_p104558181694">
       <a name="oracle_gud_ca__hcs_gud_0012_li14508185519517"></a><a name="hcs_gud_0012_li14508185519517"></a>The displayed information varies depending on the browser. The following uses Google Chrome 120 as an example.
       <ol id="oracle_gud_ca__hcs_gud_0012_ol245918511655">
        <li id="oracle_gud_ca__hcs_gud_0012_li94591551955">In the address box of Google Chrome, type the URL of DeviceManager of the storage device and press <strong id="oracle_gud_ca__hcs_gud_0012_b439014204619">Enter</strong>.</li>
        <li id="oracle_gud_ca__hcs_gud_0012_li8459451150">Click <strong id="oracle_gud_ca__hcs_gud_0012_b11879202513613">Not secure</strong> on the left of the URL address box, and click <strong id="oracle_gud_ca__hcs_gud_0012_b287910251269">Certificate is not valid</strong>.<p id="oracle_gud_ca__hcs_gud_0012_p6388181019412"><span><img id="oracle_gud_ca__hcs_gud_0012_image193883101346" src="en-us_image_0000001839274661.png"></span></p></li>
        <li id="oracle_gud_ca__hcs_gud_0012_li2459175117516">In the dialog box that is displayed, click the <strong id="oracle_gud_ca__hcs_gud_0012_b193986520307">Details</strong> tab and click <strong id="oracle_gud_ca__hcs_gud_0012_b8170165693014">Export</strong>.</li>
        <li id="oracle_gud_ca__hcs_gud_0012_li1945914516513">Export the certificate and rename the certificate file <em id="oracle_gud_ca__hcs_gud_0012_i74083585309">XXX</em><strong id="oracle_gud_ca__hcs_gud_0012_b1409558173016">.pem</strong> as prompted.
         <div class="note" id="oracle_gud_ca__hcs_gud_0012_note14297195512584">
          <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
          <div class="notebody">
           <p id="oracle_gud_ca__hcs_gud_0012_p52973551583"><em id="oracle_gud_ca__hcs_gud_0012_i3384141183120">XXX</em> indicates the actual certificate name. The certificate must be in <strong id="oracle_gud_ca__hcs_gud_0012_b93846111315">.pem</strong> format.</p>
          </div>
         </div></li>
       </ol>
      </div></li>
    </ul>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="oracle_gud_0013.html">Backing Up an Oracle Database</a>
    </div>
   </div>
  </div>
 </body>
</html>