<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Step 1: Initializing sys_rman">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="kingbase-00008.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="EN-US_TOPIC_0000002015631765">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Step 1: Initializing sys_rman</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="EN-US_TOPIC_0000002015631765"></a><a name="EN-US_TOPIC_0000002015631765"></a>

<h1 class="topictitle1">Step 1: Initializing sys_rman</h1>
<div><p>You need to initialize the <strong>sys_rman</strong> configuration after the system is upgraded from 1.5.0 to 1.6.0 or the first backup or active/standby switchover after restoration using a copy generated in 1.5.0. Otherwise, backup and restoration cannot be performed.</p>
<div class="section"><h4 class="sectiontitle">Procedure</h4><p><strong>Single-node system initialization</strong></p>
<ol><li><span>Log in to the host where the Kingbase instance is located.</span></li><li><span>Run the following command to check the installation directory and data directory of the Kingbase database:</span><p><pre class="screen">ps -ef | grep kingbase</pre>
<p>The following figure shows the command output.</p>
<p><span><img src="en-us_image_0000001979761660.png"></span></p>
</p></li><li><span>Modify the <strong>kingbase.conf</strong> file.</span><p><pre class="screen">vi /<em>Data directory</em>/kingbase.conf
archive_mode = on
archive_command = ''</pre>
</p></li><li><span>Run the following commands to reload the Kingbase configuration. <em>kingbase</em> indicates the name of the database installation user.</span><p><pre class="screen">su - <em>kingbase</em>
/<em>Installation directory</em>/bin/sys_ctl reload -D /<em>Data directory</em>
exit</pre>
</p></li><li><span>Run the following commands to configure the node IP address, database installation directory, and data directory in the <strong>bin/sys_backup.conf</strong> file:</span><p><pre class="screen">cp /<em>Installation directory</em>/share/sys_backup.conf /<em>Installation directory</em>/bin/sys_backup.conf
cd /<em>Installation directory</em>/bin
vi sys_backup.conf
_target_db_style="single"
_one_db_ip="<em>IP address of the current node</em>"
_repo_ip="<em>IP address of the current node</em>"
_single_data_dir="/<em>Data directory</em>"
_single_bin_dir="/<em>Installation directory</em>/bin"</pre>
</p></li><li><span>Run the following commands to complete the initialization. <em>kingbase</em> indicates the name of the database installation user.</span><p><pre class="screen">su - <em>kingbase</em>
cd /<em>Installation directory</em>/bin
./sys_backup.sh init</pre>
</p></li></ol>
<p><strong>Initializing a cluster</strong></p>
<ol><li><span>Log in to the host where the Kingbase instance is located.</span></li><li><span>Run the following command to check the installation directory and data directory of the Kingbase database:</span><p><pre class="screen">ps -ef | grep kingbase</pre>
<p>The following figure shows the command output.</p>
<p><span><img src="en-us_image_0000002016443885.png"></span></p>
</p></li><li><span>Run the following commands to modify the <strong>kingbase.conf</strong> file on the active/standby node:</span><p><pre class="screen">vi /<em>Data directory</em>/kingbase.conf
archive_mode = on
archive_command=''</pre>
</p></li><li><span>Run the following commands to reload the Kingbase configuration. <em>kingbase</em> indicates the name of the database installation user.</span><p><pre class="screen">su - <em>kingbase</em>
/<em>Installation directory</em>/bin/sys_ctl reload -D /<em>Data directory</em>
exit</pre>
</p></li><li><span>Run the following commands to configure the node IP address, database installation directory, and data directory in the <strong>bin/sys_backup.conf</strong> on the active and standby nodes:</span><p><pre class="screen">cp /<em>Installation directory</em>/share/sys_backup.conf /<em>Installation directory</em>/bin/sys_backup.conf
cd /<em>Installation directory</em>/bin
vi sys_backup.conf
_target_db_style="cluster"
_one_db_ip="<em>IP address of the current node</em>" 
_repo_ip="<em>IP address of the current node</em>"
_single_data_dir="/<em>Data directory</em>"
_single_bin_dir="/<em>Installation directory</em>/bin"
_use_scmd=off</pre>
<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p>Steps 1 to 5 must be performed on both the active and standby nodes.</p>
</div></div>
</p></li><li><span>Run the following commands on the repo node to complete the initialization. <em>kingbase</em> indicates the name of the database installation user.</span><p><pre class="screen">su - <em>kingbase</em>
cd /<em>Installation directory</em>/bin
./sys_backup.sh init</pre>
<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p>Step 6 must be performed on the active node.</p>
</div></div>
</p></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="kingbase-00008.html">Backing Up a Kingbase Instance</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>