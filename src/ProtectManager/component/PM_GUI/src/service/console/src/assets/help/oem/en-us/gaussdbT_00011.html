<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Step 1: Checking and Configuring the Database Environment">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="gaussdbT_00010.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="gaussdbT_00011">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Step 1: Checking and Configuring the Database Environment</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="gaussdbT_00011"></a><a name="gaussdbT_00011"></a>
  <h1 class="topictitle1">Step 1: Checking and Configuring the Database Environment</h1>
  <div>
   <p>Before performing backup and restoration, ensure that the GaussDB T database status is normal. This section describes how to check whether the database is normal.</p>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <p><strong>Checking the GaussDB T database cluster type status</strong></p>
    <ol>
     <li><span>Log in to the database host.</span><p></p><p>This section uses GaussDB T 1.2.1 as an example.</p> <p></p></li>
     <li><span>Run the following command to change to the database user, for example, user <strong>omm</strong>.</span><p></p><pre class="screen">su - omm</pre> <p></p></li>
     <li><span>Run the following command to check the database status:</span><p></p><pre class="screen">gs_om -t status</pre>
      <div class="p">
       If the command output is similar to the following example, the database status is normal.
       <pre class="screen">[omm@euler124 ~]$ gs_om -t status
Set output to terminal.
--------------------------------Cluster Status-------------------------
az_state :      single_az
<strong>cluster_state : Normal</strong>
<strong>balanced :      true</strong></pre>
      </div> <p></p></li>
    </ol>
   </div>
   <p><strong>Checking the status of the GaussDB T database single-node system</strong></p>
   <ol>
    <li><span>Log in to the database host.</span><p></p><p>This section uses GaussDB T 1.2.1 as an example.</p> <p></p></li>
    <li><span>Run the following command to change to the database user, for example, user <strong>omm</strong>.</span><p></p><pre class="screen">su - omm</pre> <p></p></li>
    <li><span>Run the following command to go to the directory where the database management tool is located:</span><p></p><pre class="screen">cd $GAUSSHOME/bin</pre> <p></p></li>
    <li><span>Run the following command to check the database status:</span><p></p><pre class="screen">python zctl.py -t status -D <em>DATADIR</em> -P</pre> <p><a href="#gaussdbT_00011__table188211837115812">Table 1</a> describes the related parameters.</p>
     <div class="tablenoborder">
      <a name="gaussdbT_00011__table188211837115812"></a><a name="table188211837115812"></a>
      <table cellpadding="4" cellspacing="0" summary="" id="gaussdbT_00011__table188211837115812" frame="border" border="1" rules="all">
       <caption>
        <b>Table 1 </b>Parameters of the zctl.py tool
       </caption>
       <colgroup>
        <col style="width:50%">
        <col style="width:50%">
       </colgroup>
       <thead align="left">
        <tr>
         <th align="left" class="cellrowborder" valign="top" width="50%" id="mcps1.3.4.4.2.3.2.3.1.1"><p>Parameter</p></th>
         <th align="left" class="cellrowborder" valign="top" width="50%" id="mcps1.3.4.4.2.3.2.3.1.2"><p>Description</p></th>
        </tr>
       </thead>
       <tbody>
        <tr>
         <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.4.4.2.3.2.3.1.1 "><p>-D</p></td>
         <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.4.4.2.3.2.3.1.2 "><p>Database data file path, that is, the <strong>GSDB_DATA</strong> path.</p></td>
        </tr>
        <tr>
         <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.4.4.2.3.2.3.1.1 "><p>-P</p></td>
         <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.4.4.2.3.2.3.1.2 "><p>Tool connects to the database using the username and password. During the command execution, the system prompts you to enter the username and password for connecting to the database. This parameter can be left empty. If it is left empty, the database is connected through password-free login.</p>
          <div class="note">
           <span class="notetitle"> NOTE: </span>
           <div class="notebody">
            <p>When <strong>zctl.py -t start</strong> is invoked, you do not need to log in to the database. In this case, the <strong>-P</strong> parameter is used only as a compatibility parameter. The script does not verify or use the username and password entered in interactive mode.</p>
           </div>
          </div></td>
        </tr>
        <tr>
         <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.4.4.2.3.2.3.1.1 "><p>Username</p></td>
         <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.4.4.2.3.2.3.1.2 "><p>Username of the database system. This parameter is mandatory when password-free login is not configured for the database system.</p></td>
        </tr>
        <tr>
         <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.4.4.2.3.2.3.1.1 "><p>Password</p></td>
         <td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.4.4.2.3.2.3.1.2 "><p>Password of the database system. This parameter is mandatory when password-free login is not configured for the database system.</p></td>
        </tr>
       </tbody>
      </table>
     </div> <p>If the command output is similar to the following example, the database status is normal.</p> <p><span><img src="en-us_image_0000001839268085.png"></span></p> <p></p></li>
   </ol>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="gaussdbT_00010.html">Backing Up a GaussDB T Database</a>
    </div>
   </div>
  </div>
 </body>
</html>