<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Performing Backup">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="EN-US_TOPIC_0000001964713008">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Performing Backup</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="EN-US_TOPIC_0000001964713008"></a><a name="EN-US_TOPIC_0000001964713008"></a>

<h1 class="topictitle1">Performing Backup</h1>
<div><p id="EN-US_TOPIC_0000001964713008__p14427140104917">Before performing backup, you need to associate the resources to be protected with a specified SLA. The system protects the resources and periodically executes backup jobs based on the SLA. You can perform a backup job immediately through manual backup.</p>
<div class="section"><h4 class="sectiontitle">Precautions</h4><ul><li id="EN-US_TOPIC_0000001964713008__li336125611415">If a replication or archive policy has been defined in the SLA, the system will perform replication or archiving once based on the SLA when you perform manual backup.</li></ul>
</div>
<div class="section"><h4 class="sectiontitle">Prerequisites</h4><p>XX</p>
</div>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li id="EN-US_TOPIC_0000001964713008__li173515381130"><span>Select an SLA.</span><p><div class="p" id="EN-US_TOPIC_0000001964713008__p16181740836">You can also click <span class="uicontrol" id="EN-US_TOPIC_0000001964713008__uicontrol169554520714"><b><span id="EN-US_TOPIC_0000001964713008__text179554516717"><strong>Create</strong></span></b></span> to create an SLA.<div class="note" id="EN-US_TOPIC_0000001964713008__note25947145710"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="EN-US_TOPIC_0000001964713008__p125947145711"><span id="EN-US_TOPIC_0000001964713008__ph1413818422327">If a WORM policy has been configured for the resources to be protected, select an SLA without a WORM policy to avoid WORM policy conflicts.</span></p>
</div></div>
</div>
</p></li><li id="EN-US_TOPIC_0000001964713008__li1333921134717"><span>Click <span class="uicontrol" id="EN-US_TOPIC_0000001964713008__uicontrol23691650677"><b>OK</b></span>.</span><p><p id="EN-US_TOPIC_0000001964713008__en-us_topic_0000001656691397_p25041111535">If the current system time is later than the start time of the first backup specified in the SLA, you can select <strong id="EN-US_TOPIC_0000001964713008__b9624185216713">Execute manual backup now</strong> in the dialog box that is displayed or choose to perform automatic backup periodically based on the backup policy set in the SLA.</p>
</p></li><li id="EN-US_TOPIC_0000001964713008__li647231474515"><strong>Optional: </strong><span>Perform manual backup.</span><p><div class="p" id="EN-US_TOPIC_0000001964713008__p11899115114511">If you want to execute a backup job immediately, perform manual backup through the following operations. Otherwise, skip this step.<ol type="a" id="EN-US_TOPIC_0000001964713008__ol1888313571212"><li id="EN-US_TOPIC_0000001964713008__li6449836144510">In the row of the target resource, choose <strong id="EN-US_TOPIC_0000001964713008__b146814226353">More</strong> &gt; <span id="EN-US_TOPIC_0000001964713008__text18439122416533"><strong>Manual Backup</strong></span>.<div class="note" id="EN-US_TOPIC_0000001964713008__note1527151103"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="EN-US_TOPIC_0000001964713008__en-us_topic_0000001607531736_p870125314544">You can select multiple resources to perform manual backup in batches. Select multiple protected resources and choose <strong id="EN-US_TOPIC_0000001964713008__b1657653183519">More</strong> &gt; <span id="EN-US_TOPIC_0000001964713008__text122852865419"><strong>Manual Backup</strong></span> in the upper left corner of the resource list.</p>
</div></div>
</li><li id="EN-US_TOPIC_0000001964713008__li5644125184512">Set the name of the copy generated during manual backup.<p id="EN-US_TOPIC_0000001964713008__p444210492470"><a name="EN-US_TOPIC_0000001964713008__li5644125184512"></a><a name="li5644125184512"></a>If this parameter is left unspecified, the system sets the copy name to <strong id="EN-US_TOPIC_0000001964713008__b17709174711816">backup_</strong><em id="EN-US_TOPIC_0000001964713008__i1371044717814">Timestamp</em> by default.</p>
</li><li id="EN-US_TOPIC_0000001964713008__li9911344144518">Select a protection policy, which can be <span id="EN-US_TOPIC_0000001964713008__text1037414518818"><strong>Full backup</strong></span> or <span id="EN-US_TOPIC_0000001964713008__text937414517813"><strong>Forever Incremental (Synthetic Full) Backup</strong></span>.<div class="note" id="EN-US_TOPIC_0000001964713008__note1552298154910"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="EN-US_TOPIC_0000001964713008__p13522118194919"><span id="EN-US_TOPIC_0000001964713008__ph19374310343">For 1.6.0 and later versions, if the selected protection policy is different from that configured in the associated SLA, the WORM configuration does not take effect.</span></p>
</div></div>
</li><li id="EN-US_TOPIC_0000001964713008__li2637101024612">Click <span class="uicontrol" id="EN-US_TOPIC_0000001964713008__uicontrol1662615410817"><b><span id="EN-US_TOPIC_0000001964713008__text13625854687"><strong>OK</strong></span></b></span>.</li></ol>
</div>
</p></li></ol>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>