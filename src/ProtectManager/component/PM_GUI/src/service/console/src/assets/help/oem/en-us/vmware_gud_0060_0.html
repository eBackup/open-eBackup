<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Restoring Files in a VMware VM">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="vmware_gud_0060_0">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Restoring Files in a VMware VM</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="vmware_gud_0060_0"></a><a name="vmware_gud_0060_0"></a>
  <h1 class="topictitle1">Restoring Files in a VMware VM</h1>
  <div>
   <p>This section describes how to restore files on a VM by using the file-level restoration function.</p>
   <div class="section" id="vmware_gud_0060_0__section15316122111191">
    <h4 class="sectiontitle">Context</h4>
    <ul id="vmware_gud_0060_0__ul1684123102616">
     <li id="vmware_gud_0060_0__li15684153110268">The <span id="vmware_gud_0060_0__text10124544712">product</span> supports file-level restoration using backup copies and replication copies. Note that data cannot be restored to the original location when a replication copy is used.</li>
     <li id="vmware_gud_0060_0__li2871134142617">The types of Linux file systems that can be restored are ext2/ext3/ext4 and XFS. The types of Windows file systems that can be restored are NTFS, FAT, and FAT32.</li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Prerequisites</h4>
    <ul>
     <li id="vmware_gud_0060_0__li7797742163118">The IP address of the backup network plane has been configured for the VM to which files are to be restored.</li>
     <li>If the target VM runs the Windows OS, ensure that ports <strong>445</strong> and <strong>138</strong> have been enabled on the target VM. If the target VM runs the Linux OS, ensure that port <strong>22</strong> has been enabled on the target VM.</li>
     <li id="vmware_gud_0060_0__li18725333113017">If the target VM runs on the Windows OS, ensure that the CIFS service has been enabled on the VM. For details, see the <a href="https://docs.microsoft.com/en-us/windows-server/storage/file-server/troubleshoot/detect-enable-and-disable-smbv1-v2-v3" target="_blank" rel="noopener noreferrer">Microsoft official website</a>.</li>
     <li id="vmware_gud_0060_0__li4463165994619">If the target VM runs on a non-Windows OS, ensure that rsync has been installed and started on the target VM.</li>
     <li>Ensure that VMware Tools has been installed on the target VM. For details, see <a href="vmware_gud_0016.html">Step 1: Checking and Installing VMware Tools</a>.</li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li><span>Choose <span class="uicontrol" id="vmware_gud_0060_0__vmware_gud_0058_0_en-us_topic_0000001839142377_uicontrol1812933174613"><b><span id="vmware_gud_0060_0__vmware_gud_0058_0_en-us_topic_0000001839142377_text0129103112462"><strong>Explore</strong></span> &gt; <span id="vmware_gud_0060_0__vmware_gud_0058_0_en-us_topic_0000001839142377_text81291431134613"><strong>Copy Data</strong></span> &gt; <span id="vmware_gud_0060_0__vmware_gud_0058_0_en-us_topic_0000001839142377_text860175510430"><strong>Virtualization</strong></span> &gt; <span id="vmware_gud_0060_0__vmware_gud_0058_0_en-us_topic_0000001839142377_text8319371528"><strong>VMware</strong></span></b></span>.</span></li>
     <li id="vmware_gud_0060_0__li888616341964"><span>You can search for a copy by VM or copy. This section uses a VM as an example.</span><p></p><p id="vmware_gud_0060_0__p624620403810">Click <strong id="vmware_gud_0060_0__b1858284474018">Resources</strong> to switch to the corresponding page, search for the VM to be restored by name, and click the VM name.</p> <p></p></li>
     <li id="vmware_gud_0060_0__li02165214225"><span>Click <span class="uicontrol" id="vmware_gud_0060_0__uicontrol1812414562220"><b><span id="vmware_gud_0060_0__text2022513145564"><strong>Copy Data</strong></span></b></span> and select the year, month, and day in sequence to find the copy.</span><p></p><p id="vmware_gud_0060_0__p9918150182310">If <span><img id="vmware_gud_0060_0__image99531319193912" src="en-us_image_0000001839187161.png"></span> is displayed below a month or date, copies exist in the month or on the date.</p> <p></p></li>
     <li><span>In the row of the target copy, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>File-level Restoration</strong></span></b></span>.</span><p></p>
      <div class="note">
       <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
       <div class="notebody">
        <ul>
         <li id="vmware_gud_0060_0__li15621555121712">For 1.5.0, the index status of the copy used for file-level restoration must be <span class="uicontrol" id="vmware_gud_0060_0__uicontrol483213401802"><b><span id="vmware_gud_0060_0__text1711017353520"><strong>Indexed</strong></span></b></span>. For 1.6.0 and later versions, copies whose index status is <span id="vmware_gud_0060_0__text413155718286"><strong>Not indexed</strong></span> can also be used for file-level restoration.</li>
         <li id="vmware_gud_0060_0__li1694104242611">For 1.5.0, if automatic indexing has been enabled for the SLA associated with the VM corresponding to the copy, the index status of the copy is <span class="uicontrol" id="vmware_gud_0060_0__uicontrol2988201618593"><b><span id="vmware_gud_0060_0__text198816168595"><strong>Indexed</strong></span></b></span>, and file-level restoration can be directly performed. If automatic indexing is disabled, click <span class="uicontrol" id="vmware_gud_0060_0__uicontrol1036516011116"><b><span id="vmware_gud_0060_0__text31091220421"><strong>Manually Create Index</strong></span></b></span> and then perform file-level restoration.</li>
         <li id="vmware_gud_0060_0__li158712231756">A copy that does not contain system disks cannot be used for file-level restoration.</li>
         <li id="vmware_gud_0060_0__li1022011225616">Reverse replication copies and archive copies cannot be used for file-level restoration.</li>
         <li>During VMware VM backup, the mount point information that is not written into the <strong>/etc/fstab</strong> file is stored in the memory. The <span>product</span> does not back up the information and does not support file-level restoration. If new file systems need to support file-level restoration, write the mount point information of the new file systems to the <strong>/etc/fstab</strong> file.</li>
         <li>You can also click <span class="uicontrol"><b><span><strong>Export</strong></span></b></span> to download files to the local host. The size of the file to be downloaded cannot exceed 1 GB. Linked files cannot be downloaded and do not support file-level restoration.</li>
         <li id="vmware_gud_0060_0__li7100104738">If the name of a folder or file contains garbled characters, file-level restoration is not supported. Do not select folders or files of this type. Otherwise, the restoration job fails.</li>
         <li id="vmware_gud_0060_0__li1836116462493">If the target VM runs the Windows OS and only the SMBv1 service is enabled, the time of the restored file is the current VM time.</li>
        </ul>
       </div>
      </div> <p></p></li>
     <li id="vmware_gud_0060_0__li1695634732916"><span>On the displayed page, click the <span class="wintitle" id="vmware_gud_0060_0__wintitle15956154715296"><b><span id="vmware_gud_0060_0__text10956184782910"><strong>File</strong></span></b></span> tab.</span></li>
     <li id="vmware_gud_0060_0__li789323103112"><span>Select the files to be restored and click <span class="uicontrol" id="vmware_gud_0060_0__uicontrol4286263213"><b>Restore File</b></span>.</span><p></p>
      <ul id="vmware_gud_0060_0__ul1521452811317">
       <li id="vmware_gud_0060_0__li8386311313">For 1.5.0, select the files to be restored from the directory tree.</li>
       <li id="vmware_gud_0060_0__li12142286316">For 1.6.0 and later versions, set <strong id="vmware_gud_0060_0__b175942210539">File Obtaining Mode</strong>, which can be <strong id="vmware_gud_0060_0__b29462035125211">Select file paths from the directory tree</strong> or <strong id="vmware_gud_0060_0__b1211515508528">Enter file paths</strong>.
        <div class="note" id="vmware_gud_0060_0__note1195634711290">
         <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
         <div class="notebody">
          <ul id="vmware_gud_0060_0__ul14956124712297">
           <li id="vmware_gud_0060_0__li1095674772911">If the copy contains too many files, the system may time out, preventing you from selecting files to be restored from the directory tree. Therefore, you are advised to enter the paths of files to be restored.</li>
           <li id="vmware_gud_0060_0__li19956104762918">When entering a file path, enter a complete file path, for example, <strong id="vmware_gud_0060_0__b745510458333">/opt/abc/efg.txt</strong> or <strong id="vmware_gud_0060_0__b186591048123315">C:\abc\efg.txt</strong>. If you enter a folder path, for example, <strong id="vmware_gud_0060_0__b15108423133414">/opt/abc</strong> or <strong id="vmware_gud_0060_0__b13283162543410">C:\abc</strong>, all files in the folder are restored. The file name in the path is case sensitive.</li>
          </ul>
         </div>
        </div></li>
      </ul> <p></p></li>
     <li><span>On the displayed <strong>Restore File</strong> page, select <strong>Original location</strong> or <strong>New location</strong> for <strong>Restore To</strong> and set restoration parameters.</span><p></p>
      <ul>
       <li>Select <strong>Original location</strong> to restore data to the original directory of the original VM.
        <div class="p">
         <a href="#vmware_gud_0060_0__table9960822181311">Table 1</a> describes the related parameters. 
         <div class="tablenoborder">
          <a name="vmware_gud_0060_0__table9960822181311"></a><a name="table9960822181311"></a>
          <table cellpadding="4" cellspacing="0" summary="" id="vmware_gud_0060_0__table9960822181311" frame="border" border="1" rules="all">
           <caption>
            <b>Table 1 </b>Parameters for restoring data to the original location
           </caption>
           <colgroup>
            <col style="width:29.9%">
            <col style="width:70.1%">
           </colgroup>
           <thead align="left">
            <tr>
             <th align="left" class="cellrowborder" valign="top" width="29.9%" id="mcps1.3.4.2.7.2.1.1.2.2.2.3.1.1"><p>Parameter</p></th>
             <th align="left" class="cellrowborder" valign="top" width="70.1%" id="mcps1.3.4.2.7.2.1.1.2.2.2.3.1.2"><p>Description</p></th>
            </tr>
           </thead>
           <tbody>
            <tr>
             <td class="cellrowborder" valign="top" width="29.9%" headers="mcps1.3.4.2.7.2.1.1.2.2.2.3.1.1 "><p><span><strong>VM IP Address</strong></span></p></td>
             <td class="cellrowborder" valign="top" width="70.1%" headers="mcps1.3.4.2.7.2.1.1.2.2.2.3.1.2 ">
              <div class="p" id="vmware_gud_0060_0__p49594222135">
               Select the configured backup network IP address. For 1.6.0 and later versions, you can manually enter the IP address.
               <div class="note" id="vmware_gud_0060_0__note13959142281314">
                <span class="notetitle"> NOTE: </span>
                <div class="notebody">
                 <ul id="vmware_gud_0060_0__ul184254158477">
                  <li id="vmware_gud_0060_0__li13425121554713">Before performing file-level restoration, you need to configure a backup network IP address for the VM. During file-level restoration, the system uses this IP address to connect to the target VM for restoration.</li>
                  <li id="vmware_gud_0060_0__li1130711714475">If the VM IP address is not in the list, exit the current page. Then, choose <span class="uicontrol" id="vmware_gud_0060_0__uicontrol61481615124114"><b><span id="vmware_gud_0060_0__en-us_topic_0000001839142377_text738292620372"><strong>Protection</strong></span> &gt; Virtualization &gt; VMware</b></span> and rescan the virtualization environment.</li>
                 </ul>
                </div>
               </div>
              </div></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="29.9%" headers="mcps1.3.4.2.7.2.1.1.2.2.2.3.1.1 "><p><span><strong>VM Username</strong></span></p></td>
             <td class="cellrowborder" rowspan="2" valign="top" width="70.1%" headers="mcps1.3.4.2.7.2.1.1.2.2.2.3.1.2 ">
              <div class="p" id="vmware_gud_0060_0__p196016226135">
               During file-level restoration, the target VM needs to verify the login credential of the <span id="vmware_gud_0060_0__text426913561378">product</span>. Enter a username and its password for logging in to the target VM.
               <ul id="vmware_gud_0060_0__ul119609220135">
                <li id="vmware_gud_0060_0__li796017225136">Windows OS: The default user name is <strong id="vmware_gud_0060_0__b1579523971220">Administrator</strong>.</li>
                <li id="vmware_gud_0060_0__li1496012225130">Linux OS: The default user name is <strong id="vmware_gud_0060_0__b29741052111218">root</strong>.</li>
               </ul>
               <div class="note" id="vmware_gud_0060_0__note10319381535">
                <span class="notetitle"> NOTE: </span>
                <div class="notebody">
                 <ul id="vmware_gud_0060_0__ul871974405518">
                  <li id="vmware_gud_0060_0__li8719444175514">The login user must have the read and write permissions on the directory to which data is to be restored on the target VM.</li>
                  <li id="vmware_gud_0060_0__li231424655513">After the restoration, the access permission on the file is the same as that of the login user.</li>
                 </ul>
                </div>
               </div>
              </div></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.7.2.1.1.2.2.2.3.1.1 "><p><span><strong>VM Password</strong></span></p></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="29.9%" headers="mcps1.3.4.2.7.2.1.1.2.2.2.3.1.1 "><p><span><strong>Overwrite Rule</strong></span></p></td>
             <td class="cellrowborder" valign="top" width="70.1%" headers="mcps1.3.4.2.7.2.1.1.2.2.2.3.1.2 ">
              <div class="p" id="vmware_gud_0060_0__p78773497306">
               If a file with the same name exists in the restoration path, you can choose to replace or skip the existing file.
               <ul id="vmware_gud_0060_0__ul138776497307">
                <li id="vmware_gud_0060_0__li1087714914306"><span id="vmware_gud_0060_0__text2877849173018"><strong>Replace existing files</strong></span></li>
                <li id="vmware_gud_0060_0__li1287717497308"><span id="vmware_gud_0060_0__text148771149123013"><strong>Skip existing files</strong></span>: Files with the same names are skipped and are not replaced.</li>
                <li id="vmware_gud_0060_0__li68771149143011"><span id="vmware_gud_0060_0__text5877184993017"><strong>Only replace the files older than the restoration file</strong></span>
                 <div class="note" id="vmware_gud_0060_0__note98771049113012">
                  <span class="notetitle"> NOTE: </span>
                  <div class="notebody">
                   <p id="vmware_gud_0060_0__p15877144918305">If the access to the file with the same name on the target VM is denied, the file fails to be restored during file replacement.</p>
                  </div>
                 </div></li>
               </ul>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
        </div></li>
       <li>Select <strong>New location</strong> to restore data to a specified VM.<p><a href="#vmware_gud_0060_0__table1881412588262">Table 2</a> describes the related parameters.</p>
        <div class="tablenoborder">
         <a name="vmware_gud_0060_0__table1881412588262"></a><a name="table1881412588262"></a>
         <table cellpadding="4" cellspacing="0" summary="" id="vmware_gud_0060_0__table1881412588262" frame="border" border="1" rules="all">
          <caption>
           <b>Table 2 </b>Parameters for restoring data to a new location
          </caption>
          <colgroup>
           <col style="width:22.75%">
           <col style="width:77.25%">
          </colgroup>
          <thead align="left">
           <tr>
            <th align="left" class="cellrowborder" valign="top" width="22.75%" id="mcps1.3.4.2.7.2.1.2.3.2.3.1.1"><p>Parameter</p></th>
            <th align="left" class="cellrowborder" valign="top" width="77.25%" id="mcps1.3.4.2.7.2.1.2.3.2.3.1.2"><p>Description</p></th>
           </tr>
          </thead>
          <tbody>
           <tr>
            <td class="cellrowborder" valign="top" width="22.75%" headers="mcps1.3.4.2.7.2.1.2.3.2.3.1.1 "><p><span><strong>Compute Location</strong></span></p></td>
            <td class="cellrowborder" valign="top" width="77.25%" headers="mcps1.3.4.2.7.2.1.2.3.2.3.1.2 ">
             <div class="p">
              Select the target VM to which data is restored.
              <div class="note">
               <span class="notetitle"> NOTE: </span>
               <div class="notebody">
                <div class="p">
                 For 1.5.0 and earlier versions:
                 <ul id="vmware_gud_0060_0__ul1581325811265">
                  <li id="vmware_gud_0060_0__li4813358152610">If the specified VM runs the Windows OS, the directory to which the file is restored is the same as the source directory of the file.
                   <ul id="vmware_gud_0060_0__ul481325852619">
                    <li id="vmware_gud_0060_0__li88134588262">If the corresponding drive letter does not exist on the VM, the restoration will fail.</li>
                    <li id="vmware_gud_0060_0__li2081385812616">If the corresponding drive letter exists but the corresponding directory does not exist on the VM, the corresponding directory will be automatically created on the VM before file restoration. For example, if the path of the specified file is <strong id="vmware_gud_0060_0__b17645189926">D:\a</strong>, the <strong id="vmware_gud_0060_0__b8645396219">a</strong> directory will be automatically created in drive <strong id="vmware_gud_0060_0__b3646129822">D</strong> of the specified VM before file restoration.</li>
                   </ul></li>
                  <li id="vmware_gud_0060_0__li081325872610">If the specified VM runs the Linux OS, the directory to which data is restored is the same as the original directory of the file. During restoration, the corresponding path is automatically created on the VM before file restoration is performed.</li>
                 </ul>
                </div>
               </div>
              </div>
             </div></td>
           </tr>
           <tr>
            <td class="cellrowborder" valign="top" width="22.75%" headers="mcps1.3.4.2.7.2.1.2.3.2.3.1.1 "><p>Target Path</p>
             <div class="note">
              <span class="notetitle"> NOTE: </span>
              <div class="notebody">
               <p>Only 1.6.0 and later versions support this parameter.</p>
              </div>
             </div></td>
            <td class="cellrowborder" valign="top" width="77.25%" headers="mcps1.3.4.2.7.2.1.2.3.2.3.1.2 ">
             <ul>
              <li>If no target path is not specified, data will be restored to the target VM based on the original path where the selected files are stored.</li>
              <li>If the target path is specified, the files will be restored to the specified path on the target VM. Enter an absolute path.
               <div class="note">
                <span class="notetitle"> NOTE: </span>
                <div class="notebody">
                 <ul>
                  <li>Assume that the target VM runs the Windows OS and the specified path for file restoration is <strong>D:\a</strong>. If drive <strong>D</strong> does not exist on the target VM, the restoration will fail. If drive <strong>D</strong> exists on the target VM but does not have the <strong>a</strong> directory, the <strong>a</strong> directory will be automatically created on drive <strong>D</strong> of the VM before file restoration is performed.</li>
                  <li>Assume that the target VM runs the Linux OS and the specified path for file restoration is <strong>/opt/b</strong>. If the target path does not exist on the VM, the corresponding path will be automatically created on the VM before file restoration is performed.</li>
                 </ul>
                </div>
               </div></li>
             </ul></td>
           </tr>
           <tr>
            <td class="cellrowborder" valign="top" width="22.75%" headers="mcps1.3.4.2.7.2.1.2.3.2.3.1.1 "><p><span><strong>VM IP Address</strong></span></p></td>
            <td class="cellrowborder" valign="top" width="77.25%" headers="mcps1.3.4.2.7.2.1.2.3.2.3.1.2 ">
             <div class="p">
              Select the configured backup network IP address. For 1.6.0 and later versions, you can manually enter the IP address.
              <div class="note" id="vmware_gud_0060_0__vmware_gud_0060_0_note13959142281314">
               <span class="notetitle"> NOTE: </span>
               <div class="notebody">
                <ul id="vmware_gud_0060_0__vmware_gud_0060_0_ul184254158477">
                 <li id="vmware_gud_0060_0__vmware_gud_0060_0_li13425121554713">Before performing file-level restoration, you need to configure a backup network IP address for the VM. During file-level restoration, the system uses this IP address to connect to the target VM for restoration.</li>
                 <li id="vmware_gud_0060_0__vmware_gud_0060_0_li1130711714475">If the VM IP address is not in the list, exit the current page. Then, choose <span class="uicontrol" id="vmware_gud_0060_0__vmware_gud_0060_0_uicontrol61481615124114"><b><span id="vmware_gud_0060_0__vmware_gud_0060_0_en-us_topic_0000001839142377_text738292620372"><strong>Protection</strong></span> &gt; Virtualization &gt; VMware</b></span> and rescan the virtualization environment.</li>
                </ul>
               </div>
              </div>
             </div></td>
           </tr>
           <tr>
            <td class="cellrowborder" valign="top" width="22.75%" headers="mcps1.3.4.2.7.2.1.2.3.2.3.1.1 "><p><span><strong>VM Username</strong></span></p></td>
            <td class="cellrowborder" rowspan="2" valign="top" width="77.25%" headers="mcps1.3.4.2.7.2.1.2.3.2.3.1.2 ">
             <div class="p">
              During file-level restoration, the target VM needs to verify the login credential of the <span id="vmware_gud_0060_0__vmware_gud_0060_0_text426913561378">product</span>. Enter a username and its password for logging in to the target VM.
              <ul id="vmware_gud_0060_0__vmware_gud_0060_0_ul119609220135">
               <li id="vmware_gud_0060_0__vmware_gud_0060_0_li796017225136">Windows OS: The default user name is <strong id="vmware_gud_0060_0__vmware_gud_0060_0_b1579523971220">Administrator</strong>.</li>
               <li id="vmware_gud_0060_0__vmware_gud_0060_0_li1496012225130">Linux OS: The default user name is <strong id="vmware_gud_0060_0__vmware_gud_0060_0_b29741052111218">root</strong>.</li>
              </ul>
              <div class="note" id="vmware_gud_0060_0__vmware_gud_0060_0_note10319381535">
               <span class="notetitle"> NOTE: </span>
               <div class="notebody">
                <ul id="vmware_gud_0060_0__vmware_gud_0060_0_ul871974405518">
                 <li id="vmware_gud_0060_0__vmware_gud_0060_0_li8719444175514">The login user must have the read and write permissions on the directory to which data is to be restored on the target VM.</li>
                 <li id="vmware_gud_0060_0__vmware_gud_0060_0_li231424655513">After the restoration, the access permission on the file is the same as that of the login user.</li>
                </ul>
               </div>
              </div>
             </div></td>
           </tr>
           <tr>
            <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.7.2.1.2.3.2.3.1.1 "><p><span><strong>VM Password</strong></span></p></td>
           </tr>
           <tr>
            <td class="cellrowborder" valign="top" width="22.75%" headers="mcps1.3.4.2.7.2.1.2.3.2.3.1.1 "><p><span><strong>Overwrite Rule</strong></span></p></td>
            <td class="cellrowborder" valign="top" width="77.25%" headers="mcps1.3.4.2.7.2.1.2.3.2.3.1.2 ">
             <div class="p">
              If a file with the same name exists in the restoration path, you can choose to replace or skip the existing file.
              <ul id="vmware_gud_0060_0__vmware_gud_0060_0_ul138776497307">
               <li id="vmware_gud_0060_0__vmware_gud_0060_0_li1087714914306"><span id="vmware_gud_0060_0__vmware_gud_0060_0_text2877849173018"><strong>Replace existing files</strong></span></li>
               <li id="vmware_gud_0060_0__vmware_gud_0060_0_li1287717497308"><span id="vmware_gud_0060_0__vmware_gud_0060_0_text148771149123013"><strong>Skip existing files</strong></span>: Files with the same names are skipped and are not replaced.</li>
               <li id="vmware_gud_0060_0__vmware_gud_0060_0_li68771149143011"><span id="vmware_gud_0060_0__vmware_gud_0060_0_text5877184993017"><strong>Only replace the files older than the restoration file</strong></span>
                <div class="note" id="vmware_gud_0060_0__vmware_gud_0060_0_note98771049113012">
                 <span class="notetitle"> NOTE: </span>
                 <div class="notebody">
                  <p id="vmware_gud_0060_0__vmware_gud_0060_0_p15877144918305">If the access to the file with the same name on the target VM is denied, the file fails to be restored during file replacement.</p>
                 </div>
                </div></li>
              </ul>
             </div></td>
           </tr>
          </tbody>
         </table>
        </div></li>
      </ul> <p></p></li>
     <li id="vmware_gud_0060_0__li46688254910"><span>Click <span class="uicontrol" id="vmware_gud_0060_0__uicontrol18891938492"><b><span id="vmware_gud_0060_0__text1252310490912"><strong>Test</strong></span></b></span> and ensure that the target VM to be restored is properly connected to the <span id="vmware_gud_0060_0__text12612599715">product</span>.</span></li>
     <li><span>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</span></li>
    </ol>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Follow-up Procedure</h4>
    <p id="vmware_gud_0060_0__vmware_gud_0058_0_p171491160561">For 1.6.0 and later versions, if a restoration job fails, execute the job again by referring to <a href="vmware_gud_0099_2.html">How Do I Retry a Failed VMware Restoration Job? (Applicable to 1.6.0 and Later Versions)</a>.</p>
   </div>
  </div>
 </body>
</html>