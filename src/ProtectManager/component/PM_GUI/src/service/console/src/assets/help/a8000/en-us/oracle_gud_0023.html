<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Step 4: Registering a Database">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="oracle_gud_0013.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="oracle_gud_0023">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Step 4: Registering a Database</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="oracle_gud_0023"></a><a name="oracle_gud_0023"></a>

<h1 class="topictitle1">Step 4: Registering a Database</h1>
<div><p>Before backing up and restoring a database, you must register the database with the <span>OceanProtect</span>.</p>
<div class="section"><h4 class="sectiontitle">Prerequisites</h4><p id="oracle_gud_0023__oracle_gud_0022_p138022121518">Ensure that the ProtectAgent installation user can be switched to the Oracle installation user by running the <strong id="oracle_gud_0023__oracle_gud_0022_b7716401510">su - oracle</strong> command. If the grid component is installed, ensure that you can run the <strong id="oracle_gud_0023__oracle_gud_0022_b923919186527">su - grid</strong> command to switch to user <strong id="oracle_gud_0023__oracle_gud_0022_b15797182405211">grid</strong>.</p>
</div>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li><span>Choose <span class="uicontrol" id="oracle_gud_0023__en-us_topic_0000001839142377_uicontrol49641842919"><b><span id="oracle_gud_0023__en-us_topic_0000001839142377_text63901566195"><strong>Protection</strong></span> &gt; Databases &gt; Oracle</b></span>.</span></li></ol><ol start="2"><li><span>Click the <span class="uicontrol"><b><span><strong>Databases</strong></span></b></span> tab, and then click <span class="uicontrol"><b><span><strong>Register</strong></span></b></span>.</span><p><p><a href="#oracle_gud_0023__table17561557152715">Table 1</a> describes the related parameters.</p>

<div class="tablenoborder"><a name="oracle_gud_0023__table17561557152715"></a><a name="table17561557152715"></a><table cellpadding="4" cellspacing="0" summary="" id="oracle_gud_0023__table17561557152715" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Parameters for registering a database</caption><colgroup><col style="width:31.4%"><col style="width:68.60000000000001%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="31.4%" id="mcps1.3.3.3.1.2.2.2.3.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="68.60000000000001%" id="mcps1.3.3.3.1.2.2.2.3.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="31.4%" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><span><strong>Type</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="68.60000000000001%" headers="mcps1.3.3.3.1.2.2.2.3.1.2 "><p>Database type, which can be <span class="uicontrol"><b>Single Server</b></span> or <span class="uicontrol"><b>Cluster</b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="31.4%" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><span><strong>Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="68.60000000000001%" headers="mcps1.3.3.3.1.2.2.2.3.1.2 "><p>Name of the database to be protected.</p>
<div class="p">The name must be the same as the database name in the production environment. Generally, the value of <strong>name</strong> is the same as that of <strong>db_unique_name</strong>. If they are different, set the database name to <strong>db_unique_name</strong>. You can run the following SQL statement to view the values of <strong>name</strong> and <strong>db_unique_name</strong>:<pre class="screen">select name,db_unique_name FROM v$database;</pre>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="31.4%" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><strong>Host</strong>/<strong>Cluster</strong></p>
</td>
<td class="cellrowborder" valign="top" width="68.60000000000001%" headers="mcps1.3.3.3.1.2.2.2.3.1.2 "><p>Host or cluster where the database is located.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="31.4%" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><strong>Running Database Username</strong></p>
</td>
<td class="cellrowborder" valign="top" width="68.60000000000001%" headers="mcps1.3.3.3.1.2.2.2.3.1.2 "><p>If the host where the database is located runs the Windows OS, contact the database administrator to obtain the username of the system administrator of the host.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="31.4%" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><strong>Running Database User Password</strong></p>
</td>
<td class="cellrowborder" valign="top" width="68.60000000000001%" headers="mcps1.3.3.3.1.2.2.2.3.1.2 "><p>If the host where the database is located runs the Windows OS, contact the database administrator to obtain the password of the system administrator of the host.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="31.4%" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><span><strong>Database Authentication Method</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="68.60000000000001%" headers="mcps1.3.3.3.1.2.2.2.3.1.2 "><p>Database authentication mode, which can be <span class="uicontrol"><b>Database authentication</b></span> or <span class="uicontrol"><b>OS authentication</b></span>. The value must be the same as that configured on the database.</p>
<p>For details about how to query the database authentication mode, see <a href="oracle_gud_0012.html">Preparations for Backup</a>.</p>
<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><ul><li>If the host where the database to be backed up resides is the secondary end of the Oracle ADG cluster, this parameter must be set to <strong>Database authentication</strong>.</li><li>If multiple agent hosts are specified in <a href="oracle_gud_0028.html#oracle_gud_0028__li164660725814">4</a> to perform backup jobs, this parameter must be set to <strong>Database authentication</strong>.</li><li>In the cluster scenario, if the database authentication modes queried on different database nodes are different, set this parameter to <strong>Database authentication</strong>.</li></ul>
</div></div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="31.4%" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><span><strong>Database Installation Username</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="68.60000000000001%" headers="mcps1.3.3.3.1.2.2.2.3.1.2 "><p>User for database installation, for example, <strong>oracle</strong>. For details about how to obtain the value, see <a href="oracle_gud_0012.html">Preparations for Backup</a>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="31.4%" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><strong>ORACLE_HOME Path</strong></p>
<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><p>This parameter is available only in 1.6.0 and later versions.</p>
</div></div>
</td>
<td class="cellrowborder" valign="top" width="68.60000000000001%" headers="mcps1.3.3.3.1.2.2.2.3.1.2 "><p>This parameter is displayed only when the database node runs the Linux OS.</p>
<p><strong>ORACLE_HOME</strong> is an environment variable of the Oracle software. If the <strong>ORACLE_HOME</strong> environment variable is not configured for the database node installation user, you need to manually configure it. The configuration method is as follows (when <strong>Type</strong> is set to <strong>Cluster</strong>, perform the following steps on all nodes):</p>
<ol type="a"><li>Log in to the database node and check whether the <strong>ORACLE_HOME</strong> environment variable is configured.<pre class="screen">su - oracle -c 'env | grep ORACLE_HOME'</pre>
<ul><li>If the command output is not empty, the <strong>ORACLE_HOME</strong> environment variable has been configured. In this case, you do not need to configure this parameter on the management page.</li><li>If the command output is empty, go to the next step.</li></ul>
</li><li>Manually configure the <strong>ORACLE_HOME</strong> environment variable.<ul><li>If the environment variable configuration file can be modified, perform the following operations:<div class="p">Contact the database administrator to obtain ORACLE_HOME path, and edit the environment variable configuration file (for example, <strong>.bash_profile</strong>) in the directory of the database installation user by adding the path to the last line. For example:<pre class="screen">export ORACLE_HOME=/<em>XXX</em></pre>
</div>
<p>Save the environment variable configuration file and run the following command to make the environment variable take effect:</p>
<pre class="screen">source ~/.bash_profile</pre>
</li><li>If the environment variable configuration file cannot be modified, contact the database administrator to obtain the ORACLE_HOME path and enter it on the management page.</li></ul>
</li></ol>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="31.4%" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><strong>ORACLE_BASE Path</strong></p>
<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><p>This parameter is available only in 1.6.0 and later versions.</p>
</div></div>
</td>
<td class="cellrowborder" valign="top" width="68.60000000000001%" headers="mcps1.3.3.3.1.2.2.2.3.1.2 "><p>This parameter is displayed only when the database node runs the Linux OS.</p>
<p><strong>ORACLE_BASE</strong> is an environment variable of the Oracle software. If the <strong>ORACLE_BASE</strong> environment variable is not configured for the database node installation user, you need to manually configure it. The configuration method is as follows (when <strong>Type</strong> is set to <strong>Cluster</strong>, perform the following steps on all nodes):</p>
<ol type="a"><li>Log in to the database node and check whether the <strong>ORACLE_BASE</strong> environment variable is configured.<pre class="screen">su - oracle -c 'env | grep ORACLE_BASE'</pre>
<ul><li>If the command output is not empty, the <strong>ORACLE_BASE</strong> environment variable has been configured. In this case, you do not need to configure this parameter on the management page.</li><li>If the command output is empty, go to the next step.</li></ul>
</li><li>Manually configure the <strong>ORACLE_BASE</strong> environment variable.<ul><li>If the environment variable configuration file can be modified, perform the following operations:<div class="p">Contact the database administrator to obtain ORACLE_BASE path, and edit the environment variable configuration file (for example, <strong>.bash_profile</strong>) in the directory of the database installation user by adding the path to the last line. For example:<pre class="screen">export ORACLE_BASE=/<em>XXX</em></pre>
</div>
<p>Save the environment variable configuration file and run the following command to make the environment variable take effect:</p>
<pre class="screen">source ~/.bash_profile</pre>
</li><li>If the environment variable configuration file cannot be modified, contact the database administrator to obtain the ORACLE_BASE path and enter it on the management page.</li></ul>
</li></ol>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="31.4%" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><span><strong>Database Username</strong></span></p>
</td>
<td class="cellrowborder" rowspan="2" valign="top" width="68.60000000000001%" headers="mcps1.3.3.3.1.2.2.2.3.1.2 "><p>If the <span class="uicontrol"><b>Database Authentication Method</b></span> is set to <span class="uicontrol"><b>Database authentication</b></span>, you need to configure the username and password of the database.</p>
<p>Enter the name and password of a user with the <strong>sysdba</strong> permissions.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><span><strong>Database Password</strong></span></p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="31.4%" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><span><strong>ASM Authentication</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="68.60000000000001%" headers="mcps1.3.3.3.1.2.2.2.3.1.2 "><p>If the database uses ASM, you need to enable ASM authentication.</p>
<p>To check whether the database uses the ASM, see <a href="oracle_gud_0012.html">Preparations for Backup</a>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="31.4%" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><span><strong>Authentication Method</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="68.60000000000001%" headers="mcps1.3.3.3.1.2.2.2.3.1.2 "><p>ASM authentication mode, which must be the same as that configured in the database.</p>
<p>To query the ASM authentication mode configured in the database, see <a href="oracle_gud_0012.html">Preparations for Backup</a>.</p>
<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><p>In the cluster scenario, if the ASM authentication modes queried on different database nodes are different, set this parameter to <strong>ASM Authentication</strong>.</p>
</div></div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="31.4%" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><span><strong>Installation Username</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="68.60000000000001%" headers="mcps1.3.3.3.1.2.2.2.3.1.2 "><p>ASM installation username, for example, <strong>grid</strong>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="31.4%" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><span><strong>ASM Username</strong></span></p>
</td>
<td class="cellrowborder" rowspan="2" valign="top" width="68.60000000000001%" headers="mcps1.3.3.3.1.2.2.2.3.1.2 "><p>This parameter is mandatory when <strong>Authentication Method</strong> is set to <strong>ASM Authentication</strong>. Enter the name and password of a user with the <strong>sysdba</strong> permissions.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.3.3.1.2.2.2.3.1.1 "><p><span><strong>ASM Password</strong></span></p>
</td>
</tr>
</tbody>
</table>
</div>
</p></li><li><span>In the <span><strong>Storage Resources</strong></span> area, click <span><strong>Add</strong></span>. <a href="#oracle_gud_0023__table5692101024117">Table 2</a> describes storage resource parameters.</span><p><div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><ul><li>Only 1.6.0 and later versions support this function.</li><li>When the Oracle database is connected to the OceanStor Dorado 6.<em>x</em> storage device and snapshot-based backup at the storage layer is required, add storage resources. In other scenarios, you do not need to add storage resources.</li><li>When the Oracle database is connected to OceanStor Dorado 6.<em>x</em> storage devices of a HyperMetro pair, you can add two storage resources. The storage resource that is first added is preferentially used for backup and restoration jobs.</li></ul>
</div></div>

<div class="tablenoborder"><a name="oracle_gud_0023__table5692101024117"></a><a name="table5692101024117"></a><table cellpadding="4" cellspacing="0" summary="" id="oracle_gud_0023__table5692101024117" frame="border" border="1" rules="all"><caption><b>Table 2 </b>Parameters for adding storage resources</caption><colgroup><col style="width:25.629999999999995%"><col style="width:74.37%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="25.629999999999995%" id="mcps1.3.3.3.2.2.2.2.3.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="74.37%" id="mcps1.3.3.3.2.2.2.2.3.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.3.2.2.2.2.3.1.1 "><p><span><strong>Device Type</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.3.2.2.2.2.3.1.2 "><p>Storage resource type.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.3.2.2.2.2.3.1.1 "><p><span><strong>Management IP Address</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.3.2.2.2.2.3.1.2 "><p>You can enter multiple management IP addresses of a storage device. If a management IP address cannot be accessed, the system automatically switches to another accessible management IP address.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.3.2.2.2.2.3.1.1 "><p><span><strong>Port</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.3.2.2.2.2.3.1.2 "><p>Port for accessing storage resources. Port <strong>8088</strong> is used by default.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.3.2.2.2.2.3.1.1 "><p><span><strong>Username</strong></span></p>
</td>
<td class="cellrowborder" rowspan="2" valign="top" width="74.37%" headers="mcps1.3.3.3.2.2.2.2.3.1.2 "><p>Username and password for accessing storage resources.</p>
<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><p>Use an administrator account, and ensure that the RESTful login mode is available for the user and the password has been initialized. Otherwise, backup of resources of the device may fail.</p>
</div></div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.3.3.2.2.2.2.3.1.1 "><p><span><strong>Password</strong></span></p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.3.2.2.2.2.3.1.1 "><p><span><strong>Protocol</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.3.2.2.2.2.3.1.2 "><p>Select the data transmission protocol used by the Oracle database host to connect to storage resources.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.3.2.2.2.2.3.1.1 "><p><span><strong>Certificate verification</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.3.2.2.2.2.3.1.2 "><p>This function is enabled by default. The storage resource certificate will be verified during database registration to ensure security of interactions between the <span>OceanProtect</span> and external devices. After this function is disabled, security risks may exist in the communication between the system and the storage device. Exercise caution when performing this operation.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.3.2.2.2.2.3.1.1 "><p><span><strong>Certificate</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.3.2.2.2.2.3.1.2 "><p>Import the CA certificate of the storage resource obtained in <a href="oracle_gud_ca.html">Step 2: Obtaining the CA Certificate of the Storage Resource (Applicable to Snapshot-based Backup at the Storage Layer)</a>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.3.3.2.2.2.2.3.1.1 "><p><span><strong>Revocation List</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.3.3.2.2.2.2.3.1.2 "><p>After the CRL is imported, the system checks whether the client certificate has been revoked. If the certificate has been revoked, storage device registration fails.</p>
</td>
</tr>
</tbody>
</table>
</div>
</p></li><li><span>Click <span class="uicontrol"><b>OK</b></span>.</span></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="oracle_gud_0013.html">Backing Up an Oracle Database</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>