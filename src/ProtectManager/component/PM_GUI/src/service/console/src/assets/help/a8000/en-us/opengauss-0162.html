<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Managing openGauss Clusters">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="opengauss-0159.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="opengauss-0162">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Managing openGauss Clusters</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="opengauss-0162"></a><a name="opengauss-0162"></a>

<h1 class="topictitle1">Managing openGauss Clusters</h1>
<div><p>The <span>OceanProtect</span> allows you to delete, authorize, or reclaim clusters.</p>
<p>You need to log in to the management page, choose <span class="uicontrol"><b><span id="opengauss-0162__en-us_topic_0000001839142377_text197344403116"><strong>Protection</strong></span> &gt; Databases &gt; openGauss</b></span>, click the <span class="uicontrol"><b>Cluster</b></span> tab, and find the target cluster.</p>
<p><a href="#opengauss-0162__en-us_topic_0000001263933970_table24934294919">Table 1</a> describes related operations.</p>

<div class="tablenoborder"><a name="opengauss-0162__en-us_topic_0000001263933970_table24934294919"></a><a name="en-us_topic_0000001263933970_table24934294919"></a><table cellpadding="4" cellspacing="0" summary="" id="opengauss-0162__en-us_topic_0000001263933970_table24934294919" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Cluster-related operations</caption><colgroup><col style="width:14.37%"><col style="width:57.9%"><col style="width:27.73%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="14.370000000000003%" id="mcps1.3.4.2.4.1.1"><p>Operation</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="57.900000000000006%" id="mcps1.3.4.2.4.1.2"><p>Description</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="27.730000000000004%" id="mcps1.3.4.2.4.1.3"><p>Navigation Path</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Modifying</p>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This section describes how to modify the cluster name, type, and node information.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify</strong></span></b></span> in the row where the cluster is located.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Deleting</p>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>You can delete a host or cluster if its databases no longer require protection. After the host or cluster is deleted, its information disappears from the WebUI.</p>
<p><strong>Note</strong></p>
<p>A protected database or instance in a cluster cannot be deleted.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target host or cluster, choose <span class="uicontrol"><b>More &gt; Delete</b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Authorizing resources</p>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>By default, only the system administrator has the permission to protect resources. If you want to assign this permission to a data protection administrator, authorize the data protection administrator as the system administrator to protect resources.</p>
<p><strong>Note</strong></p>
<ul><li id="opengauss-0162__en-us_topic_0000001656681249_li1491782615119">Users cannot be authorized to protect resources that have been associated with SLAs. You need to remove the protection of resources before performing authorization.</li><li id="opengauss-0162__en-us_topic_0000001656681249_li14274329155112">A single resource can be authorized only to a single data protection administrator.</li></ul>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Locate the row that contains the target host or cluster, choose <span class="uicontrol"><b>More &gt; Authorize Resource</b></span>, and select the data protection administrator to be authorized.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><p>Reclaiming resources</p>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to reclaim resources authorized to the data protection administrator. The data protection administration is not permitted to protect the resources that have been reclaimed.</p>
<p><strong>Note</strong></p>
<p>Users are not allowed to reclaim resources that have been associated with SLAs. You need to remove the protection of resources before reclaiming them.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>In the row where the target host or cluster resides, choose <span class="uicontrol"><b>More &gt; Reclaim Resource</b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Test Connectivity</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to test the connectivity between the <span>OceanProtect</span> and the cluster.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target host or cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Test Connectivity</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Resource Scan</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p>This operation enables you to immediately update changes to the <span>OceanProtect</span>.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target host or cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Resource Scan</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><div class="p">Authorizing resources<div class="note" id="opengauss-0162__en-us_topic_0000001839223137_note1819152144411"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="opengauss-0162__en-us_topic_0000001839223137_p72172194413">Only version 1.5.0 supports this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>By default, only a system administrator has the permission to protect resources. However, a system administrator can grant this permission to a data protection administrator.</p>
<p><strong>Note</strong></p>
<ul><li>Permissions on resources that have been associated with SLAs cannot be granted. Remove protection before authorization.</li><li>The permission on a single resource can be granted to only one data protection administrator.</li></ul>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target host or cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Authorize Resource</strong></span></b></span>, and select a data protection administrator for resource authorization.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><div class="p">Reclaiming resources<div class="note" id="opengauss-0162__en-us_topic_0000001839223137_note9135337194416"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="opengauss-0162__en-us_topic_0000001839223137_en-us_topic_0000001839223137_p72172194413">Only version 1.5.0 supports this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to reclaim resources authorized to a data protection administrator. The data protection administration is not permitted to protect the resources that have been reclaimed.</p>
<p><strong>Note</strong></p>
<p>Resources that have been associated with SLAs cannot be reclaimed. You need to remove the protection for the resources before reclaiming them.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target host or cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Reclaim Resource</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><div class="p">Adding a tag<div class="note" id="opengauss-0162__en-us_topic_0000001792344090_note161679211561"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="opengauss-0162__en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong id="opengauss-0162__en-us_topic_0000001792344090_b1495945913127">Scenario</strong></p>
<p>This operation enables you to quickly filter and manage resources.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><div class="p">Choose <span class="uicontrol" id="opengauss-0162__en-us_topic_0000001792344090_uicontrol84421111904"><b><span id="opengauss-0162__en-us_topic_0000001792344090_text6442710012"><strong>More</strong></span> &gt; Add Tag</b></span>.<div class="note" id="opengauss-0162__en-us_topic_0000001792344090_note164681316118"><span class="notetitle"> NOTE: </span><div class="notebody"><ul id="opengauss-0162__en-us_topic_0000001792344090_ul22969251338"><li id="opengauss-0162__en-us_topic_0000001792344090_li142961925139">On the displayed <strong id="opengauss-0162__en-us_topic_0000001792344090_b171967427617">Add Tag</strong> dialog box, you can select existing tags or click <strong id="opengauss-0162__en-us_topic_0000001792344090_b1711194810818">Create</strong> to create a tag.</li><li id="opengauss-0162__en-us_topic_0000001792344090_li135161927738">To modify or delete a tag, click <strong id="opengauss-0162__en-us_topic_0000001792344090_b17675195912164">Go to Tag Management</strong> on the <strong id="opengauss-0162__en-us_topic_0000001792344090_b13536183171711">Add Tag</strong> page to go to the <strong id="opengauss-0162__en-us_topic_0000001792344090_b1386161351713">Tag Management</strong> page.</li></ul>
</div></div>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.370000000000003%" headers="mcps1.3.4.2.4.1.1 "><div class="p">Removing a tag<div class="note" id="opengauss-0162__en-us_topic_0000001792344090_note4957114117575"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="opengauss-0162__en-us_topic_0000001792344090_en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="57.900000000000006%" headers="mcps1.3.4.2.4.1.2 "><p><strong id="opengauss-0162__en-us_topic_0000001792344090_b1495945913127_1">Scenario</strong></p>
<p>This operation enables you to remove the tag of a resource when it is no longer needed.</p>
<p></p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol" id="opengauss-0162__en-us_topic_0000001792344090_uicontrol4993205113111"><b><span id="opengauss-0162__en-us_topic_0000001792344090_text6993751201118"><strong>More</strong></span> &gt; Remove Tag</b></span>.</p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="opengauss-0159.html">openGauss Database Environments</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>