<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Step 4: Obtaining the VMware Certificate">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="vmware_gud_0015_0.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="vmware_gud_0023">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Step 4: Obtaining the VMware Certificate</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="vmware_gud_0023"></a><a name="vmware_gud_0023"></a>

<h1 class="topictitle1">Step 4: Obtaining the VMware Certificate</h1>
<div><p>When registering a VMware environment to be protected, you are advised to import the CA certificate of the environment to be protected. Otherwise, the system cannot verify the information about the devices to be accessed, resulting in security risks. This section describes how to obtain the CA certificate of a VMware environment to be protected.</p>
<div class="section" id="vmware_gud_0023__section96983519518"><h4 class="sectiontitle">Procedure</h4><ul id="vmware_gud_0023__ul9303155415576"><li id="vmware_gud_0023__li1030335414578">Method 1: Obtain it from the VMware administrator.</li><li id="vmware_gud_0023__li992313153581">Method 2: Download it by yourself.<ol id="vmware_gud_0023__ol91681531175316"><li id="vmware_gud_0023__li19220122517446">If the environment to be protected is an independent ESXi host, perform this step to check whether the ESXi host has been managed by a vCenter Server. Otherwise, skip this step.<div class="p" id="vmware_gud_0023__p1968462613446"><a name="vmware_gud_0023__li19220122517446"></a><a name="li19220122517446"></a>Assume that ESXi 8.0 is used.<ol type="a" id="vmware_gud_0023__ol176441450124011"><li id="vmware_gud_0023__en-us_topic_0000001656861689_li2024144995218">Log in to ESXi using a browser.<p id="vmware_gud_0023__en-us_topic_0000001656861689_p14208857125212"><a name="vmware_gud_0023__en-us_topic_0000001656861689_li2024144995218"></a><a name="en-us_topic_0000001656861689_li2024144995218"></a>Login address: <strong id="vmware_gud_0023__en-us_topic_0000001656861689_b3208205755214">https://</strong><em id="vmware_gud_0023__en-us_topic_0000001656861689_i1750152611591">ESXi management IP address</em></p>
</li><li id="vmware_gud_0023__li1964435084014">Choose <span class="uicontrol" id="vmware_gud_0023__uicontrol19313014217"><b>Host &gt; Manage &gt; Security &amp; users &gt; Certificates</b></span>, and view the <span class="uicontrol" id="vmware_gud_0023__uicontrol9350184211"><b>Issuer</b></span> parameter.<pre class="screen" id="vmware_gud_0023__screen456110100599">OU=VMware Engineering,O=<strong id="vmware_gud_0023__b191554574391">vcenter21.hw.com</strong>,ST=California,C=US,DC=local,DC=vsphere,CN=CA</pre>
<ul id="vmware_gud_0023__ul3207185834413"><li id="vmware_gud_0023__li4207165894412">If the value of <span class="uicontrol" id="vmware_gud_0023__uicontrol339323805812"><b>Issuer</b></span> contains a vCenter Server domain name (for example, <strong id="vmware_gud_0023__b15959217204511">vcenter21.hw.com</strong> in the preceding example), the ESXi host has been managed by the vCenter Server and the ESXi host certificate is issued by this vCenter Server. In this case, go to <a href="#vmware_gud_0023__li2083185225310">2</a>.</li><li id="vmware_gud_0023__li108845413492">If no vCenter Server domain name is contained in the value of <span class="uicontrol" id="vmware_gud_0023__uicontrol58135291494"><b>Issuer</b></span>, the ESXi host has never been managed by a vCenter Server. In this case, obtain the CA certificate from the VMware administrator and skip the subsequent steps.</li></ul>
</li></ol>
</div>
</li><li id="vmware_gud_0023__li2083185225310"><a name="vmware_gud_0023__li2083185225310"></a><a name="li2083185225310"></a>Access the address for logging in to the vCenter Server using a browser.<p id="vmware_gud_0023__p171383317558"><a name="vmware_gud_0023__li2083185225310"></a><a name="li2083185225310"></a>Login address: <strong id="vmware_gud_0023__b1846275114553">https://</strong><em id="vmware_gud_0023__i18830121311485">vCenter Server domain name</em></p>
</li><li id="vmware_gud_0023__li1764225610537">Click <strong id="vmware_gud_0023__b19365825131310">Download trusted root CA certificates</strong> on the right of the page to download the certificate package to a local directory.<p id="vmware_gud_0023__p1167811558116">If garbled characters are displayed in the browser during the download, you are advised to:</p>
<ul id="vmware_gud_0023__ul714785815113"><li id="vmware_gud_0023__li182638318124">Method 1: Use another browser (for example, Internet Explorer) for download.</li><li id="vmware_gud_0023__li4147558131118">Method 2: Right-click the blank area on the page with garbled characters and choose <strong id="vmware_gud_0023__b10925204319365">Save as</strong> from the shortcut menu to save the compressed certificate package to the local end.</li></ul>
</li><li id="vmware_gud_0023__li11651183217218">Decompress the certificate package and change the file name extension of the certificate as follows:<ul id="vmware_gud_0023__ul88521736142117"><li id="vmware_gud_0023__li1485273672114">For VMware vSphere 6.0, find the certificate file in <span class="uicontrol" id="vmware_gud_0023__uicontrol1560491417226"><b>*.0</b></span> format and change the certificate format to <span class="uicontrol" id="vmware_gud_0023__uicontrol135299177227"><b>.0.pem</b></span>.</li><li id="vmware_gud_0023__li132991823202211">For the VMware vSphere versions 6.5, 6.7, 7.<em id="vmware_gud_0023__i14295103381615">x</em>, and 8.0, find the certificate file in <span class="uicontrol" id="vmware_gud_0023__uicontrol788013373237"><b>*.crt</b></span> format and change the certificate format to <span class="uicontrol" id="vmware_gud_0023__uicontrol19246194116238"><b>.pem</b></span>.<div class="note" id="vmware_gud_0023__note1046113304573"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="vmware_gud_0023__p246173011572">If there are multiple <span class="uicontrol" id="vmware_gud_0023__uicontrol18192154717218"><b>*.crt</b></span> files, double-click a <span class="uicontrol" id="vmware_gud_0023__uicontrol5958172212313"><b>*.crt</b></span> file to open it. If the values of <span class="uicontrol" id="vmware_gud_0023__uicontrol111694221343"><b>Issued to</b></span> and <span class="uicontrol" id="vmware_gud_0023__uicontrol1074116240411"><b>Issued by</b></span> in the certificate information are <span class="uicontrol" id="vmware_gud_0023__uicontrol04846571944"><b>CA</b></span>, the <span class="uicontrol" id="vmware_gud_0023__uicontrol1180018121411"><b>*.crt</b></span> file is the desired one.</p>
</div></div>
</li></ul>
</li></ol>
</li></ul>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="vmware_gud_0015_0.html">Backing Up a VMware VM</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>