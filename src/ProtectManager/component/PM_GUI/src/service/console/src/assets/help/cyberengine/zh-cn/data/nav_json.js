naviData = [
  {
    id: 1,
    parentId: 0,
    name: '简介',
    local: 'zh-cn_topic_0000001783062250.html',
    children: [
      {
        id: 6,
        parentId: 1,
        name: '关于OceanCyber',
        local: 'zh-cn_topic_0000001829901645.html'
      },
      {
        id: 7,
        parentId: 1,
        name: '快速入门',
        local: 'zh-cn_topic_0000001783221938.html'
      },
      {
        id: 8,
        parentId: 1,
        name: '个人数据隐私声明',
        local: 'zh-cn_topic_0000001829901649.html'
      }
    ]
  },
  {
    id: 2,
    parentId: 0,
    name: '首页',
    local: 'zh-cn_topic_0000001829901609.html'
  },
  {
    id: 3,
    parentId: 0,
    name: '数据安全',
    local: 'zh-cn_topic_0000001783221934.html',
    children: [
      {
        id: 9,
        parentId: 3,
        name: '添加存储设备',
        local: 'zh-cn_topic_0000001829899765.html'
      },
      {
        id: 10,
        parentId: 3,
        name: '（可选）配置网络',
        local: 'zh-cn_topic_0000001783220034.html'
      },
      {
        id: 11,
        parentId: 3,
        name: '配置勒索病毒防护',
        local: 'zh-cn_topic_0000001867860069.html',
        children: [
          {
            id: 20,
            parentId: 11,
            name: '配置文件拦截（事前拦截）',
            local: 'zh-cn_topic_0000001783060330.html',
            children: [
              {
                id: 23,
                parentId: 20,
                name: '创建文件扩展名过滤规则',
                local: 'zh-cn_topic_0000001783060402.html'
              },
              {
                id: 24,
                parentId: 20,
                name: '文件扩展名过滤规则关联文件系统',
                local: 'zh-cn_topic_0000001829899749.html'
              },
              {
                id: 25,
                parentId: 20,
                name: '更新文件扩展名过滤规则',
                local: 'zh-cn_topic_0000002085819285.html'
              },
              {
                id: 26,
                parentId: 20,
                name: '扫描文件系统',
                local: 'zh-cn_topic_0000001829899793.html'
              },
              {
                id: 27,
                parentId: 20,
                name: '开启文件拦截',
                local: 'zh-cn_topic_0000001783220050.html'
              }
            ]
          },
          {
            id: 21,
            parentId: 11,
            name: '配置实时侦测（事中拦截）',
            local: 'zh-cn_topic_0000001810100230.html',
            children: [
              {
                id: 28,
                parentId: 21,
                name: '创建实时侦测策略',
                local: 'zh-cn_topic_0000001856698993.html'
              },
              {
                id: 29,
                parentId: 21,
                name: '（可选）配置白名单',
                local: 'zh-cn_topic_0000001856779037.html'
              },
              {
                id: 30,
                parentId: 21,
                name: '执行实时侦测',
                local: 'zh-cn_topic_0000001809940362.html'
              }
            ]
          },
          {
            id: 22,
            parentId: 11,
            name: '配置智能侦测（事后拦截）',
            local: 'zh-cn_topic_0000001829819561.html',
            children: [
              {
                id: 31,
                parentId: 22,
                name: '创建智能侦测策略',
                local: 'zh-cn_topic_0000001829819617.html'
              },
              {
                id: 32,
                parentId: 22,
                name: '执行智能侦测',
                local: 'zh-cn_topic_0000001829899605.html',
                children: [
                  {
                    id: 33,
                    parentId: 32,
                    name: '周期性执行侦测',
                    local: 'zh-cn_topic_0000001783060350.html'
                  },
                  {
                    id: 34,
                    parentId: 32,
                    name: '手动执行侦测',
                    local: 'zh-cn_topic_0000001783220022.html'
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        id: 12,
        parentId: 3,
        name: '配置Air Gap',
        local: 'zh-cn_topic_0000001783060154.html',
        children: [
          {
            id: 35,
            parentId: 12,
            name: '创建Air Gap策略',
            local: 'zh-cn_topic_0000001783060314.html'
          },
          {
            id: 36,
            parentId: 12,
            name: '关联Air Gap策略',
            local: 'zh-cn_topic_0000001829899653.html'
          }
        ]
      },
      {
        id: 13,
        parentId: 3,
        name: '查看数据安全报告',
        local: 'zh-cn_topic_0000001783219966.html',
        children: [
          {
            id: 37,
            parentId: 13,
            name: '添加报告',
            local: 'zh-cn_topic_0000001829819765.html'
          },
          {
            id: 38,
            parentId: 13,
            name: '查看报告',
            local: 'zh-cn_topic_0000001783220082.html'
          }
        ]
      },
      {
        id: 14,
        parentId: 3,
        name: '总览勒索侦测报表',
        local: 'zh-cn_topic_0000001829899581.html'
      },
      {
        id: 15,
        parentId: 3,
        name: '恢复快照数据',
        local: 'zh-cn_topic_0000001783060378.html'
      },
      {
        id: 16,
        parentId: 3,
        name: '管理勒索病毒防护',
        local: 'zh-cn_topic_0000001821100600.html',
        children: [
          {
            id: 39,
            parentId: 16,
            name: '管理文件拦截（事前拦截）',
            local: 'zh-cn_topic_0000001829819717.html',
            children: [
              {
                id: 43,
                parentId: 39,
                name: '文件扩展名过滤规则解除关联文件系统',
                local: 'zh-cn_topic_0000001783060398.html'
              },
              {
                id: 44,
                parentId: 39,
                name: '删除文件扩展名过滤规则',
                local: 'zh-cn_topic_0000001783220086.html'
              },
              {
                id: 45,
                parentId: 39,
                name: '更新文件扩展名过滤规则',
                local: 'zh-cn_topic_0000001829899781.html'
              },
              {
                id: 46,
                parentId: 39,
                name: '禁用文件拦截',
                local: 'zh-cn_topic_0000001829819801.html'
              }
            ]
          },
          {
            id: 40,
            parentId: 16,
            name: '管理实时侦测（事中拦截）',
            local: 'zh-cn_topic_0000001810100234.html',
            children: [
              {
                id: 47,
                parentId: 40,
                name: '管理实时侦测策略',
                local: 'zh-cn_topic_0000001809940366.html'
              },
              {
                id: 48,
                parentId: 40,
                name: '管理白名单',
                local: 'zh-cn_topic_0000001856779041.html'
              },
              {
                id: 49,
                parentId: 40,
                name: '管理文件系统保护计划',
                local: 'zh-cn_topic_0000001856698997.html'
              }
            ]
          },
          {
            id: 41,
            parentId: 16,
            name: '管理智能侦测（事后拦截）',
            local: 'zh-cn_topic_0000001783060278.html',
            children: [
              {
                id: 50,
                parentId: 41,
                name: '查看文件系统信息',
                local: 'zh-cn_topic_0000001829819809.html'
              },
              {
                id: 51,
                parentId: 41,
                name: '管理文件系统保护计划',
                local: 'zh-cn_topic_0000001783060366.html'
              },
              {
                id: 52,
                parentId: 41,
                name: '管理智能侦测策略',
                local: 'zh-cn_topic_0000001783220090.html'
              }
            ]
          },
          {
            id: 42,
            parentId: 16,
            name: '管理侦测模型',
            local: 'zh-cn_topic_0000001829899789.html'
          }
        ]
      },
      {
        id: 17,
        parentId: 3,
        name: '管理Air Gap',
        local: 'zh-cn_topic_0000001867944245.html',
        children: [
          {
            id: 53,
            parentId: 17,
            name: '管理存储设备',
            local: 'zh-cn_topic_0000001783060298.html',
            children: [
              {
                id: 55,
                parentId: 53,
                name: '查看存储设备',
                local: 'zh-cn_topic_0000001829899737.html'
              },
              {
                id: 56,
                parentId: 53,
                name: '修改存储设备关联的Air Gap策略',
                local: 'zh-cn_topic_0000001783220094.html'
              },
              {
                id: 57,
                parentId: 53,
                name: '移除存储设备关联的Air Gap策略',
                local: 'zh-cn_topic_0000001829819793.html'
              },
              {
                id: 58,
                parentId: 53,
                name: '开启存储设备关联的Air Gap策略',
                local: 'zh-cn_topic_0000001829819821.html'
              },
              {
                id: 59,
                parentId: 53,
                name: '关闭存储设备关联的Air Gap策略',
                local: 'zh-cn_topic_0000001829899797.html'
              }
            ]
          },
          {
            id: 54,
            parentId: 17,
            name: '管理Air Gap策略',
            local: 'zh-cn_topic_0000001783219866.html'
          }
        ]
      },
      {
        id: 18,
        parentId: 3,
        name: '管理快照数据',
        local: 'zh-cn_topic_0000001783219918.html',
        children: [
          {
            id: 60,
            parentId: 18,
            name: '查看快照数据',
            local: 'zh-cn_topic_0000001829819673.html'
          },
          {
            id: 61,
            parentId: 18,
            name: '查看侦测结果',
            local: 'zh-cn_topic_0000001829819813.html'
          },
          {
            id: 62,
            parentId: 18,
            name: '误报处理',
            local: 'zh-cn_topic_0000001783220070.html'
          }
        ]
      },
      {
        id: 19,
        parentId: 3,
        name: '管理数据安全报告',
        local: 'zh-cn_topic_0000001829899529.html',
        children: [
          {
            id: 63,
            parentId: 19,
            name: '删除报告',
            local: 'zh-cn_topic_0000001829819781.html'
          }
        ]
      }
    ]
  },
  {
    id: 4,
    parentId: 0,
    name: '监控',
    local: 'zh-cn_topic_0000001829901625.html',
    children: [
      {
        id: 64,
        parentId: 4,
        name: '管理告警和事件信息',
        local: 'zh-cn_topic_0000001829821661.html'
      },
      {
        id: 65,
        parentId: 4,
        name: '管理任务',
        local: 'zh-cn_topic_0000001829821681.html',
        children: [
          {
            id: 66,
            parentId: 65,
            name: '查看任务进度',
            local: 'zh-cn_topic_0000001829821677.html'
          },
          {
            id: 67,
            parentId: 65,
            name: '停止任务',
            local: 'zh-cn_topic_0000001829821649.html'
          },
          {
            id: 68,
            parentId: 65,
            name: '下载任务',
            local: 'zh-cn_topic_0000001829821665.html'
          }
        ]
      }
    ]
  },
  {
    id: 5,
    parentId: 0,
    name: '系统',
    local: 'zh-cn_topic_0000001783062254.html',
    children: [
      {
        id: 69,
        parentId: 5,
        name: '管理用户',
        local: 'zh-cn_topic_0000001879127937.html',
        children: [
          {
            id: 78,
            parentId: 69,
            name: '用户角色介绍',
            local: 'zh-cn_topic_0000001832328588.html'
          },
          {
            id: 79,
            parentId: 69,
            name: '创建用户',
            local: 'zh-cn_topic_0000001879247733.html'
          },
          {
            id: 80,
            parentId: 69,
            name: '修改用户',
            local: 'zh-cn_topic_0000001832488404.html'
          },
          {
            id: 81,
            parentId: 69,
            name: '锁定用户',
            local: 'zh-cn_topic_0000001879127941.html'
          },
          {
            id: 82,
            parentId: 69,
            name: '解锁用户',
            local: 'zh-cn_topic_0000001832328592.html'
          },
          {
            id: 83,
            parentId: 69,
            name: '移除用户',
            local: 'zh-cn_topic_0000001879247737.html'
          },
          {
            id: 84,
            parentId: 69,
            name: '重置用户密码',
            local: 'zh-cn_topic_0000001832488408.html'
          },
          {
            id: 85,
            parentId: 69,
            name: '管理密码找回邮箱',
            local: 'zh-cn_topic_0000001879127945.html'
          }
        ]
      },
      {
        id: 70,
        parentId: 5,
        name: '管理安全策略',
        local: 'zh-cn_topic_0000001832328596.html'
      },
      {
        id: 71,
        parentId: 5,
        name: '管理证书',
        local: 'zh-cn_topic_0000001879247741.html',
        children: [
          {
            id: 86,
            parentId: 71,
            name: '查看证书信息',
            local: 'zh-cn_topic_0000001832488412.html'
          },
          {
            id: 87,
            parentId: 71,
            name: '添加外部证书',
            local: 'zh-cn_topic_0000001879127949.html'
          },
          {
            id: 88,
            parentId: 71,
            name: '导入证书',
            local: 'zh-cn_topic_0000001832328600.html'
          },
          {
            id: 89,
            parentId: 71,
            name: '导出请求文件',
            local: 'zh-cn_topic_0000001879247749.html'
          },
          {
            id: 90,
            parentId: 71,
            name: '修改证书过期告警',
            local: 'zh-cn_topic_0000001832488416.html'
          },
          {
            id: 91,
            parentId: 71,
            name: '管理证书吊销列表',
            local: 'zh-cn_topic_0000001879127953.html',
            children: [
              {
                id: 96,
                parentId: 91,
                name: '导入证书吊销列表',
                local: 'zh-cn_topic_0000001832328604.html'
              },
              {
                id: 97,
                parentId: 91,
                name: '查看证书吊销列表',
                local: 'zh-cn_topic_0000001879247753.html'
              },
              {
                id: 98,
                parentId: 91,
                name: '下载证书吊销列表',
                local: 'zh-cn_topic_0000001832488424.html'
              },
              {
                id: 99,
                parentId: 91,
                name: '删除证书吊销列表',
                local: 'zh-cn_topic_0000001879127957.html'
              }
            ]
          },
          {
            id: 92,
            parentId: 71,
            name: '下载证书',
            local: 'zh-cn_topic_0000001832328608.html'
          },
          {
            id: 93,
            parentId: 71,
            name: '删除外部证书',
            local: 'zh-cn_topic_0000001879247757.html'
          },
          {
            id: 94,
            parentId: 71,
            name: '替换服务端OceanCyber 300 数据安全一体机的SSL证书',
            local: 'zh-cn_topic_0000001832488428.html'
          },
          {
            id: 95,
            parentId: 71,
            name: '在终端浏览器中导入安全证书',
            local: 'zh-cn_topic_0000001879127961.html'
          }
        ]
      },
      {
        id: 72,
        parentId: 5,
        name: '管理日志',
        local: 'zh-cn_topic_0000001832328616.html'
      },
      {
        id: 73,
        parentId: 5,
        name: '管理系统数据备份',
        local: 'zh-cn_topic_0000001879247761.html',
        children: [
          {
            id: 100,
            parentId: 73,
            name: '配置管理数据备份',
            local: 'zh-cn_topic_0000001832488432.html'
          },
          {
            id: 101,
            parentId: 73,
            name: '导出管理数据备份',
            local: 'zh-cn_topic_0000001879127965.html'
          },
          {
            id: 102,
            parentId: 73,
            name: '删除管理数据备份',
            local: 'zh-cn_topic_0000001832328620.html'
          },
          {
            id: 103,
            parentId: 73,
            name: '导入管理数据备份',
            local: 'zh-cn_topic_0000001879247765.html'
          },
          {
            id: 104,
            parentId: 73,
            name: '恢复管理数据',
            local: 'zh-cn_topic_0000001832488436.html',
            children: [
              {
                id: 105,
                parentId: 104,
                name: '恢复当前系统管理数据',
                local: 'zh-cn_topic_0000001879127973.html'
              },
              {
                id: 106,
                parentId: 104,
                name: '系统重装后恢复管理数据',
                local: 'zh-cn_topic_0000001832328628.html'
              }
            ]
          }
        ]
      },
      {
        id: 74,
        parentId: 5,
        name: '管理事件转储',
        local: 'zh-cn_topic_0000001879247769.html'
      },
      {
        id: 75,
        parentId: 5,
        name: '管理SNMP Trap通知',
        local: 'zh-cn_topic_0000001832488444.html'
      },
      {
        id: 76,
        parentId: 5,
        name: '配置iBMC时间',
        local: 'zh-cn_topic_0000001832329248.html'
      },
      {
        id: 77,
        parentId: 5,
        name: '管理告警通知',
        local: 'zh-cn_topic_0000001879127977.html',
        children: [
          {
            id: 107,
            parentId: 77,
            name: '发件设置',
            local: 'zh-cn_topic_0000002077228765.html'
          },
          {
            id: 108,
            parentId: 77,
            name: 'Syslog通知',
            local: 'zh-cn_topic_0000002077110145.html'
          }
        ]
      }
    ]
  }
];
topLanguage = 'zh';
topMainPage = 'zh-cn_topic_0000001783062250.html';
