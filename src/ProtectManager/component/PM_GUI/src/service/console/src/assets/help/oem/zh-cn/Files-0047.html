<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-cn" xml:lang="zh-cn">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="即时挂载文件集">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="Files-0047">
  <meta name="DC.Language" content="zh-cn">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>即时挂载文件集</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="Files-0047"></a><a name="Files-0047"></a>
  <h1 class="topictitle1">即时挂载文件集</h1>
  <div>
   <p>通过即时挂载功能，可将文件集副本挂载给使用数据的目标主机。</p>
   <div class="section">
    <h4 class="sectiontitle">前提条件</h4>
    <p>挂载至的目标主机已安装ProtectAgent及其他相关软件。</p>
    <p>具体操作可参考<span>《OceanProtect DataBackup 1.5.0-1.6.0 ProtectAgent安装指南》</span>。</p>
   </div>
   <div class="section">
    <h4 class="sectiontitle">操作步骤</h4>
    <ol>
     <li><span>选择<span class="uicontrol" id="Files-0047__zh-cn_topic_0000001839142377_uicontrol81551121183717">“<span id="Files-0047__zh-cn_topic_0000001839142377_text91553217374">数据利用</span> &gt; <span id="Files-0047__zh-cn_topic_0000001839142377_text101552021103718">即时挂载</span> &gt; <span id="Files-0047__zh-cn_topic_0000001839142377_text42471249173719">文件集</span>”</span>。</span></li>
     <li><span>单击<span class="uicontrol">“<span>创建</span>”</span>。</span></li>
     <li><span>在<span class="uicontrol">“<span>选择资源</span>”</span>页签，通过搜索或选择要挂载的副本对应的资源，单击<span class="uicontrol">“<span>下一步</span>”</span>。</span></li>
     <li><span>在<span class="uicontrol">“<span>选择副本</span>”</span>页签执行如下操作：</span><p></p>
      <ol type="a">
       <li>选择要挂载的副本。
        <ul>
         <li>创建即时挂载的副本必须是<span class="uicontrol">“<span>正常</span>”</span>状态。</li>
         <li>开启小文件聚合的副本不支持即时挂载。</li>
        </ul></li>
       <li>选择已创建的挂载更新策略。<p>如果未在此处选择挂载更新策略，后续可<a href="Files-0051.html">通过修改操作配置挂载更新策略</a>。</p></li>
       <li>单击<span class="uicontrol">“<span>下一步</span>”</span>。</li>
      </ol> <p></p></li>
     <li><span>在<span class="uicontrol">“<span>挂载选项</span>”</span>页签，配置挂载参数，单击<span class="uicontrol">“<span>下一步</span>”</span>。</span><p></p>
      <ol type="a">
       <li>配置即时挂载基本参数和高级参数。
        <div class="p">
         相关参数说明如<a href="#Files-0047__zh-cn_topic_0000001387707949_table89615043315">表1</a>所示。
         <div class="note">
          <img src="public_sys-resources/note_3.0-zh-cn.png"><span class="notetitle"> </span>
          <div class="notebody">
           <ul>
            <li>带宽与IOPS可以任意配置一个或者两个都配置，如果是带宽型业务，建议只配置带宽，如果是IOPS型业务，建议只配置IOPS，如果不确定或者有时是带宽型、有时是IOPS型，则建议两个都配置，此时流量控制以小的一个为准。</li>
            <li>标准IOPS(8KB)与真实IO模型的换算关系可以通过界面上的换算表查看。</li>
            <li>通过限制挂载副本的最大流量，以保证备份、恢复任务的性能；设置挂载副本的下限保障，以保证挂载副本的最低性能。</li>
           </ul>
          </div>
         </div>
         <div class="tablenoborder">
          <a name="Files-0047__zh-cn_topic_0000001387707949_table89615043315"></a><a name="zh-cn_topic_0000001387707949_table89615043315"></a>
          <table cellpadding="4" cellspacing="0" summary="" id="Files-0047__zh-cn_topic_0000001387707949_table89615043315" frame="border" border="1" rules="all">
           <caption>
            <b>表1 </b>即时挂载参数说明
           </caption>
           <colgroup>
            <col style="width:16.439999999999998%">
            <col style="width:20.78%">
            <col style="width:62.78%">
           </colgroup>
           <thead align="left">
            <tr>
             <th align="left" class="cellrowborder" colspan="2" valign="top" id="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.1"><p>参数</p></th>
             <th align="left" class="cellrowborder" valign="top" id="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.2"><p>说明</p></th>
            </tr>
           </thead>
           <tbody>
            <tr>
             <td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.1 "><p><span>目标主机</span></p></td>
             <td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.2 "><p>挂载到的目标主机。</p> <p>可通过搜索主机名称或主机IP地址选择相应主机。</p></td>
            </tr>
            <tr>
             <td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.1 "><p>CIFS共享名</p> <p></p></td>
             <td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.2 "><p>访问文件系统的共享名称。</p> <p>默认共享名称为mount_<em>时间戳</em>，您也可以修改共享名称。</p> <p><span style="color:#282B33;">请您确认备份资源支持CIFS共享协议，否则挂载时将不包含CIFS文件权限信息。</span></p>
              <div class="note">
               <span class="notetitle"> 说明： </span>
               <div class="notebody">
                <p>仅Windows操作系统显示该参数。</p>
               </div>
              </div></td>
            </tr>
            <tr>
             <td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.1 "><p>类型</p></td>
             <td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.2 "><p>访问CIFS共享的用户类型。</p>
              <ul>
               <li><span>Everyone</span></li>
               <li><span>Windows本地认证用户</span></li>
               <li><span>Windows本地认证用户组</span></li>
              </ul>
              <div class="note">
               <span class="notetitle"> 说明： </span>
               <div class="notebody">
                <p>仅Windows操作系统显示该参数。</p>
               </div>
              </div></td>
            </tr>
            <tr>
             <td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.1 "><p>用户</p></td>
             <td class="cellrowborder" valign="top" headers="mcps1.3.3.2.5.2.1.1.1.3.2.4.1.2 "><p>选择需要的用户或用户组。</p>
              <div class="note">
               <span class="notetitle"> 说明： </span>
               <div class="notebody">
                <ul>
                 <li>如果不存在需要的用户或用户组，请到DeviceManager界面创建。</li>
                 <li>仅Windows操作系统显示该参数。</li>
                </ul>
               </div>
              </div></td>
            </tr>
           </tbody>
          </table>
         </div>
        </div></li>
       <li>单击<span class="uicontrol">“<span>下一步</span>”</span>。</li>
      </ol> <p></p></li>
     <li><span>预览挂载参数，确认无误后，单击<span class="uicontrol">“<span>完成</span>”</span>。</span><p></p>
      <div class="note">
       <img src="public_sys-resources/note_3.0-zh-cn.png"><span class="notetitle"> </span>
       <div class="notebody">
        <p>副本生成后立即用该副本执行即时挂载，可能会由于副本仍处于初始化状态而导致挂载任务失败，请3分钟后重试。</p>
       </div>
      </div> <p></p></li>
    </ol>
   </div>
  </div>
 </body>
</html>