<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Step 2: (Optional) Generating a Token with the Minimum Permissions">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="kubernetes_CSI_00010.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="kubernetes_CSI_00077">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Step 2: (Optional) Generating a Token with the Minimum Permissions</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="kubernetes_CSI_00077"></a><a name="kubernetes_CSI_00077"></a>
  <h1 class="topictitle1">Step 2: (Optional) Generating a Token with the Minimum Permissions</h1>
  <div>
   <p>You need to perform the operations in this section to generate a token with the minimum permissions only when token authentication is selected during subsequent Kubernetes cluster registration. If there is no special requirement, the token with the super administrator permissions can be used.</p>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li id="kubernetes_CSI_00077__li6288174805511"><a name="kubernetes_CSI_00077__li6288174805511"></a><a name="li6288174805511"></a><span>Query the EIPs bound to worker nodes in the CCE cluster.</span><p></p>
      <ol type="a">
       <li>Log in to ManageOne Operation Portal.</li>
       <li>Choose <span class="uicontrol"><b>Service List &gt; Cloud Container Engine &gt; CCE cluster</b></span>. Select the target Kubernetes cluster name to go to the cluster information page.</li>
       <li>Choose <span class="uicontrol"><b>Cluster &gt; Nodes</b></span>, and click the <span class="uicontrol"><b>Nodes</b></span> tab to view the EIPs bound to all worker nodes in the CCE cluster.</li>
      </ol> <p></p></li>
     <li><span>Log in to any node in the CCE cluster using the EIP obtained in <a href="#kubernetes_CSI_00077__li6288174805511">1</a>.</span></li>
     <li><span>Create cluster role resources. In the Kubernetes production cluster, create the <strong>pm-k8s-sa-role.yaml</strong> file and write the following content to the file.</span><p></p><pre class="screen">apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: pm-k8s-sa-role
rules:
  - apiGroups: [ "*" ]
    resources: [ "*" ]
    verbs: [ "list", "create", "get" ]
  - apiGroups: [ "*" ]
    resources: [ "pods", "persistentvolumeclaims", "volumesnapshots" ]
    verbs: [ "delete" ]
  - apiGroups: [ "*" ]
    resources: [ "statefulsets", "replicasets", "deployments", "daemonsets", "jobs", "cronjobs" ]
    verbs: [ "patch" ]</pre> <p></p></li>
     <li><span>On the background page of the Kubernetes cluster, run the <strong>kubectl apply -f pm-k8s-sa-role.yaml</strong> command to create <strong>ClusterRole</strong>.</span></li>
     <li><span>Create a service account. In the Kubernetes production cluster, create the <strong>pm-k8s-sa.yaml</strong> file and write the following content to the file.</span><p></p><pre class="screen">apiVersion: v1
kind: ServiceAccount
metadata:
  name: pm-k8s-sa
  namespace: kube-system</pre> <p></p></li>
     <li><span>On the background page of the Kubernetes cluster, run the <strong>kubectl apply -f pm-k8s-sa.yaml</strong> command to create <strong>ServiceAccount</strong>.</span></li>
     <li><span>Create <strong>ClusterRoleBinding</strong>. In the Kubernetes production cluster, create the <strong>pm-k8s-cluster-role-binding.yaml</strong> file and write the following content to the file.</span><p></p><pre class="screen">apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: pm-k8s-cluster-role-binding
subjects:
- kind: ServiceAccount
  name: pm-k8s-sa      
  namespace: kube-system
roleRef:
  kind: ClusterRole
  name: pm-k8s-sa-role
  apiGroup: rbac.authorization.k8s.io</pre> <p></p></li>
     <li><span>On the background page of the Kubernetes cluster, run the <strong>kubectl apply -f pm-k8s-cluster-role-binding.yaml</strong> command to create <strong>ClusterRoleBinding</strong>.</span></li>
     <li><span>Create <strong>Secret</strong> to save the token information. In the Kubernetes production cluster, create the <strong>pm-k8s-secret.yaml</strong> file and write the following content to the file.</span><p></p><pre class="screen">apiVersion: v1
kind: Secret
type: kubernetes.io/service-account-token
metadata:
  namespace: kube-system
  name: pm-k8s-secret
  annotations:
    kubernetes.io/service-account.name: pm-k8s-sa</pre> <p></p></li>
     <li><span>On the background page of the Kubernetes cluster, run the <strong>kubectl apply -f pm-k8s-secret.yaml</strong> command to create <strong>Secret</strong>.</span></li>
     <li><span>Obtain the token with the minimum permissions. In the production cluster, run the <strong>kubectl describe secret/pm-k8s-secret -n kube-system</strong> command. The value of the token field in the command output is the required token with the minimum permissions.</span></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="kubernetes_CSI_00010.html">Backing Up Namespaces or Datasets</a>
    </div>
   </div>
  </div>
 </body>
</html>