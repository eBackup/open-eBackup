naviData = [
  {
    id: 1,
    parentId: 0,
    name: 'Overview',
    local: 'en-us_topic_0000001783062250.html',
    children: [
      {
        id: 6,
        parentId: 1,
        name: 'About OceanCyber',
        local: 'en-us_topic_0000001829901645.html'
      },
      {
        id: 7,
        parentId: 1,
        name: 'Quick Start',
        local: 'en-us_topic_0000001783221938.html'
      },
      {
        id: 8,
        parentId: 1,
        name: 'Personal Data Privacy Statement',
        local: 'en-us_topic_0000001829901649.html'
      }
    ]
  },
  {
    id: 2,
    parentId: 0,
    name: 'Home Page',
    local: 'en-us_topic_0000001829901609.html'
  },
  {
    id: 3,
    parentId: 0,
    name: 'Data Security',
    local: 'en-us_topic_0000001783221934.html',
    children: [
      {
        id: 9,
        parentId: 3,
        name: 'Adding Storage Devices',
        local: 'en-us_topic_0000001829899765.html'
      },
      {
        id: 10,
        parentId: 3,
        name: '(Optional) Configuring the Network',
        local: 'en-us_topic_0000001783220034.html'
      },
      {
        id: 11,
        parentId: 3,
        name: 'Configuring Ransomware Protection',
        local: 'en-us_topic_0000001867860069.html',
        children: [
          {
            id: 20,
            parentId: 11,
            name: 'Configuring File Interception (Pre-event Interception)',
            local: 'en-us_topic_0000001783060330.html',
            children: [
              {
                id: 23,
                parentId: 20,
                name: 'Creating a File Name Extension Filtering Rule',
                local: 'en-us_topic_0000001783060402.html'
              },
              {
                id: 24,
                parentId: 20,
                name:
                  'Associating a File Name Extension Filtering Rule with a File System',
                local: 'en-us_topic_0000001829899749.html'
              },
              {
                id: 25,
                parentId: 20,
                name: 'Updating a File Name Extension Filtering Rule',
                local: 'en-us_topic_0000002085819285.html'
              },
              {
                id: 26,
                parentId: 20,
                name: 'Scanning for File Systems',
                local: 'en-us_topic_0000001829899793.html'
              },
              {
                id: 27,
                parentId: 20,
                name: 'Enabling File Interception',
                local: 'en-us_topic_0000001783220050.html'
              }
            ]
          },
          {
            id: 21,
            parentId: 11,
            name: 'Configuring Real-Time Detection (In-event Interception)',
            local: 'en-us_topic_0000001810100230.html',
            children: [
              {
                id: 28,
                parentId: 21,
                name: 'Creating a Real-Time Detection Policy',
                local: 'en-us_topic_0000001856698993.html'
              },
              {
                id: 29,
                parentId: 21,
                name: '(Optional) Configuring a Whitelist',
                local: 'en-us_topic_0000001856779037.html'
              },
              {
                id: 30,
                parentId: 21,
                name: 'Performing Real-Time Detection',
                local: 'en-us_topic_0000001809940362.html'
              }
            ]
          },
          {
            id: 22,
            parentId: 11,
            name: 'Configuring Intelligent Detection (Post-event Interception)',
            local: 'en-us_topic_0000001829819561.html',
            children: [
              {
                id: 31,
                parentId: 22,
                name: 'Creating an Intelligent Detection Policy',
                local: 'en-us_topic_0000001829819617.html'
              },
              {
                id: 32,
                parentId: 22,
                name: 'Performing Intelligent Detection',
                local: 'en-us_topic_0000001829899605.html',
                children: [
                  {
                    id: 33,
                    parentId: 32,
                    name: 'Performing a Detection Periodically',
                    local: 'en-us_topic_0000001783060350.html'
                  },
                  {
                    id: 34,
                    parentId: 32,
                    name: 'Performing a Detection Manually',
                    local: 'en-us_topic_0000001783220022.html'
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        id: 12,
        parentId: 3,
        name: 'Configuring Air Gap',
        local: 'en-us_topic_0000001783060154.html',
        children: [
          {
            id: 35,
            parentId: 12,
            name: 'Creating an Air Gap Policy',
            local: 'en-us_topic_0000001783060314.html'
          },
          {
            id: 36,
            parentId: 12,
            name: 'Associating an Air Gap Policy',
            local: 'en-us_topic_0000001829899653.html'
          }
        ]
      },
      {
        id: 13,
        parentId: 3,
        name: 'Viewing a Data Security Report',
        local: 'en-us_topic_0000001783219966.html',
        children: [
          {
            id: 37,
            parentId: 13,
            name: 'Adding a Report',
            local: 'en-us_topic_0000001829819765.html'
          },
          {
            id: 38,
            parentId: 13,
            name: 'Viewing a Report',
            local: 'en-us_topic_0000001783220082.html'
          }
        ]
      },
      {
        id: 14,
        parentId: 3,
        name: 'Ransomware Detection Report Overview',
        local: 'en-us_topic_0000001829899581.html'
      },
      {
        id: 15,
        parentId: 3,
        name: 'Restoring Data Using a Snapshot',
        local: 'en-us_topic_0000001783060378.html'
      },
      {
        id: 16,
        parentId: 3,
        name: 'Managing Ransomware Protection',
        local: 'en-us_topic_0000001821100600.html',
        children: [
          {
            id: 39,
            parentId: 16,
            name: 'Managing File Interception (Pre-event Interception)',
            local: 'en-us_topic_0000001829819717.html',
            children: [
              {
                id: 43,
                parentId: 39,
                name:
                  'File Name Extension Filtering Rule Disassociated From A File System',
                local: 'en-us_topic_0000001783060398.html'
              },
              {
                id: 44,
                parentId: 39,
                name: 'Deleting a File Name Extension Filtering Rule',
                local: 'en-us_topic_0000001783220086.html'
              },
              {
                id: 45,
                parentId: 39,
                name: 'Updating a File Name Extension Filtering Rule',
                local: 'en-us_topic_0000001829899781.html'
              },
              {
                id: 46,
                parentId: 39,
                name: 'Disabling File Interception',
                local: 'en-us_topic_0000001829819801.html'
              }
            ]
          },
          {
            id: 40,
            parentId: 16,
            name: 'Managing Real-Time Detection (In-event Interception)',
            local: 'en-us_topic_0000001810100234.html',
            children: [
              {
                id: 47,
                parentId: 40,
                name: 'Managing Real-Time Detection Policies',
                local: 'en-us_topic_0000001809940366.html'
              },
              {
                id: 48,
                parentId: 40,
                name: 'Managing a Whitelist',
                local: 'en-us_topic_0000001856779041.html'
              },
              {
                id: 49,
                parentId: 40,
                name: 'Managing Protection Plans for File Systems',
                local: 'en-us_topic_0000001856698997.html'
              }
            ]
          },
          {
            id: 41,
            parentId: 16,
            name: 'Managing Intelligent Detection (Post-event Interception)',
            local: 'en-us_topic_0000001783060278.html',
            children: [
              {
                id: 50,
                parentId: 41,
                name: 'Viewing File System Information',
                local: 'en-us_topic_0000001829819809.html'
              },
              {
                id: 51,
                parentId: 41,
                name: 'Managing Protection Plans for File Systems',
                local: 'en-us_topic_0000001783060366.html'
              },
              {
                id: 52,
                parentId: 41,
                name: 'Managing Intelligent Detection Policies',
                local: 'en-us_topic_0000001783220090.html'
              }
            ]
          },
          {
            id: 42,
            parentId: 16,
            name: 'Managing Detection Models',
            local: 'en-us_topic_0000001829899789.html'
          }
        ]
      },
      {
        id: 17,
        parentId: 3,
        name: 'Managing Air Gap',
        local: 'en-us_topic_0000001867944245.html',
        children: [
          {
            id: 53,
            parentId: 17,
            name: 'Managing Storage Devices',
            local: 'en-us_topic_0000001783060298.html',
            children: [
              {
                id: 55,
                parentId: 53,
                name: 'Viewing Storage Devices',
                local: 'en-us_topic_0000001829899737.html'
              },
              {
                id: 56,
                parentId: 53,
                name:
                  'Modifying the Air Gap Policy Associated with a Storage Device',
                local: 'en-us_topic_0000001783220094.html'
              },
              {
                id: 57,
                parentId: 53,
                name:
                  'Removing the Air Gap Policy Associated with a Storage Device',
                local: 'en-us_topic_0000001829819793.html'
              },
              {
                id: 58,
                parentId: 53,
                name:
                  'Enabling the Air Gap Policy Associated with a Storage Device',
                local: 'en-us_topic_0000001829819821.html'
              },
              {
                id: 59,
                parentId: 53,
                name:
                  'Disabling the Air Gap Policy Associated with a Storage Device',
                local: 'en-us_topic_0000001829899797.html'
              }
            ]
          },
          {
            id: 54,
            parentId: 17,
            name: 'Managing Air Gap Policies',
            local: 'en-us_topic_0000001783219866.html'
          }
        ]
      },
      {
        id: 18,
        parentId: 3,
        name: 'Managing Snapshot Data',
        local: 'en-us_topic_0000001783219918.html',
        children: [
          {
            id: 60,
            parentId: 18,
            name: 'Viewing Snapshot Data',
            local: 'en-us_topic_0000001829819673.html'
          },
          {
            id: 61,
            parentId: 18,
            name: 'Viewing Detection Results',
            local: 'en-us_topic_0000001829819813.html'
          },
          {
            id: 62,
            parentId: 18,
            name: 'Handling False Detection',
            local: 'en-us_topic_0000001783220070.html'
          }
        ]
      },
      {
        id: 19,
        parentId: 3,
        name: 'Managing Data Security Reports',
        local: 'en-us_topic_0000001829899529.html',
        children: [
          {
            id: 63,
            parentId: 19,
            name: 'Deleting a Report',
            local: 'en-us_topic_0000001829819781.html'
          }
        ]
      }
    ]
  },
  {
    id: 4,
    parentId: 0,
    name: 'Insight',
    local: 'en-us_topic_0000001829901625.html',
    children: [
      {
        id: 64,
        parentId: 4,
        name: 'Managing Alarms and Events',
        local: 'en-us_topic_0000001829821661.html'
      },
      {
        id: 65,
        parentId: 4,
        name: 'Managing Jobs',
        local: 'en-us_topic_0000001829821681.html',
        children: [
          {
            id: 66,
            parentId: 65,
            name: 'Viewing the Job Progress',
            local: 'en-us_topic_0000001829821677.html'
          },
          {
            id: 67,
            parentId: 65,
            name: 'Stopping Jobs',
            local: 'en-us_topic_0000001829821649.html'
          },
          {
            id: 68,
            parentId: 65,
            name: 'Downloading Jobs',
            local: 'en-us_topic_0000001829821665.html'
          }
        ]
      }
    ]
  },
  {
    id: 5,
    parentId: 0,
    name: 'System',
    local: 'en-us_topic_0000001783062254.html',
    children: [
      {
        id: 69,
        parentId: 5,
        name: 'Managing Users',
        local: 'en-us_topic_0000001879127937.html',
        children: [
          {
            id: 78,
            parentId: 69,
            name: 'Overview of User Roles',
            local: 'en-us_topic_0000001832328588.html'
          },
          {
            id: 79,
            parentId: 69,
            name: 'Creating a User',
            local: 'en-us_topic_0000001879247733.html'
          },
          {
            id: 80,
            parentId: 69,
            name: 'Modifying a User',
            local: 'en-us_topic_0000001832488404.html'
          },
          {
            id: 81,
            parentId: 69,
            name: 'Locking Out a User',
            local: 'en-us_topic_0000001879127941.html'
          },
          {
            id: 82,
            parentId: 69,
            name: 'Unlocking a User',
            local: 'en-us_topic_0000001832328592.html'
          },
          {
            id: 83,
            parentId: 69,
            name: 'Removing a User',
            local: 'en-us_topic_0000001879247737.html'
          },
          {
            id: 84,
            parentId: 69,
            name: 'Resetting the User Password',
            local: 'en-us_topic_0000001832488408.html'
          },
          {
            id: 85,
            parentId: 69,
            name: 'Managing the Password Retrieval Email Address',
            local: 'en-us_topic_0000001879127945.html'
          }
        ]
      },
      {
        id: 70,
        parentId: 5,
        name: 'Security Policy Management',
        local: 'en-us_topic_0000001832328596.html'
      },
      {
        id: 71,
        parentId: 5,
        name: 'Managing Certificates',
        local: 'en-us_topic_0000001879247741.html',
        children: [
          {
            id: 86,
            parentId: 71,
            name: 'Viewing Certificate Information',
            local: 'en-us_topic_0000001832488412.html'
          },
          {
            id: 87,
            parentId: 71,
            name: 'Adding an External Certificate',
            local: 'en-us_topic_0000001879127949.html'
          },
          {
            id: 88,
            parentId: 71,
            name: 'Importing a Certificate',
            local: 'en-us_topic_0000001832328600.html'
          },
          {
            id: 89,
            parentId: 71,
            name: 'Exporting Request Files',
            local: 'en-us_topic_0000001879247749.html'
          },
          {
            id: 90,
            parentId: 71,
            name: 'Modifying Expiration Warning Days',
            local: 'en-us_topic_0000001832488416.html'
          },
          {
            id: 91,
            parentId: 71,
            name: 'Managing CRLs',
            local: 'en-us_topic_0000001879127953.html',
            children: [
              {
                id: 96,
                parentId: 91,
                name: 'Importing a CRL',
                local: 'en-us_topic_0000001832328604.html'
              },
              {
                id: 97,
                parentId: 91,
                name: 'Viewing a CRL',
                local: 'en-us_topic_0000001879247753.html'
              },
              {
                id: 98,
                parentId: 91,
                name: 'Downloading a CRL',
                local: 'en-us_topic_0000001832488424.html'
              },
              {
                id: 99,
                parentId: 91,
                name: 'Deleting a CRL',
                local: 'en-us_topic_0000001879127957.html'
              }
            ]
          },
          {
            id: 92,
            parentId: 71,
            name: 'Downloading a Certificate',
            local: 'en-us_topic_0000001832328608.html'
          },
          {
            id: 93,
            parentId: 71,
            name: 'Deleting an External Certificate',
            local: 'en-us_topic_0000001879247757.html'
          },
          {
            id: 94,
            parentId: 71,
            name:
              'Replacing the SSL Certificate of the OceanCyber 300 Data Security Appliance on the Server',
            local: 'en-us_topic_0000001832488428.html'
          },
          {
            id: 95,
            parentId: 71,
            name: 'Importing the Security Certificate to the Terminal Browser',
            local: 'en-us_topic_0000001879127961.html'
          }
        ]
      },
      {
        id: 72,
        parentId: 5,
        name: 'Managing Logs',
        local: 'en-us_topic_0000001832328616.html'
      },
      {
        id: 73,
        parentId: 5,
        name: 'Management System Data Backup',
        local: 'en-us_topic_0000001879247761.html',
        children: [
          {
            id: 100,
            parentId: 73,
            name: 'Configuring Management Data Backup',
            local: 'en-us_topic_0000001832488432.html'
          },
          {
            id: 101,
            parentId: 73,
            name: 'Exporting a Management Data Backup',
            local: 'en-us_topic_0000001879127965.html'
          },
          {
            id: 102,
            parentId: 73,
            name: 'Deleting a Management Data Backup',
            local: 'en-us_topic_0000001832328620.html'
          },
          {
            id: 103,
            parentId: 73,
            name: 'Importing a Management Data Backup',
            local: 'en-us_topic_0000001879247765.html'
          },
          {
            id: 104,
            parentId: 73,
            name: 'Restoring Management Data',
            local: 'en-us_topic_0000001832488436.html',
            children: [
              {
                id: 105,
                parentId: 104,
                name: 'Restoring the Current System Management Data',
                local: 'en-us_topic_0000001879127973.html'
              },
              {
                id: 106,
                parentId: 104,
                name: 'Restoring Management Data After System Reinstallation',
                local: 'en-us_topic_0000001832328628.html'
              }
            ]
          }
        ]
      },
      {
        id: 74,
        parentId: 5,
        name: 'Managing Event Dump',
        local: 'en-us_topic_0000001879247769.html'
      },
      {
        id: 75,
        parentId: 5,
        name: 'Managing SNMP Trap Notification',
        local: 'en-us_topic_0000001832488444.html'
      },
      {
        id: 76,
        parentId: 5,
        name: 'Configuring the iBMC Time',
        local: 'en-us_topic_0000001832329248.html'
      },
      {
        id: 77,
        parentId: 5,
        name: 'Managing Alarm Notification',
        local: 'en-us_topic_0000001879127977.html',
        children: [
          {
            id: 107,
            parentId: 77,
            name: 'Sender Settings',
            local: 'en-us_topic_0000002077228765.html'
          },
          {
            id: 108,
            parentId: 77,
            name: 'Syslog Notification',
            local: 'en-us_topic_0000002077110145.html'
          }
        ]
      }
    ]
  }
];
topLanguage = 'en';
topMainPage = 'en-us_topic_0000001783062250.html';
