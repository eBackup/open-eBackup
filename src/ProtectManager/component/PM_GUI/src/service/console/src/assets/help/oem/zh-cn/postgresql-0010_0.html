<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-cn" xml:lang="zh-cn">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="步骤2：开启归档模式">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="postgresql-0009.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect 备份一体机 1.5.0-1.6.0 帮助中心">
  <meta name="DC.Publisher" content="20240320">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="postgresql-0010_0">
  <meta name="DC.Language" content="zh-cn">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>步骤2：开启归档模式</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="postgresql-0010_0"></a><a name="postgresql-0010_0"></a>
  <h1 class="topictitle1">步骤2：开启归档模式</h1>
  <div>
   <p>在执行数据库备份前必须开启归档模式，否则将会导致备份失败。</p>
   <div class="section">
    <h4 class="sectiontitle">操作步骤</h4>
    <ul>
     <li><strong>方式一：通过修改配置文件参数开启归档模式</strong>
      <ol>
       <li>使用PuTTY，登录PostgreSQL数据库主机。</li>
       <li>创建存放归档日志（WAL日志）的路径，后续操作以/mnt/server/archivedir/路径为例。<pre class="screen">mkdir -p<em> /mnt/server/archivedir/</em></pre></li>
       <li>赋予数据库管理用户postgres读写权限。<pre class="screen">chmod 750 <em>/mnt/server/archivedir/</em>
chown postgres:postgres <em>/mnt/server/archivedir/</em></pre></li>
       <li>执行<strong>su postgres</strong>切换数据库管理用户postgres。系统回显示例如下：<pre class="screen">[root@pg_102_129 <font style="font-size:8pt" face="Courier New">~</font>]# su postgres
[postgres@pg_102_129 root]$<em>  </em></pre></li>
       <li>登录数据库管理用户postgres。<pre class="screen">cd <em>/usr/local/pgsql/bin</em>
./psql</pre></li>
       <li>执行<strong>show config_file</strong><strong>;</strong>查询postgresql.conf文件所在路径。系统回显示例如下：<pre class="screen">postgres=# show config_file;
              config_file
---------------------------------------
 /usr/local/pgsql/data/postgresql.conf
(1 row)</pre></li>
       <li>在键盘上按<span class="uicontrol">“Ctrl+d”</span>退出登录数据库管理用户postgres，进入PostgreSQL数据库主机。</li>
       <li>进入postgresql.conf文件，这里以/usr/local/pgsql/data/postgresql.conf路径为例。<pre class="screen">vi <em>/usr/local/pgsql/data/postgresql.conf</em></pre></li>
       <li>找到并修改postgresql.conf文件中的wal_level 、archive_mode和archive_command参数，如下所示：<pre class="screen">wal_level = replica                     # minimal, replica, or logical
archive_mode = on               # enables archiving; off, on, or always
archive_command = 'cp %p /home/postgres/archivedir/%f'          # command to use to archive a logfile segment</pre>
        <div class="note">
         <img src="public_sys-resources/note_3.0-zh-cn.png"><span class="notetitle"> </span>
         <div class="notebody">
          <ul>
           <li>将wal_level设置为archive（PostgreSQL 9.6及以上版本设置为replica）。</li>
           <li>将archive_mode设置为on。</li>
           <li>将archive_command设置为 'cp %p /mnt/server/archivedir/%f'，并确保归档日志的路径是单个路径。</li>
           <li>archive_command只支持cp命令，其他命令不支持。</li>
           <li>修改postgresql.conf文件时，请修改文件中已存在的字段值，禁止在文件中自行新增同样的字段，否则将会影响恢复任务。</li>
           <li>开启归档模式后，需要手动清理日志，否则会影响数据库的正常使用。手动清理日志的操作参见<a href="zh-cn_topic_0000002135383169.html">手动清理归档日志</a>。</li>
          </ul>
         </div>
        </div></li>
      </ol></li>
    </ul>
    <ul>
     <li id="postgresql-0010_0__zh-cn_topic_0000001607842332_li125261454163017"><strong id="postgresql-0010_0__zh-cn_topic_0000001607842332_b520795215418">方式二：通过执行数据库命令开启归档模式</strong>
      <ol id="postgresql-0010_0__zh-cn_topic_0000001607842332_ol1431724414327">
       <li id="postgresql-0010_0__zh-cn_topic_0000001607842332_li387595019325">使用PuTTY，登录PostgreSQL数据库主机。</li>
       <li id="postgresql-0010_0__zh-cn_topic_0000001607842332_li11497105712322">创建存放归档日志（WAL日志）的路径，后续操作以/mnt/server/archivedir/路径为例。<pre class="screen" id="postgresql-0010_0__zh-cn_topic_0000001607842332_screen2049712570326">mkdir -p /mnt/server/archivedir/</pre></li>
       <li id="postgresql-0010_0__zh-cn_topic_0000001607842332_li157953814338">赋予运行数据库的操作系统用户有读写权限。<pre class="screen" id="postgresql-0010_0__zh-cn_topic_0000001607842332_screen11795158133313">chmod 750 /mnt/server/archivedir/
chown postgres:postgres /mnt/server/archivedir/</pre></li>
       <li id="postgresql-0010_0__zh-cn_topic_0000001607842332_li296831915334">执行<strong id="postgresql-0010_0__zh-cn_topic_0000001607842332_b13968171911333">su postgres</strong>切换数据库管理用户postgres。系统回显示例如下：<pre class="screen" id="postgresql-0010_0__zh-cn_topic_0000001607842332_screen7968719103320">[root@pg_102_129 <font style="font-size:8pt" face="Courier New">~</font>]# su postgres
[postgres@pg_102_129 root]$<em id="postgresql-0010_0__zh-cn_topic_0000001607842332_i5968151915333">  </em></pre></li>
       <li id="postgresql-0010_0__zh-cn_topic_0000001607842332_li1141133713334">登录数据库管理用户postgres。<pre class="screen" id="postgresql-0010_0__zh-cn_topic_0000001607842332_screen14412163743318">cd /usr/local/pgsql/bin
./psql</pre></li>
       <li id="postgresql-0010_0__zh-cn_topic_0000001607842332_li1012154817339">执行<strong id="postgresql-0010_0__zh-cn_topic_0000001607842332_b1812144810335">show config_file</strong><strong id="postgresql-0010_0__zh-cn_topic_0000001607842332_b5121174816333">;</strong>查询postgresql.conf文件所在路径。系统回显示例如下：<pre class="screen" id="postgresql-0010_0__zh-cn_topic_0000001607842332_screen14121134853313">postgres=# show config_file;
              config_file
---------------------------------------
 /usr/local/pgsql/data/postgresql.conf
(1 row)</pre></li>
       <li id="postgresql-0010_0__zh-cn_topic_0000001607842332_li945175818339">在PostgreSQL数据库主机，依次执行以下命令，开启数据库归档模式。<pre class="screen" id="postgresql-0010_0__zh-cn_topic_0000001607842332_screen1645125843316">alter system set wal_level= 'replica';
alter system set archive_mode= 'on';
alter system set archive_command ='cp %p /mnt/server/archivedir/%f'</pre>
        <div class="note" id="postgresql-0010_0__zh-cn_topic_0000001607842332_note48201149191716">
         <img src="public_sys-resources/note_3.0-zh-cn.png"><span class="notetitle"> </span>
         <div class="notebody">
          <p id="postgresql-0010_0__zh-cn_topic_0000001607842332_p0821449131719">archive_command只支持cp命令，其他命令不支持。</p>
         </div>
        </div></li>
       <li id="postgresql-0010_0__zh-cn_topic_0000001607842332_li587338113413">集群场景下请执行以下命令，重启pgpool服务。/usr/local/pgpool/bin/pgpool代表PostgreSQL的安装路径。<pre class="screen" id="postgresql-0010_0__zh-cn_topic_0000001607842332_screen2823323174014"><em id="postgresql-0010_0__zh-cn_topic_0000001607842332_i376417247500">/usr/local/pgpool/bin/pgpool</em> -n &gt; /dev/null 2&gt;&amp;1 &amp;</pre> <p id="postgresql-0010_0__zh-cn_topic_0000001607842332_p387319813415">单机场景下请执行以下命令，重启数据库。/usr/local/pgsql/bin/pg_ctl代表PostgreSQL安装路径下面的pg_ctl的路径，-D参数代表客户自行指定的数据目录，-l参数代表PostgreSQL数据库在启动过程中指定的日志输出文件，在指定日志输出文件前，需要保证该文件有权限可创建成功。</p> <pre class="screen" id="postgresql-0010_0__zh-cn_topic_0000001607842332_screen1388613527407"><em id="postgresql-0010_0__zh-cn_topic_0000001607842332_i145921251165216">/usr/local/pgsql/bin/pg_ctl</em> -D <em id="postgresql-0010_0__zh-cn_topic_0000001607842332_i138621547451">/usr/local/pgsql/data</em> -l <em id="postgresql-0010_0__zh-cn_topic_0000001607842332_i9466985451">logfile</em>  restart</pre></li>
      </ol></li>
    </ul>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>父主题：</strong> <a href="postgresql-0009.html">备份PostgreSQL</a>
    </div>
   </div>
  </div>
 </body>
</html>