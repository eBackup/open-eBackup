<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Managing SNMP Trap Notification">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="helpcenter000126.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="admin_snmp">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Managing SNMP Trap Notification</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="admin_snmp"></a><a name="admin_snmp"></a>

<h1 class="topictitle1">Managing SNMP Trap Notification</h1>
<div><p class="msonormal" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p1319601185115">By configuring SNMP, you can query and set information about the <span id="admin_snmp__en-us_topic_0000001839224705_text1314614416111">OceanProtect</span>, or receive alarms of the <span id="admin_snmp__en-us_topic_0000001839224705_text1846015100135">OceanProtect</span>. The <span id="admin_snmp__en-us_topic_0000001839224705_text6188647121114">OceanProtect</span> supports SNMPv2c and SNMPv3. This section describes SNMP configuration methods when different protocols are used.</p>
<div class="section" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_section11242162043420"><h4 class="sectiontitle">Context</h4><p class="msonormal" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p12370152617344">Trap is a type of SNMP message that indicates the occurrence of an event. To send alarm notifications by SNMP, you must configure a trap server IP address. After the trap server address is set, alarm information of the <span id="admin_snmp__en-us_topic_0000001839224705_text4918347111113">OceanProtect</span> will be sent to the third-party network management tool specified by the trap.</p>
</div>
<div class="section" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_section1615164218346"><h4 class="sectiontitle">Precautions</h4><p class="msonormal" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p12994613510">A third-party network management tool must meet the following requirements:</p>
<ul id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_ul29926133517"><li id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_li159915683515">If SNMPv2c is used, the used community name must be the same as that configured on the <span id="admin_snmp__en-us_topic_0000001839224705_text13691348141112">OceanProtect</span>.</li><li id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_li499967356">If SNMPv3 is used, the username, authentication protocol, and data encryption protocol of SNMPv3 must be the same as those configured on the <span id="admin_snmp__en-us_topic_0000001839224705_text3226154919116">OceanProtect</span>.</li></ul>
</div>
<div class="section" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_section186829444316"><h4 class="sectiontitle">Prerequisites</h4><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p1946955419312">The system administrator has logged in to the management interface.</p>
</div>
<div class="section" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_section9197320152616"><h4 class="sectiontitle">Procedure</h4><ol id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_ol868620083011"><li id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_li31809451213"><span>In the navigation pane, choose <span class="uicontrol" id="admin_snmp__en-us_topic_0000001839224705_uicontrol18295622422"><b>System &gt; Settings &gt; SNMP Trap</b></span>.</span></li><li id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_li13504124114563"><span>In the <span class="uicontrol" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_uicontrol882519457200"><b><span id="admin_snmp__en-us_topic_0000001839224705_text99606615514"><strong>Trap Parameter Settings</strong></span></b></span> area, configure SNMPv2c or SNMPv3 parameters.</span><p><div class="notice" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_note1773261124713"><span class="noticetitle"><img src="public_sys-resources/notice_3.0-en-us.png"> </span><div class="noticebody"><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p17732517473">There will be security risks in enabling SNMPv2c. You are advised to use the secure SNMPv3 protocol.</p>
</div></div>
<ul id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_ul922591311112"><li id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_li1422541320110">Set SNMPv3 parameters.<ol type="a" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_ol2515101616271"><li id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_li242694411426">Click <span id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_text5329112918811"><strong>Modify</strong></span>.</li><li id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_li1693853912473">Set <span class="parmname" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_parmname175021727194816"><b><span id="admin_snmp__en-us_topic_0000001839224705_text575314251859"><strong>Protocol Version</strong></span></b></span> to <span class="parmvalue" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_parmvalue14249231194817"><b>SNMPv3</b></span>.<div class="note" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_note65203238550"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p25201123185512">If you need to configure an engine ID for SNMP of a third-party NMS, click <span class="uicontrol" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_uicontrol117541911165713"><b><span id="admin_snmp__en-us_topic_0000001839224705_text1979614121719"><strong>View SNMPv3 Engine ID</strong></span></b></span> next to <span class="uicontrol" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_uicontrol6524145145713"><b><span id="admin_snmp__en-us_topic_0000001839224705_text1354023320518"><strong>Protocol Version</strong></span></b></span> and configure the engine ID based on the controller SNMPv3 engine ID displayed on the page.</p>
</div></div>
</li><li id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_li5515616162715">Set SNMPv3 parameters. <a href="#admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_table13515151615273">Table 1</a> describes the related parameters.
<div class="tablenoborder"><a name="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_table13515151615273"></a><a name="en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_table13515151615273"></a><table cellpadding="4" cellspacing="0" summary="" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_table13515151615273" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Parameter description</caption><colgroup><col style="width:21.04%"><col style="width:78.96%"></colgroup><thead align="left"><tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_row1550618164275"><th align="left" class="cellrowborder" valign="top" width="21.04%" id="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.1"><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p750617160277">Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="78.96%" id="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.2"><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p650691617271">Description</p>
</th>
</tr>
</thead>
<tbody><tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_row85078160278"><td class="cellrowborder" valign="top" width="21.04%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.1 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p105061516182719"><span id="admin_snmp__en-us_topic_0000001839224705_text1956613404208"><strong>Username</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="78.96%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.2 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p250651613270">Username.</p>
<p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p9506171672719">The value consists of 1 to 64 characters.</p>
</td>
</tr>
<tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_row450861612712"><td class="cellrowborder" valign="top" width="21.04%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.1 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p1050751642715"><span id="admin_snmp__en-us_topic_0000001839224705_text377125112203"><strong>Authentication Protocol</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="78.96%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.2 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p147556359410">Authentication algorithms of the user, including <span class="uicontrol" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_uicontrol27341629101113"><b><span id="admin_snmp__en-us_topic_0000001839224705_text1673615173248"><strong>None</strong></span></b></span>, <strong id="admin_snmp__en-us_topic_0000001839224705_b43162187952725">HMAC_MD5</strong>, <strong id="admin_snmp__en-us_topic_0000001839224705_b83337610652725">HMAC_SHA1</strong>, and <strong id="admin_snmp__en-us_topic_0000001839224705_b19214913652725">HMAC_SHA2</strong>.</p>
<p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p1650761612716">Default value: <strong id="admin_snmp__en-us_topic_0000001839224705_b187298083052725">HMAC_SHA2</strong>.</p>
<div class="note" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_note10507131672714"><span class="notetitle"> NOTE: </span><div class="notebody"><p class="NotesTextinTable" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p1450761602710"><strong id="admin_snmp__en-us_topic_0000001839224705_b188585544352725">HMAC_SHA</strong> is more secure than <strong id="admin_snmp__en-us_topic_0000001839224705_b96478892952725">HMAC_MD5</strong>. You are advised to use <strong id="admin_snmp__en-us_topic_0000001839224705_b120666992352725">HMAC_SHA2</strong>.</p>
</div></div>
</td>
</tr>
<tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_row8509141611275"><td class="cellrowborder" valign="top" width="21.04%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.1 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p09561572013"><span id="admin_snmp__en-us_topic_0000001839224705_text16710203622415"><strong>Authorization Password</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="78.96%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.2 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p145081416192715">Authentication password of the user.</p>
</td>
</tr>
<tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_row5509016102717"><td class="cellrowborder" valign="top" width="21.04%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.1 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p13509191612276"><span id="admin_snmp__en-us_topic_0000001839224705_text10819184218240"><strong>Confirm Authorization Password</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="78.96%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.2 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p45091163273">Authentication password of the user.</p>
</td>
</tr>
<tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_row351001692719"><td class="cellrowborder" valign="top" width="21.04%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.1 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p165094162271"><span id="admin_snmp__en-us_topic_0000001839224705_text27710504245"><strong>Data Encryption Protocol</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="78.96%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.2 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p12509151616271">Encryption protocols of the user, including <strong id="admin_snmp__en-us_topic_0000001839224705_b137168417952725">None</strong>, <strong id="admin_snmp__en-us_topic_0000001839224705_b20859860852725">DES</strong>, and <strong id="admin_snmp__en-us_topic_0000001839224705_b202369163552725">AES</strong>.</p>
<p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p632602070">Default value: <strong id="admin_snmp__en-us_topic_0000001839224705_b61845611052725">AES</strong></p>
<div class="note" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_note750941612715"><span class="notetitle"> NOTE: </span><div class="notebody"><p class="NotesTextinTable" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p15098160279">AES is more secure than DES. For security purposes, you are advised to use AES.</p>
</div></div>
</td>
</tr>
<tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_row55141816132720"><td class="cellrowborder" valign="top" width="21.04%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.1 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p05101716172712"><span id="admin_snmp__en-us_topic_0000001839224705_text145081222152515"><strong>Data Encryption Password</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="78.96%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.2 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p3511201613277">Data encryption password used by the user.</p>
<div class="note" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_note13432106204820"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p1843219634818"><span id="admin_snmp__en-us_topic_0000001839224705_text94253012256"><strong>Data Encryption Password</strong></span> and <span id="admin_snmp__en-us_topic_0000001839224705_text91888394251"><strong>Authorization Password</strong></span> must be different.</p>
</div></div>
</td>
</tr>
<tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_row6514101662711"><td class="cellrowborder" valign="top" width="21.04%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.1 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p10514171652717"><span id="admin_snmp__en-us_topic_0000001839224705_text8123112610257"><strong>Confirm Data Encryption Password</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="78.96%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.2 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p1151471618274">Data encryption password used by the user.</p>
</td>
</tr>
<tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_row135231908612"><td class="cellrowborder" valign="top" width="21.04%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.1 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p9523201064"><span id="admin_snmp__en-us_topic_0000001839224705_text9156548182515"><strong>Environment Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="78.96%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.2 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p105231503616">Name of the environment engine.</p>
</td>
</tr>
<tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_row65231008615"><td class="cellrowborder" valign="top" width="21.04%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.1 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p14523904617"><span id="admin_snmp__en-us_topic_0000001839224705_text10875135513259"><strong>Environment Engine ID</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="78.96%" headers="mcps1.3.5.2.2.2.2.1.1.3.2.2.3.1.2 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p20962155851513">Unique identifier of an SNMP engine. This ID is used together with <strong id="admin_snmp__en-us_topic_0000001839224705_b26513437552725">Environment Name</strong> to identify the environment of an SNMP entity.</p>
<p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p796225814154">An SNMP message packet is processed only when the environment of the transmitter fully matches that of the receiver. Otherwise, the SNMP message packet is discarded.</p>
</td>
</tr>
</tbody>
</table>
</div>
</li><li id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_li2515151617274">Click <strong id="admin_snmp__en-us_topic_0000001839224705_b1291317345477"><span id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_text1541746292"><strong>Save</strong></span></strong>.</li></ol>
</li><li id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_li16225101320114">Set SNMPv2c parameters.<ol type="a" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_ol78108137410"><li id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_li1972885284018">Click <span id="admin_snmp__en-us_topic_0000001839224705_text1583122174517"><strong>Modify</strong></span>.</li><li id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_li3623182519509">Set <span class="parmname" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_parmname186231825115012"><b><span id="admin_snmp__en-us_topic_0000001839224705_text1647131952611"><strong>Protocol Version</strong></span></b></span> to <span class="parmvalue" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_parmvalue2062392515508"><b>SNMPv2c</b></span>.</li><li id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_li31732191644">Set the community name. <a href="#admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_table155033194144">Table 2</a> describes the related parameters.
<div class="tablenoborder"><a name="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_table155033194144"></a><a name="en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_table155033194144"></a><table cellpadding="4" cellspacing="0" summary="" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_table155033194144" frame="border" border="1" rules="all"><caption><b>Table 2 </b>Parameters for modifying a community name</caption><colgroup><col style="width:33.29%"><col style="width:66.71000000000001%"></colgroup><thead align="left"><tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_row104929198141"><th align="left" class="cellrowborder" valign="top" width="33.29%" id="mcps1.3.5.2.2.2.2.2.1.3.2.2.3.1.1"><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p154911319191419">Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="66.71000000000001%" id="mcps1.3.5.2.2.2.2.2.1.3.2.2.3.1.2"><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p164915192143">Description</p>
</th>
</tr>
</thead>
<tbody><tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_row164931819121418"><td class="cellrowborder" valign="top" width="33.29%" headers="mcps1.3.5.2.2.2.2.2.1.3.2.2.3.1.1 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p3492019121418"><span id="admin_snmp__en-us_topic_0000001839224705_text1429713319265"><strong>Community Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="66.71000000000001%" headers="mcps1.3.5.2.2.2.2.2.1.3.2.2.3.1.2 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_p118419572236">Name of the community that has the read and write permissions.</p>
</td>
</tr>
</tbody>
</table>
</div>
</li><li id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_li183559502483">Click <span id="admin_snmp__en-us_topic_0000001839224705_text10799172815453"><strong>Save</strong></span>.</li></ol>
</li></ul>
</p></li><li id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_li1269210143019"><span>In the <strong id="admin_snmp__en-us_topic_0000001839224705_b157941727184514"><span id="admin_snmp__en-us_topic_0000001839224705_text198237012711"><strong>Trap Server</strong></span></strong> area, click <strong id="admin_snmp__en-us_topic_0000001839224705_b57951727154520"><span id="admin_snmp__en-us_topic_0000001839224705_text6316143092715"><strong>Add</strong></span></strong> and configure the trap server. <a href="#admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_table18404111116216">Table 3</a> describes the related parameters.</span><p>
<div class="tablenoborder"><a name="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_table18404111116216"></a><a name="en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_table18404111116216"></a><table cellpadding="4" cellspacing="0" summary="" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_table18404111116216" frame="border" border="1" rules="all"><caption><b>Table 3 </b>Trap server configuration parameters</caption><colgroup><col style="width:18.52%"><col style="width:81.47999999999999%"></colgroup><thead align="left"><tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_row1440419111129"><th align="left" class="cellrowborder" valign="top" width="18.52%" id="mcps1.3.5.2.3.2.1.2.3.1.1"><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p1840411115216">Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="81.47999999999999%" id="mcps1.3.5.2.3.2.1.2.3.1.2"><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p14404511922">Description</p>
</th>
</tr>
</thead>
<tbody><tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_row154043113210"><td class="cellrowborder" valign="top" width="18.52%" headers="mcps1.3.5.2.3.2.1.2.3.1.1 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p64041711124">IP Address</p>
</td>
<td class="cellrowborder" valign="top" width="81.47999999999999%" headers="mcps1.3.5.2.3.2.1.2.3.1.2 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p44041511824">Currently, only IPv4 addresses are supported.</p>
</td>
</tr>
<tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_row6404121114210"><td class="cellrowborder" valign="top" width="18.52%" headers="mcps1.3.5.2.3.2.1.2.3.1.1 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p1440415116214">Port</p>
</td>
<td class="cellrowborder" valign="top" width="81.47999999999999%" headers="mcps1.3.5.2.3.2.1.2.3.1.2 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p134041114220">Port used by the trap server to listen to trap messages.</p>
</td>
</tr>
<tr id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_row1840431114215"><td class="cellrowborder" valign="top" width="18.52%" headers="mcps1.3.5.2.3.2.1.2.3.1.1 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p94045111021">Description</p>
</td>
<td class="cellrowborder" valign="top" width="81.47999999999999%" headers="mcps1.3.5.2.3.2.1.2.3.1.2 "><p id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_p94047111226">Trap server description.</p>
</td>
</tr>
</tbody>
</table>
</div>
</p></li></ol>
</div>
<div class="section" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_section19455621153113"><h4 class="sectiontitle">Follow-up Procedure</h4><p class="msonormal" id="admin_snmp__en-us_topic_0000001839224705_en-us_topic_0000001348750433_en-us_topic_0000001310893213_en-us_topic_0000001123437765_en-us_topic_0171822298_p55205482">In the <strong id="admin_snmp__en-us_topic_0000001839224705_b35053384911"><span id="admin_snmp__en-us_topic_0000001839224705_text3922202172817"><strong>Trap Server</strong></span></strong> area, you can view <span id="admin_snmp__en-us_topic_0000001839224705_text859325152810"><strong>Trap Server</strong></span> information and delete or change a trap IP address.</p>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="helpcenter000126.html">System</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>