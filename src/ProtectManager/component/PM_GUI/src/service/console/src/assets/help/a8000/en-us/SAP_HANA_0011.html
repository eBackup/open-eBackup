<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Preparations for Backup">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="SAP_HANA_0008.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="SAP_HANA_0011">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Preparations for Backup</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="SAP_HANA_0011"></a><a name="SAP_HANA_0011"></a>

<h1 class="topictitle1">Preparations for Backup</h1>
<div><p>Before backing up SAP HANA databases, prepare related information by following instructions in <a href="#SAP_HANA_0011__en-us_topic_0000001455091930_table10744125193920">Table 1</a>.</p>

<div class="tablenoborder"><a name="SAP_HANA_0011__en-us_topic_0000001455091930_table10744125193920"></a><a name="en-us_topic_0000001455091930_table10744125193920"></a><table cellpadding="4" cellspacing="0" summary="" id="SAP_HANA_0011__en-us_topic_0000001455091930_table10744125193920" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Preparations for backup</caption><colgroup><col style="width:17.34%"><col style="width:11.709999999999999%"><col style="width:54.61%"><col style="width:16.34%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="17.34%" id="mcps1.3.2.2.5.1.1"><p>Item</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="11.709999999999999%" id="mcps1.3.2.2.5.1.2"><p>Mandatory or Not (Y/N)</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="54.61%" id="mcps1.3.2.2.5.1.3"><p>How to Obtain</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="16.34%" id="mcps1.3.2.2.5.1.4"><p>To Be Used In</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="17.34%" headers="mcps1.3.2.2.5.1.1 "><p>Database name and instance ID</p>
</td>
<td class="cellrowborder" valign="top" width="11.709999999999999%" headers="mcps1.3.2.2.5.1.2 "><p>Y</p>
</td>
<td class="cellrowborder" valign="top" width="54.61%" headers="mcps1.3.2.2.5.1.3 "><div class="p">Name of the database to be protected. You can query the database name using the following method:<ol><li>Use PuTTY to log in to the host where the SAP HANA database resides.</li><li>Run the following command to log in to the SAP HANA database terminal system. Username <strong>s00adm</strong> is used as an example.<pre class="screen">su - <em>s00adm</em>
hdbsql -i 00 -n localhost:30013 -d SYSTEMDB -u SYSTEM -p <em>Password customized by the user during database installation</em>;</pre>
</li><li>Run the following command to query the database name and instance ID:<pre class="screen">SELECT * FROM M_DATABASES;</pre>
<p>Information similar to the following is displayed, where <strong>SYSTEMDB</strong> is the database name and <strong>S00</strong> is the instance ID.</p>
<pre class="screen">DATABASE_NAME,DESCRIPTION,ACTIVE_STATUS,ACTIVE_STATUS_DETAILS,OS_USER,OS_GROUP,RESTART_MODE
"SYSTEMDB","SystemDB-S00-00","YES","","","","DEFAULT"
"S00","S00-00","YES","","","","DEFAULT"
</pre>
</li></ol>
</div>
</td>
<td class="cellrowborder" rowspan="5" valign="top" width="16.34%" headers="mcps1.3.2.2.5.1.4 "><p><a href="SAP_HANA_0013.html">Step 1: Registering the SAP HANA Database (File Backup Mode)</a></p>
<p><a href="SAP_HANA_0020.html">Step 1: Registering the SAP HANA Database (Backint Backup Mode)</a></p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.1.1 "><p>Database host</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.1.2 "><p>Y</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.1.3 "><p>Host where the database resides.</p>
<p>If the database is deployed in a cluster, select all hosts where the database is distributed.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.1.1 "><p>Name and password of a database user</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.1.2 "><p>Y</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.1.3 "><p>Username and password used for accessing the database.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.1.1 "><p><strong>systemId</strong> and <strong>SQL Port</strong> of the database</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.1.2 "><p>Y</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.1.3 "><ul><li>You can query <strong>systemId</strong> on the SAP HANA Studio, as shown in the following figure.<p><span><img src="en-us_image_0000001792389580.png"></span></p>
</li><li>SQL Port indicates the SQL port on the master node of the system database. You can query the SQL port on the SAP HANA Studio.<p>In single-tenant mode, the default SQL Port is <strong>30015</strong>. In multi-tenant mode, the default SQL Port is <strong>30013</strong>.</p>
</li></ul>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.1.1 "><p>Username and password of the system database</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.1.2 "><p>N</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.2.2.5.1.3 "><p>This parameter is required only when the database to be protected is a tenant database.</p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="SAP_HANA_0008.html">Backup</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>