<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Managing RBAC (Applicable to 1.6.0 and Later Versions)">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="helpcenter000126.html">
<meta name="DC.Relation" scheme="URI" content="admin-0056.html">
<meta name="DC.Relation" scheme="URI" content="en-us_topic_0000002059543622.html">
<meta name="DC.Relation" scheme="URI" content="en-us_topic_0000002059385278.html">
<meta name="DC.Relation" scheme="URI" content="en-us_topic_0000002095582433.html">
<meta name="DC.Relation" scheme="URI" content="en-us_topic_0000002095463905.html">
<meta name="DC.Relation" scheme="URI" content="en-us_topic_0000002059543630.html">
<meta name="DC.Relation" scheme="URI" content="en-us_topic_0000002059385282.html">
<meta name="DC.Relation" scheme="URI" content="en-us_topic_0000002095582437.html">
<meta name="DC.Relation" scheme="URI" content="admin-0057.html">
<meta name="DC.Relation" scheme="URI" content="admin-0058.html">
<meta name="DC.Relation" scheme="URI" content="admin-0059.html">
<meta name="DC.Relation" scheme="URI" content="admin-0060.html">
<meta name="DC.Relation" scheme="URI" content="admin-0061.html">
<meta name="DC.Relation" scheme="URI" content="admin-0062.html">
<meta name="DC.Relation" scheme="URI" content="admin-0063.html">
<meta name="DC.Relation" scheme="URI" content="admin-0064.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="admin-0055">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Managing RBAC (Applicable to 1.6.0 and Later Versions)</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="admin-0055"></a><a name="admin-0055"></a>

<h1 class="topictitle1">Managing RBAC (Applicable to 1.6.0 and Later Versions)</h1>
<div><p id="admin-0055__en-us_topic_0000002014087417_p115338504102">Role-based permission management can be implemented by assigning specific roles to different users. Permissions cover access to and operations on resources. This feature is used to implement fine-grained permission and resource division, minimizing user permissions, as shown in <a href="#admin-0055__en-us_topic_0000002014087417_fig782612112143">Figure 1</a>.</p>
<div class="fignone" id="admin-0055__en-us_topic_0000002014087417_fig782612112143"><a name="admin-0055__en-us_topic_0000002014087417_fig782612112143"></a><a name="en-us_topic_0000002014087417_fig782612112143"></a><span class="figcap"><b>Figure 1 </b>Relationships between permissions and resources</span><br><span><img class="eddx" id="admin-0055__en-us_topic_0000002014087417_image19688185818232" src="en-us_image_0000002060884500.png"></span></div>
<div class="note" id="admin-0055__en-us_topic_0000002014087417_note173202388820"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><ul id="admin-0055__en-us_topic_0000002014087417_ul148413521684"><li id="admin-0055__en-us_topic_0000002014087417_li7841552689">In addition to the roles defined in the system, the system supports custom roles. Users are assigned roles to obtain corresponding permissions.</li><li id="admin-0055__en-us_topic_0000002014087417_li48418523810">A user can be bound with multiple roles. The resources authorized to a user are the union of the resource sets associated with all the bound roles. The child resources that are dynamically scanned are visible to the users bound with the corresponding parent resources.</li><li id="admin-0055__en-us_topic_0000002014087417_li88415214811">Permissions can be set for resource-related operations, such as viewing (default), backup, restoration to the original location, restoration to a new location, mounting, anonymization, and WORM setting.</li><li id="admin-0055__en-us_topic_0000002014087417_li188415526814">Permissions cannot be set for system management and monitoring operations. Rights- and domain-based management is not supported.</li><li id="admin-0055__en-us_topic_0000002014087417_li54811747122218">Users can only perform operations on the jobs that are executed after resource set authorization.</li><li id="admin-0055__en-us_topic_0000002014087417_li1010031093119">After a resource set is authorized to a user, the user can only view or perform operations on the copies generated for the corresponding resources after the resource set is created.</li></ul>
</div></div>
</div>
<div>
<ul class="ullinks">
<li class="ulchildlink"><strong><a href="admin-0056.html">Built-in User Roles</a></strong><br>
</li>
<li class="ulchildlink"><strong><a href="en-us_topic_0000002059543622.html">Creating a Role</a></strong><br>
</li>
<li class="ulchildlink"><strong><a href="en-us_topic_0000002059385278.html">Modifying a Role</a></strong><br>
</li>
<li class="ulchildlink"><strong><a href="en-us_topic_0000002095582433.html">Cloning a Role</a></strong><br>
</li>
<li class="ulchildlink"><strong><a href="en-us_topic_0000002095463905.html">Deleting a Role</a></strong><br>
</li>
<li class="ulchildlink"><strong><a href="en-us_topic_0000002059543630.html">Creating a Resource Set</a></strong><br>
</li>
<li class="ulchildlink"><strong><a href="en-us_topic_0000002059385282.html">Deleting a Resource Set</a></strong><br>
</li>
<li class="ulchildlink"><strong><a href="en-us_topic_0000002095582437.html">Modifying a Resource Set</a></strong><br>
</li>
<li class="ulchildlink"><strong><a href="admin-0057.html">Creating a User</a></strong><br>
</li>
<li class="ulchildlink"><strong><a href="admin-0058.html">Modifying a User</a></strong><br>
</li>
<li class="ulchildlink"><strong><a href="admin-0059.html">Locking Out a User</a></strong><br>
</li>
<li class="ulchildlink"><strong><a href="admin-0060.html">Unlocking a User</a></strong><br>
</li>
<li class="ulchildlink"><strong><a href="admin-0061.html">Deleting a User</a></strong><br>
</li>
<li class="ulchildlink"><strong><a href="admin-0062.html">Resetting the User Password</a></strong><br>
</li>
<li class="ulchildlink"><strong><a href="admin-0063.html">Resetting the System Administrator Password</a></strong><br>
</li>
<li class="ulchildlink"><strong><a href="admin-0064.html">Setting the Mailbox for Password Retrieval</a></strong><br>
</li>
</ul>

<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="helpcenter000126.html">System</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>