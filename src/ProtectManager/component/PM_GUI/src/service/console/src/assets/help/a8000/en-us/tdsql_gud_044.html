<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Restoring a TDSQL Database">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="tdsql_gud_041.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="tdsql_gud_044">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Restoring a TDSQL Database</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="tdsql_gud_044"></a><a name="tdsql_gud_044"></a>

<h1 class="topictitle1">Restoring a TDSQL Database</h1>
<div><p>This section describes how to use a copy to restore a database that has been backed up. For 1.6.0 and later versions, multiple instances in the same TDSQL cluster can be backed up or restored at the same time.</p>
<div class="section"><h4 class="sectiontitle">Prerequisites</h4><ul><li><span>The NTP service has been enabled for the cluster and the clock source configurations are the same.</span></li><li>During cluster instance restoration, ensure that the data synchronization user of the target cluster exists in the copy used for the restoration. Otherwise, the restoration fails.</li></ul>
</div>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li id="tdsql_gud_044__en-us_topic_0000001263614142_li10796132225712"><span>Choose <span class="uicontrol" id="tdsql_gud_044__en-us_topic_0000001839142377_uicontrol8687163819368"><b><span id="tdsql_gud_044__en-us_topic_0000001839142377_text176879387363"><strong>Explore</strong></span> &gt; <span id="tdsql_gud_044__en-us_topic_0000001839142377_text156881638113617"><strong>Copy Data</strong></span> &gt; <span id="tdsql_gud_044__en-us_topic_0000001839142377_text49981969503"><strong>Databases</strong></span> &gt; TDSQL</b></span>.</span></li><li><span>Search for copies by TDSQL resource or copy. This section describes how to search for copies by resource.</span><p><p>On the <span class="uicontrol"><b><span><strong>Resources</strong></span></b></span> tab page, locate the resource to be restored by resource name and click the name.</p>
</p></li><li><span>Select the year, month, and day in sequence to find the copy.</span><p><p>If <span><img src="en-us_image_0000001839210621.png"></span> is displayed below a month or date, copies exist in the month or on the day.</p>
</p></li><li><span>Specify a copy or a point in time for restoration.</span><p><div class="p">You can specify a copy or any point in time between two copies for restoration.<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p>If no archive logs are backed up at a time point, data cannot be restored to the time point.</p>
</div></div>
<ol type="a"><li>On the <span><strong>By Date</strong></span> tab page, select a year, month, and day in sequence to search for copies.<p>If <span><img src="en-us_image_0000001839290573.png"></span> is displayed below a month or date, copies exist in the month or on the day.</p>
</li><li>Specify a copy or a point in time for restoration.<ul><li>Specify a copy for restoration.<p><span><img class="eddx" src="en-us_image_0000001839290581.png"></span></p>
</li><li>Specify a point in time for restoration.<p>Data can be restored to a specific point in time in the blue part on the timeline. <span><img src="en-us_image_0000001886074353.png"></span> indicates that a copy exists at this point in time. If no archive logs are backed up at a time point, data cannot be restored to the time point.</p>
<p><span><img class="eddx" src="en-us_image_0000001839290565.png"></span></p>
<p></p>
<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><ul id="tdsql_gud_044__oracle_gud_0058_ul75802612312"><li id="tdsql_gud_044__oracle_gud_0058_li1497342618234">For 1.5.0, a maximum of 100 copies can be displayed on the timeline. You can click <span><img id="tdsql_gud_044__oracle_gud_0058_image173544102299" src="en-us_image_0000001886153709.png"></span> to view all copies.</li><li id="tdsql_gud_044__oracle_gud_0058_li95818264231">During point-in-time recovery, the information about the copy used for restoration cannot be obtained on the management page. Therefore, users cannot view the copy information of the restoration job on pages of the restoration job and related events.</li></ul>
</div></div>
</li></ul>
</li><li>Restore a TDSQL database.<ul><li>Restore non-distributed instances.<ul><li>Restore the TDSQL database to the original location.<ol class="substepthirdol"><li>Select <span class="uicontrol"><b><span><strong>Original location</strong></span></b></span> for restoration.</li><li>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</li></ol>
</li><li>Restore the TDSQL database to a new location.<ol class="substepthirdol"><li>Select <span class="uicontrol"><b><span><strong>New location</strong></span></b></span> for restoration.</li><li>Select a cluster for restoration from the <span class="parmname"><b><span><strong>Target Cluster</strong></span></b></span> drop-down list.</li><li>Set the target instance for restoration.<p>For 1.5.0, you can only select an existing instance as the target instance. For 1.6.0 and later versions, you can select an existing instance or create an instance. The database version of the selected target instance must be the same as that of the original instance. If you choose to create an instance, see <a href="#tdsql_gud_044__table814219141233">Table 1</a>.</p>
<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p>Before creating an instance, ensure that the target cluster has sufficient resources to create the instance. Otherwise, the restoration will fail. For details about the resources of the target cluster, log in to the TDSQL CHITU management console and click <strong>View [Equipment Resources]</strong> on the <strong>Instance management</strong> page.</p>
</div></div>

<div class="tablenoborder"><a name="tdsql_gud_044__table814219141233"></a><a name="table814219141233"></a><table cellpadding="4" cellspacing="0" summary="" id="tdsql_gud_044__table814219141233" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Parameters for creating an instance</caption><colgroup><col style="width:31.1%"><col style="width:22.66%"><col style="width:46.239999999999995%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" colspan="2" valign="top" id="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" id="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.1 "><p>DR Mode</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.2 "><p>Only the DR mode that does not exceed the number of data nodes in the target cluster can be selected.</p>
</td>
</tr>
<tr><td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.1 "><p>Instance Name</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.2 "><p>Name of an instance, which can be customized.</p>
</td>
</tr>
<tr><td class="cellrowborder" colspan="2" valign="top" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.1 "><p>Agent Host</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.2 "><p>Selects an agent host for the TDSQL service node.</p>
</td>
</tr>
<tr><td class="cellrowborder" rowspan="5" valign="top" width="31.1%" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.1 "><p>Advanced</p>
<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><p>If you do not set advanced parameters, the configuration of the original instance is used for restoration by default.</p>
</div></div>
</td>
<td class="cellrowborder" valign="top" width="22.66%" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.1 "><p>Model</p>
</td>
<td class="cellrowborder" valign="top" width="46.239999999999995%" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.2 "><p>Select the target model.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.1 "><p><strong>CPU (Cores)</strong></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.1 "><p>The value ranges from 1 to 32.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.1 "><p><strong>Memory Size (GB)</strong></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.1 "><p>The value ranges from 1 to 128.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.1 "><p><strong>Data Disk Size (GB)</strong></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.1 "><p>The value ranges from 1 to 1500.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.1 "><p><strong>Log Disk Size (GB)</strong></p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.3.3.2.4.2.1.2.3.1.1.1.2.1.3.3.2.4.1.1 "><p>The value ranges from 1 to 300.</p>
</td>
</tr>
</tbody>
</table>
</div>
</li><li>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</li></ol>
</li></ul>
</li><li>Restore distributed instances.<ul><li>Restore the TDSQL database to the original cluster.<ol class="substepthirdol"><li>Set <strong>Restore To</strong> to <span class="uicontrol"><b>Original Cluster</b></span>.</li><li id="tdsql_gud_044__li1545975010612">Select the mode to be restored from the <span class="parmname" id="tdsql_gud_044__parmname4364123931018"><b>DR Mode</b></span> drop-down list.<div class="note" id="tdsql_gud_044__note1427914498213"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="tdsql_gud_044__p2279349628">Only the DR mode that does not exceed the number of data nodes in the target cluster can be selected.</p>
</div></div>
</li><li id="tdsql_gud_044__li850010299118">In the <span class="wintitle" id="tdsql_gud_044__wintitle1567142671910"><b>Data Nodes</b></span> area, select an agent host for the TDSQL service node.</li><li id="tdsql_gud_044__li1473317682215">Set advanced parameters. For details about the parameters, see <a href="#tdsql_gud_044__table18661192115247">Table 2</a>.<div class="note" id="tdsql_gud_044__note839823416220"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="tdsql_gud_044__p939843418226">If you do not set advanced parameters, the configuration of the original instance is used for restoration by default.</p>
</div></div>

<div class="tablenoborder"><a name="tdsql_gud_044__table18661192115247"></a><a name="table18661192115247"></a><table cellpadding="4" cellspacing="0" summary="" id="tdsql_gud_044__table18661192115247" frame="border" border="1" rules="all"><caption><b>Table 2 </b>Advanced configuration parameters</caption><colgroup><col style="width:50%"><col style="width:50%"></colgroup><thead align="left"><tr id="tdsql_gud_044__row36611021192414"><th align="left" class="cellrowborder" valign="top" width="50%" id="mcps1.3.3.2.4.2.1.2.3.1.2.1.1.1.4.3.2.3.1.1"><p id="tdsql_gud_044__p36624213242">Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="50%" id="mcps1.3.3.2.4.2.1.2.3.1.2.1.1.1.4.3.2.3.1.2"><p id="tdsql_gud_044__p12662321122419">Description</p>
</th>
</tr>
</thead>
<tbody><tr id="tdsql_gud_044__row96621210246"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.1.1.4.3.2.3.1.1 "><p id="tdsql_gud_044__p4662162118242"><strong id="tdsql_gud_044__b4251644142114">Model</strong></p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.1.1.4.3.2.3.1.2 "><p id="tdsql_gud_044__p36621521182414">Select the target model.</p>
</td>
</tr>
<tr id="tdsql_gud_044__row6662132182414"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.1.1.4.3.2.3.1.1 "><p id="tdsql_gud_044__p186621621162419"><strong id="tdsql_gud_044__b1064991108">CPU (Cores)</strong></p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.1.1.4.3.2.3.1.2 "><p id="tdsql_gud_044__p86621521172410">The value ranges from 1 to 64.</p>
</td>
</tr>
<tr id="tdsql_gud_044__row26621821122419"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.1.1.4.3.2.3.1.1 "><p id="tdsql_gud_044__p66628212242"><strong id="tdsql_gud_044__b120715118">Memory Size (GB)</strong></p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.1.1.4.3.2.3.1.2 "><p id="tdsql_gud_044__p172071437202715">The value ranges from 1 to 128.</p>
</td>
</tr>
<tr id="tdsql_gud_044__row266262110245"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.1.1.4.3.2.3.1.1 "><p id="tdsql_gud_044__p14662152182410"><strong id="tdsql_gud_044__b958865432">Data Disk Size (GB)</strong></p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.1.1.4.3.2.3.1.2 "><p id="tdsql_gud_044__p18104921182913">The value ranges from 1 to 800.</p>
</td>
</tr>
<tr id="tdsql_gud_044__row116886327298"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.1.1.4.3.2.3.1.1 "><p id="tdsql_gud_044__p156881932182912"><strong id="tdsql_gud_044__b1335652519">Log Disk Size (GB)</strong></p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.1.1.4.3.2.3.1.2 "><p id="tdsql_gud_044__p8456447102919">The value ranges from 1 to 800.</p>
</td>
</tr>
</tbody>
</table>
</div>
</li><li>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</li></ol>
</li><li>Restore the TDSQL database to a new cluster.<ol class="substepthirdol"><li>Set <strong>Restore To</strong> to <span class="uicontrol"><b>New Cluster</b></span>.</li><li>Select the cluster to be restored from the <span class="parmname"><b><span>Target Cluster</span></b></span> drop-down list.<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p>The zkmeta automatic backup function must be enabled for the selected target cluster. Otherwise, the restoration will fail.</p>
</div></div>
</li><li>Select the mode to be restored from the <span class="parmname" id="tdsql_gud_044__tdsql_gud_044_parmname4364123931018"><b>DR Mode</b></span> drop-down list.<div class="note" id="tdsql_gud_044__tdsql_gud_044_note1427914498213"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="tdsql_gud_044__tdsql_gud_044_p2279349628">Only the DR mode that does not exceed the number of data nodes in the target cluster can be selected.</p>
</div></div>
</li><li>In the <span class="wintitle" id="tdsql_gud_044__tdsql_gud_044_wintitle1567142671910"><b>Data Nodes</b></span> area, select an agent host for the TDSQL service node.</li><li>Set advanced parameters. For details about the parameters, see <a href="#tdsql_gud_044__tdsql_gud_044_table18661192115247">Table 3</a>.<div class="note" id="tdsql_gud_044__tdsql_gud_044_note839823416220"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p id="tdsql_gud_044__tdsql_gud_044_p939843418226">If you do not set advanced parameters, the configuration of the original instance is used for restoration by default.</p>
</div></div>

<div class="tablenoborder"><a name="tdsql_gud_044__tdsql_gud_044_table18661192115247"></a><a name="tdsql_gud_044_table18661192115247"></a><table cellpadding="4" cellspacing="0" summary="" id="tdsql_gud_044__tdsql_gud_044_table18661192115247" frame="border" border="1" rules="all"><caption><b>Table 3 </b>Advanced configuration parameters</caption><colgroup><col style="width:50%"><col style="width:50%"></colgroup><thead align="left"><tr id="tdsql_gud_044__tdsql_gud_044_row36611021192414"><th align="left" class="cellrowborder" valign="top" width="50%" id="mcps1.3.3.2.4.2.1.2.3.1.2.1.2.1.5.3.2.3.1.1"><p id="tdsql_gud_044__tdsql_gud_044_p36624213242">Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="50%" id="mcps1.3.3.2.4.2.1.2.3.1.2.1.2.1.5.3.2.3.1.2"><p id="tdsql_gud_044__tdsql_gud_044_p12662321122419">Description</p>
</th>
</tr>
</thead>
<tbody><tr id="tdsql_gud_044__tdsql_gud_044_row96621210246"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.2.1.5.3.2.3.1.1 "><p id="tdsql_gud_044__tdsql_gud_044_p4662162118242"><strong id="tdsql_gud_044__tdsql_gud_044_b4251644142114">Model</strong></p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.2.1.5.3.2.3.1.2 "><p id="tdsql_gud_044__tdsql_gud_044_p36621521182414">Select the target model.</p>
</td>
</tr>
<tr id="tdsql_gud_044__tdsql_gud_044_row6662132182414"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.2.1.5.3.2.3.1.1 "><p id="tdsql_gud_044__tdsql_gud_044_p186621621162419"><strong id="tdsql_gud_044__tdsql_gud_044_b1064991108">CPU (Cores)</strong></p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.2.1.5.3.2.3.1.2 "><p id="tdsql_gud_044__tdsql_gud_044_p86621521172410">The value ranges from 1 to 64.</p>
</td>
</tr>
<tr id="tdsql_gud_044__tdsql_gud_044_row26621821122419"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.2.1.5.3.2.3.1.1 "><p id="tdsql_gud_044__tdsql_gud_044_p66628212242"><strong id="tdsql_gud_044__tdsql_gud_044_b120715118">Memory Size (GB)</strong></p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.2.1.5.3.2.3.1.2 "><p id="tdsql_gud_044__tdsql_gud_044_p172071437202715">The value ranges from 1 to 128.</p>
</td>
</tr>
<tr id="tdsql_gud_044__tdsql_gud_044_row266262110245"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.2.1.5.3.2.3.1.1 "><p id="tdsql_gud_044__tdsql_gud_044_p14662152182410"><strong id="tdsql_gud_044__tdsql_gud_044_b958865432">Data Disk Size (GB)</strong></p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.2.1.5.3.2.3.1.2 "><p id="tdsql_gud_044__tdsql_gud_044_p18104921182913">The value ranges from 1 to 800.</p>
</td>
</tr>
<tr id="tdsql_gud_044__tdsql_gud_044_row116886327298"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.2.1.5.3.2.3.1.1 "><p id="tdsql_gud_044__tdsql_gud_044_p156881932182912"><strong id="tdsql_gud_044__tdsql_gud_044_b1335652519">Log Disk Size (GB)</strong></p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.3.3.2.4.2.1.2.3.1.2.1.2.1.5.3.2.3.1.2 "><p id="tdsql_gud_044__tdsql_gud_044_p8456447102919">The value ranges from 1 to 800.</p>
</td>
</tr>
</tbody>
</table>
</div>
</li><li>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</li></ol>
</li></ul>
</li></ul>
</li></ol>
</div>
</p></li><li id="tdsql_gud_044__en-us_topic_0000001263614142_li1481714307166"><span>Click <span class="uicontrol" id="tdsql_gud_044__en-us_topic_0000001263614142_uicontrol194684162818"><b><span id="tdsql_gud_044__text5761862442"><strong>OK</strong></span></b></span>.</span></li></ol>
</div>
<div class="section"><h4 class="sectiontitle">Follow-up Procedure</h4><p>After the non-distributed instance is restored, the username and password of the target instance will be overwritten by those of the instance corresponding to the copy used for the restoration. If the status of the target instance is <span class="uicontrol"><b><span><strong>Offline</strong></span></b></span>, update the username and password of the target instance.</p>
<ol><li><span>Choose <span class="uicontrol" id="tdsql_gud_044__en-us_topic_0000001839142377_uicontrol123821426193719"><b><span id="tdsql_gud_044__en-us_topic_0000001839142377_text3382726113718"><strong>Protection</strong></span> &gt; Databases &gt; TDSQL</b></span>.</span></li><li><span>Click the <span class="uicontrol" id="tdsql_gud_044__tdsql_gud_011_en-us_topic_0000001311214069_uicontrol47381735103817"><b><span id="tdsql_gud_044__tdsql_gud_011_text1668605291117"><strong>Non-distributed Instances</strong></span></b></span> tab.</span></li><li><span>Click the name of the target instance.</span></li><li><span>In the upper right corner of the page that is displayed, choose <span class="uicontrol"><b><span><strong>Modify</strong></span> &gt; <span><strong>Operation</strong></span></b></span>.</span></li><li><span>On the <span class="wintitle"><b><span><strong>Modify</strong></span></b></span> page that is displayed, update the username and password.</span></li><li><span>Click <span class="uicontrol" id="tdsql_gud_044__tdsql_gud_044_en-us_topic_0000001263614142_uicontrol194684162818"><b><span id="tdsql_gud_044__tdsql_gud_044_text5761862442"><strong>OK</strong></span></b></span>.</span></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="tdsql_gud_041.html">Restoration</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>