<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Step 3: Registering the Database of a Single PostgreSQL Instance">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="postgresql-0010-2.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="postgresql-0011">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Step 3: Registering the Database of a Single PostgreSQL Instance</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="postgresql-0011"></a><a name="postgresql-0011"></a>
  <h1 class="topictitle1">Step 3: Registering the Database of a Single PostgreSQL Instance</h1>
  <div>
   <div class="section">
    <h4 class="sectiontitle">Prerequisites</h4>
    <ul>
     <li>For 1.6.0 and later versions, before registering an instance, check whether the owner of the <strong>bin/lib/share</strong> file in the database installation path is <strong>root</strong>. If the owner is <strong>root</strong>, set <strong>enable_root</strong> to <strong>1</strong> in the <strong>/opt/DataBackup/ProtectClient/Plugins/GeneralDBPlugin/bin/applications/postgresql/conf/switch.conf</strong> configuration file.</li>
     <li>When backing up PostgreSQL database instances in the High Availability Cluster Server (HACS) scenario, the <span>product</span> supports only the backup of the PostgreSQL database in a single instance.</li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li><span>Choose <span class="uicontrol" id="postgresql-0011__en-us_topic_0000001839142377_uicontrol1389612810362"><b><span id="postgresql-0011__en-us_topic_0000001839142377_text1089610893610"><strong>Protection</strong></span> &gt; Databases &gt; PostgreSQL</b></span>.</span></li>
     <li><span>Click the <span class="uicontrol"><b>Instance</b></span> tab.</span></li>
     <li><span>Click <span class="uicontrol"><b><span><strong>Register</strong></span></b></span> to register a PostgreSQL database.</span></li>
     <li><span>Set <span class="uicontrol"><b><span><strong>Type</strong></span></b></span> to <span class="uicontrol"><b><span><strong>Single instance</strong></span></b></span>.</span></li>
     <li><span>Configure the database instance and its authentication information.</span><p></p><p><a href="#postgresql-0011__en-us_topic_0000001311214069_table241515964115">Table 1</a> describes related parameters.</p>
      <div class="tablenoborder">
       <a name="postgresql-0011__en-us_topic_0000001311214069_table241515964115"></a><a name="en-us_topic_0000001311214069_table241515964115"></a>
       <table cellpadding="4" cellspacing="0" summary="" id="postgresql-0011__en-us_topic_0000001311214069_table241515964115" frame="border" border="1" rules="all">
        <caption>
         <b>Table 1 </b>Registration information of a single PostgreSQL instance
        </caption>
        <colgroup>
         <col style="width:25.629999999999995%">
         <col style="width:74.37%">
        </colgroup>
        <thead align="left">
         <tr>
          <th align="left" class="cellrowborder" valign="top" width="25.629999999999995%" id="mcps1.3.2.2.5.2.2.2.3.1.1"><p>Parameter</p></th>
          <th align="left" class="cellrowborder" valign="top" width="74.37%" id="mcps1.3.2.2.5.2.2.2.3.1.2"><p>Description</p></th>
         </tr>
        </thead>
        <tbody>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.2.2.5.2.2.2.3.1.1 "><p><span><strong>Name</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.2.2.5.2.2.2.3.1.2 "><p>Name of the user used for installing the database, which is user-defined.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.2.2.5.2.2.2.3.1.1 "><p><span><strong>Hosts</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.2.2.5.2.2.2.3.1.2 "><p>Select the database host to be registered.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.2.2.5.2.2.2.3.1.1 "><p><span><strong>Username</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.2.2.5.2.2.2.3.1.2 "><p>OS username of the database.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.2.2.5.2.2.2.3.1.1 "><p><span><strong>Database Installation Path</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.2.2.5.2.2.2.3.1.2 "><p>Path for installing the database to be registered.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.2.2.5.2.2.2.3.1.1 "><p><span><strong>Service Plane IP Address</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.2.2.5.2.2.2.3.1.2 "><p>IP address of the host for the database service to receive TCP/IP connections.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.2.2.5.2.2.2.3.1.1 "><p><span><strong>Database Port</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.2.2.5.2.2.2.3.1.2 "><p>Port number used by the database service to listen on TCP/IP connections.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.2.2.5.2.2.2.3.1.1 "><p><span><strong>Database Username</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.2.2.5.2.2.2.3.1.2 "><p>Name of the database administrator.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.2.2.5.2.2.2.3.1.1 "><p><span><strong>Database Password</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.2.2.5.2.2.2.3.1.2 "><p>Password of the database administrator.</p></td>
         </tr>
        </tbody>
       </table>
      </div> <p></p></li>
     <li><span>Click <span class="uicontrol"><b>OK</b></span>.</span></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="postgresql-0010-2.html">Backing Up PostgreSQL</a>
    </div>
   </div>
  </div>
 </body>
</html>