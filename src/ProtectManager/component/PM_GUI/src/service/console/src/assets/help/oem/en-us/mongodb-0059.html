<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Viewing the MongoDB Environment">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="mongodb-0058.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="mongodb-0059">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Viewing the MongoDB Environment</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="mongodb-0059"></a><a name="mongodb-0059"></a>
  <h1 class="topictitle1">Viewing the MongoDB Environment</h1>
  <div>
   <p>After a MongoDB instance is registered with the <span>product</span>, you can view details about the instance and the host or cluster where the instance resides on the WebUI.</p>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li><span>Choose <span class="uicontrol" id="mongodb-0059__en-us_topic_0000001839142377_uicontrol9332164373910"><b><span id="mongodb-0059__en-us_topic_0000001839142377_text11332943123919"><strong>Protection</strong></span> &gt; Big Data &gt; MongoDB</b></span>.</span></li>
     <li><span>Query the MongoDB database information.</span><p></p>
      <div class="p">
       This page displays information about the MongoDB instances that have been registered with the <span>product</span>.
       <ol type="a">
        <li><a href="#mongodb-0059__en-us_topic_0000001311094177_table2382789197">Table 1</a> describes the detailed instance information. 
         <div class="tablenoborder">
          <a name="mongodb-0059__en-us_topic_0000001311094177_table2382789197"></a><a name="en-us_topic_0000001311094177_table2382789197"></a>
          <table cellpadding="4" cellspacing="0" summary="" id="mongodb-0059__en-us_topic_0000001311094177_table2382789197" frame="border" border="1" rules="all">
           <caption>
            <b>Table 1 </b>Instance information
           </caption>
           <colgroup>
            <col style="width:30%">
            <col style="width:70%">
           </colgroup>
           <thead align="left">
            <tr>
             <th align="left" class="cellrowborder" valign="top" width="30%" id="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.1"><p>Parameter</p></th>
             <th align="left" class="cellrowborder" valign="top" width="70%" id="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.2"><p>Description</p></th>
            </tr>
           </thead>
           <tbody>
            <tr>
             <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.1 "><p><span><strong>Name</strong></span></p></td>
             <td class="cellrowborder" valign="top" width="70%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.2 "><p>Instance name.</p></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.1 "><p><span><strong>Status</strong></span></p></td>
             <td class="cellrowborder" valign="top" width="70%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.2 "><p>Status of the connection between the instance and the <span>product</span>.</p>
              <ul>
               <li><strong>Online</strong>: The instance is properly connected to the <span>product</span>.</li>
               <li><strong>Offline</strong>: The instance is disconnected from the <span>product</span>.</li>
              </ul></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.1 "><p><span><strong>Version</strong></span></p></td>
             <td class="cellrowborder" valign="top" width="70%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.2 "><p>MongoDB version number.</p></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.1 "><p><strong>Instance Type</strong></p></td>
             <td class="cellrowborder" valign="top" width="70%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.2 "><p>Database type, which can be <strong>Single Instance</strong>, <strong>Cluster-replica set</strong>, or <strong>Cluster-sharded</strong>.</p></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.1 "><p><span><strong>SLAs</strong></span></p></td>
             <td class="cellrowborder" valign="top" width="70%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.2 "><p>If an SLA is associated with the database, the SLA name is displayed. Otherwise, <span class="uicontrol"><b>--</b></span> is displayed.</p></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.1 "><p><span><strong>SLA Compliance</strong></span></p></td>
             <td class="cellrowborder" valign="top" width="70%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.2 ">
              <div class="p">
               Whether a backup job is successfully executed based on the backup interval or backup window set in the SLA.
               <ul id="mongodb-0059__en-us_topic_0000001792344110_en-us_topic_0000001792344094_ul1823814519293">
                <li id="mongodb-0059__en-us_topic_0000001792344110_en-us_topic_0000001792344094_li9238184572919"><strong id="mongodb-0059__en-us_topic_0000001792344110_en-us_topic_0000001792344094_b1092516394564">Met</strong>: If the backup time of a backup job (except for the first full backup) meets the backup time window or backup interval requirements and the backup job is executed successfully, SLA compliance is met.</li>
                <li id="mongodb-0059__en-us_topic_0000001792344110_en-us_topic_0000001792344094_li189142052112912"><strong id="mongodb-0059__en-us_topic_0000001792344110_en-us_topic_0000001792344094_b541520442564">Missed</strong>: If the backup time does not meet the backup time window or backup interval requirements, for example, if the backup interval set in the SLA for a backup job is once every half an hour but the backup time exceeds half an hour, SLA compliance is missed. In this case, you are advised to adjust the backup interval or backup window.</li>
               </ul>
              </div> <p></p></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.1 "><p><span><strong>Protection Status</strong></span></p></td>
             <td class="cellrowborder" valign="top" width="70%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.2 "><p>Whether the instance is protected.</p>
              <ul>
               <li><span><strong>Unprotected</strong></span>: The instance is not associated with an SLA, or the instance is associated with an SLA but protection is disabled.</li>
               <li><span><strong>Protected</strong></span>: The instance is associated with an SLA and protection is activated.</li>
               <li><span><strong>Creating</strong></span>: The instance is associated with an SLA and protection is being created.</li>
              </ul></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.1 ">
              <div class="p">
               <strong id="mongodb-0059__en-us_topic_0000001792344110_b827118125517">Tag</strong>
               <div class="note" id="mongodb-0059__en-us_topic_0000001792344110_note1330311543247">
                <span class="notetitle"> NOTE: </span>
                <div class="notebody">
                 <p id="mongodb-0059__en-us_topic_0000001792344110_p630385412417">This parameter is available only in 1.6.0 and later versions.</p>
                </div>
               </div>
              </div></td>
             <td class="cellrowborder" valign="top" width="70%" headers="mcps1.3.2.2.2.2.1.2.1.2.2.3.1.2 "><p>If a tag has been added for the resource, the tag name is displayed. Otherwise, <strong id="mongodb-0059__en-us_topic_0000001792344110_b1959642011219">--</strong> is displayed.</p></td>
            </tr>
           </tbody>
          </table>
         </div></li>
        <li>Click the name and check the overview, copy information, and jobs. 
         <ul>
          <li>Overview<p>Displays the SLA information, job execution time, and total number of copies.</p></li>
          <li>Copy data<p>You can search for copies by year, month, or day.</p> <p>If <span><img src="en-us_image_0000001792389452.png"></span> is displayed below the time, a copy at that time exists.</p></li>
          <li>Job<p>Ongoing and historical jobs are displayed.</p></li>
         </ul></li>
       </ol>
      </div> <p></p></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="mongodb-0058.html">MongoDB Environment</a>
    </div>
   </div>
  </div>
 </body>
</html>