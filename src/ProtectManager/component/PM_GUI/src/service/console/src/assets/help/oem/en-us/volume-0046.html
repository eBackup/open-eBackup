<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Restoring One or Multiple Files in a Volume Copy">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="volume-0042.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="volume-0046">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Restoring One or Multiple Files in a Volume Copy</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="volume-0046"></a><a name="volume-0046"></a>
  <h1 class="topictitle1">Restoring One or Multiple Files in a Volume Copy</h1>
  <div>
   <p>You can perform file-level restoration to quickly restore one or more files in a copy.</p>
   <div class="section">
    <h4 class="sectiontitle">Prerequisites</h4>
    <ul>
     <li>Before restoration, ensure that the remaining space of the data directory at the target location for restoration is greater than the size of the copy used for restoration before reduction. Otherwise, restoration will fail.</li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Precautions</h4>
    <p id="volume-0046__volume-0045_p1521143817711">If the file system version of the target host for restoration is earlier than that of the source host and the source volumes use features of the source host's file system version, the restored volumes on the target host will be unavailable.</p>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li><span>Choose <span class="uicontrol" id="volume-0046__en-us_topic_0000001839142377_uicontrol2728131538"><b><span id="volume-0046__en-us_topic_0000001839142377_text27283316312"><strong>Explore</strong></span> &gt; <span id="volume-0046__en-us_topic_0000001839142377_text157282318317"><strong>Copy Data</strong></span> &gt; File Systems &gt; <span id="volume-0046__en-us_topic_0000001839142377_text198473585287"><strong>Volumes</strong></span></b></span>.</span></li>
     <li><span>You can search for copies by volume resource or copy. This section describes how to search for copies by resource.</span><p></p><p>On the <span class="uicontrol"><b><span><strong>Resources</strong></span></b></span> tab page, find the resource to be restored by resource name and click the resource name.</p> <p></p></li>
     <li><span>Click <span class="uicontrol"><b><span><strong>Copy Data</strong></span></b></span> and select the year, month, and day to find the copy.</span><p></p><p>If <span><img src="en-us_image_0000001844511158.png"></span> is displayed below a month or date, copies exist in the month or on the day.</p> <p></p></li>
     <li><span>Select a copy for which an index has been created, and choose <span><strong>More</strong></span> &gt; <span><strong>File-level Restoration</strong></span>.</span><p></p>
      <div class="note">
       <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
       <div class="notebody">
        <p>Linked files do not support file-level restoration.</p>
       </div>
      </div> <p></p></li>
     <li><span>Select the files to be restored and click <span class="uicontrol" id="volume-0046__vmware_gud_0060_0_uicontrol4286263213"><b>Restore File</b></span>.</span><p></p>
      <ul id="volume-0046__vmware_gud_0060_0_ul1521452811317">
       <li id="volume-0046__vmware_gud_0060_0_li8386311313">For 1.5.0, select the files to be restored from the directory tree.</li>
       <li id="volume-0046__vmware_gud_0060_0_li12142286316">For 1.6.0 and later versions, set <strong id="volume-0046__vmware_gud_0060_0_b175942210539">File Obtaining Mode</strong>, which can be <strong id="volume-0046__vmware_gud_0060_0_b29462035125211">Select file paths from the directory tree</strong> or <strong id="volume-0046__vmware_gud_0060_0_b1211515508528">Enter file paths</strong>.
        <div class="note" id="volume-0046__vmware_gud_0060_0_note1195634711290">
         <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
         <div class="notebody">
          <ul id="volume-0046__vmware_gud_0060_0_ul14956124712297">
           <li id="volume-0046__vmware_gud_0060_0_li1095674772911">If the copy contains too many files, the system may time out, preventing you from selecting files to be restored from the directory tree. Therefore, you are advised to enter the paths of files to be restored.</li>
           <li id="volume-0046__vmware_gud_0060_0_li19956104762918">When entering a file path, enter a complete file path, for example, <strong id="volume-0046__vmware_gud_0060_0_b745510458333">/opt/abc/efg.txt</strong> or <strong id="volume-0046__vmware_gud_0060_0_b186591048123315">C:\abc\efg.txt</strong>. If you enter a folder path, for example, <strong id="volume-0046__vmware_gud_0060_0_b15108423133414">/opt/abc</strong> or <strong id="volume-0046__vmware_gud_0060_0_b13283162543410">C:\abc</strong>, all files in the folder are restored. The file name in the path is case sensitive.</li>
          </ul>
         </div>
        </div></li>
      </ul> <p></p></li>
     <li><span>On the displayed <strong>Restore File</strong> page, select <strong>Original location</strong> or <strong>New location</strong> for <strong>Restore To</strong> and set restoration parameters.</span><p></p>
      <ul>
       <li>Select <strong>Original location</strong> to restore data to the original directory of the original VM.</li>
      </ul> <p><a href="#volume-0046__Files-0045_table93951625101715">Table 1</a> describes the related parameters.</p>
      <div class="tablenoborder">
       <a name="volume-0046__Files-0045_table93951625101715"></a><a name="Files-0045_table93951625101715"></a>
       <table cellpadding="4" cellspacing="0" summary="" id="volume-0046__Files-0045_table93951625101715" frame="border" border="1" rules="all">
        <caption>
         <b>Table 1 </b>Parameters for volume restoration
        </caption>
        <colgroup>
         <col style="width:23.09%">
         <col style="width:76.91%">
        </colgroup>
        <thead align="left">
         <tr>
          <th align="left" class="cellrowborder" valign="top" width="23.09%" id="mcps1.3.4.2.6.2.3.2.3.1.1"><p>Parameter</p></th>
          <th align="left" class="cellrowborder" valign="top" width="76.91%" id="mcps1.3.4.2.6.2.3.2.3.1.2"><p>Description</p></th>
         </tr>
        </thead>
        <tbody>
         <tr>
          <td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.4.2.6.2.3.2.3.1.1 "><p><span><strong>Restore To</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.4.2.6.2.3.2.3.1.2 "><p>Location where a volume is to be restored.</p>
           <ul>
            <li><span><strong>Original location</strong></span>: Restores data to the original location on the original host.</li>
            <li><span><strong>New location</strong></span>: Restores data to a new location. You need to specify the target host and path for restoration.</li>
           </ul>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p>In file-level restoration, data can only be restored to a new location.</p>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.4.2.6.2.3.2.3.1.1 "><p><span><strong>Target Host</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.4.2.6.2.3.2.3.1.2 "><p>Target host to which data is restored.</p> <p>You can search for a host name or IP address to select the host.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.4.2.6.2.3.2.3.1.1 "><p><span><strong>Target Path</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.4.2.6.2.3.2.3.1.2 "><p>Target path to which data is restored.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.4.2.6.2.3.2.3.1.1 "><p><span><strong>Overwrite Rule</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="76.91%" headers="mcps1.3.4.2.6.2.3.2.3.1.2 "><p>If files with the same names exist in the restoration path, you can choose to replace or skip existing files.</p>
           <ul>
            <li><span><strong>Replace existing files</strong></span></li>
            <li><span><strong>Skip existing files</strong></span>: Files with the same names are skipped and are not replaced.</li>
            <li><span><strong>Only replace the files older than the restoration file</strong></span>: The latest files with the same names in the target path are retained.</li>
           </ul></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="23.09%" headers="mcps1.3.4.2.6.2.3.2.3.1.1 "><p><span><strong>Script to Run Before Restoration</strong></span></p></td>
          <td class="cellrowborder" rowspan="3" valign="top" width="76.91%" headers="mcps1.3.4.2.6.2.3.2.3.1.2 "><p>You can execute a custom script before backup, upon backup success, or upon backup failure based on your need.</p> <p>Place the execution script in the <strong id="volume-0046__volume-0045_b66555811311">DataBackup/ProtectClient/ProtectClient-E/sbin/thirdparty</strong> directory, for example, the relative path of <strong id="volume-0046__volume-0045_b465517816315">/opt/DataBackup/ProtectClient/ProtectClient-E/sbin/thirdparty/</strong>.</p> <p>You can execute custom scripts of the sh type based on your needs before a restoration job is executed, or after the restoration job is successfully or fails to be executed. In addition, place the script in the <strong id="volume-0046__volume-0045_b10333183073218">DataBackup/ProtectClient/ProtectClient-E/sbin/thirdparty</strong> directory.</p>
           <div class="note">
            <span class="notetitle"> NOTE: </span>
            <div class="notebody">
             <p>If <strong>Script to Run upon Restoration Success</strong> is configured, the status of the restoration job is displayed as <strong>Successful</strong> on the product even if the script fails to be executed. Check whether the job details contain information indicating that a post-processing script fails to be executed. If yes, modify the script in a timely manner.</p>
            </div>
           </div></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.6.2.3.2.3.1.1 "><p><span><strong>Script to Run upon Restoration Success</strong></span></p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.6.2.3.2.3.1.1 "><p><span><strong>Script to Run upon Restoration Failure</strong></span></p></td>
         </tr>
        </tbody>
       </table>
      </div> <p></p></li>
     <li><span>Click <span class="uicontrol"><b><span>OK</span></b></span>.</span></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="volume-0042.html">Restoration</a>
    </div>
   </div>
  </div>
 </body>
</html>