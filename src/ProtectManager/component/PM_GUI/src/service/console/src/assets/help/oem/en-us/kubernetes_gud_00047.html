<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Performing StatefulSet Restoration">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="kubernetes_gud_00044.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="kubernetes_gud_00047">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Performing StatefulSet Restoration</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="kubernetes_gud_00047"></a><a name="kubernetes_gud_00047"></a>
  <h1 class="topictitle1">Performing StatefulSet Restoration</h1>
  <div>
   <p>This section describes how to restore a StatefulSet that has been backed up to the original location or a new location.</p>
   <div class="section">
    <h4 class="sectiontitle">Context</h4>
    <p>The <span>product</span> can restore the data of a StatefulSet to the original or a new location using backup copies and replication copies (restoration to the original StatefulSet is not supported using replication copies).</p>
    <ul>
     <li>Restoration to the original location<p>Restores the PVC in the copy to another PVC of the same volume in the StatefulSet where the copy resides.</p></li>
     <li>Restoration to a new location<p>Restores data to the following new locations:</p>
      <ul>
       <li>A StatefulSet in the same namespace as that of the original StatefulSet in the Kubernetes cluster where the copy resides</li>
       <li>A same-name StatefulSet in a namespace of another Kubernetes cluster</li>
      </ul></li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Prerequisites</h4>
    <p>Before a StatefulSet is restored to another Kubernetes cluster, the Kubernetes cluster has been registered with the <span>product</span>. For details, see <a href="kubernetes_gud_00012.html">Step 1: Registering a Cluster</a>.</p>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Precautions</h4>
    <ul>
     <li>If services in the original pod are normal, cross-pod restoration will cause startup failure of services in the target pod. You are advised to perform cross-pod restoration when the original pod is faulty.</li>
     <li>The restoration job stops all pods in the target StatefulSet. After the restoration is complete, the system restores all pods. Ensure that no service is running in the target pod before performing the restoration.</li>
     <li>In the Kubernetes active/standby environment, if the standby node is defined in the user-defined backup script during restoration, you must switch the default standby node for backing up data to the active node. Run the <strong>zcloud shutdown:maintain=yes</strong> command to stop the zCloud service and perform restoration.</li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">Procedure</h4>
    <ol>
     <li><span>Choose <span class="uicontrol" id="kubernetes_gud_00047__kubernetes_gud_00043_en-us_topic_0000001839142377_uicontrol9790142125420"><b><span id="kubernetes_gud_00047__kubernetes_gud_00043_en-us_topic_0000001839142377_text1179013426540"><strong>Explore</strong></span> &gt; <span id="kubernetes_gud_00047__kubernetes_gud_00043_en-us_topic_0000001839142377_text67901428543"><strong>Copy Data</strong></span> &gt; <span id="kubernetes_gud_00047__kubernetes_gud_00043_en-us_topic_0000001839142377_text990214444212"><strong>Containers</strong></span> &gt; Kubernetes FlexVolume</b></span>.</span></li>
     <li><span>Search for copies by resource or copy. This section describes how to search for copies by resource.</span><p></p><p>On the <span><strong>Resources</strong></span> tab page, locate the StatefulSet to be restored by StatefulSet name and click the name.</p> <p></p></li>
     <li><span>On the <span class="uicontrol"><b><span><strong>Copy Data</strong></span></b></span> page, select the year, month, and day in sequence to locate the copy.</span><p></p><p>If <span><img src="en-us_image_0000001839274513.png"></span> is displayed below a month or day, a copy is generated in the month or on the day.</p> <p></p></li>
     <li><span>Locate the copy used for restoration, and choose <span><strong>More</strong></span> &gt; <span><strong>Restore</strong></span> on the right.</span></li>
     <li><span>Restore data to the original location or a new location.</span><p></p>
      <ul>
       <li>Restoration to the original location<p><a href="#kubernetes_gud_00047__table52750616234">Table 1</a> describes the related parameters.</p>
        <div class="tablenoborder">
         <a name="kubernetes_gud_00047__table52750616234"></a><a name="table52750616234"></a>
         <table cellpadding="4" cellspacing="0" summary="" id="kubernetes_gud_00047__table52750616234" frame="border" border="1" rules="all">
          <caption>
           <b>Table 1 </b>Parameters for restoring data to the original location
          </caption>
          <colgroup>
           <col style="width:29.87%">
           <col style="width:70.13000000000001%">
          </colgroup>
          <thead align="left">
           <tr>
            <th align="left" class="cellrowborder" valign="top" width="29.87%" id="mcps1.3.5.2.5.2.1.1.2.2.3.1.1"><p>Parameter</p></th>
            <th align="left" class="cellrowborder" valign="top" width="70.13000000000001%" id="mcps1.3.5.2.5.2.1.1.2.2.3.1.2"><p>Description</p></th>
           </tr>
          </thead>
          <tbody>
           <tr>
            <td class="cellrowborder" valign="top" width="29.87%" headers="mcps1.3.5.2.5.2.1.1.2.2.3.1.1 "><p><span><strong>Volume Mappings</strong></span> (<span><strong>Target PVC</strong></span>)</p></td>
            <td class="cellrowborder" valign="top" width="70.13000000000001%" headers="mcps1.3.5.2.5.2.1.1.2.2.3.1.2 "><p>Select the target PVC volume to which the data is restored.</p> <p>The capacity of the target PVC volume must be greater than or equal to that of the source PVC volume. Otherwise, the restoration fails.</p></td>
           </tr>
          </tbody>
         </table>
        </div></li>
       <li>Restoration to a new location
        <div class="p">
         <a href="#kubernetes_gud_00047__table15668141795417">Table 2</a> describes the related parameters. 
         <div class="tablenoborder">
          <a name="kubernetes_gud_00047__table15668141795417"></a><a name="table15668141795417"></a>
          <table cellpadding="4" cellspacing="0" summary="" id="kubernetes_gud_00047__table15668141795417" frame="border" border="1" rules="all">
           <caption>
            <b>Table 2 </b>Parameters for restoring data to a new location
           </caption>
           <colgroup>
            <col style="width:29.87%">
            <col style="width:70.13000000000001%">
           </colgroup>
           <thead align="left">
            <tr>
             <th align="left" class="cellrowborder" valign="top" width="29.87%" id="mcps1.3.5.2.5.2.1.2.1.2.2.3.1.1"><p>Parameter</p></th>
             <th align="left" class="cellrowborder" valign="top" width="70.13000000000001%" id="mcps1.3.5.2.5.2.1.2.1.2.2.3.1.2"><p>Description</p></th>
            </tr>
           </thead>
           <tbody>
            <tr>
             <td class="cellrowborder" valign="top" width="29.87%" headers="mcps1.3.5.2.5.2.1.2.1.2.2.3.1.1 "><p><span><strong>Clusters</strong></span></p></td>
             <td class="cellrowborder" valign="top" width="70.13000000000001%" headers="mcps1.3.5.2.5.2.1.2.1.2.2.3.1.2 "><p>Select the cluster to which the backup copy is restored.</p></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="29.87%" headers="mcps1.3.5.2.5.2.1.2.1.2.2.3.1.1 "><p><span><strong>Namespace</strong></span></p></td>
             <td class="cellrowborder" valign="top" width="70.13000000000001%" headers="mcps1.3.5.2.5.2.1.2.1.2.2.3.1.2 "><p>Select the namespace to which the backup copy is restored.</p></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="29.87%" headers="mcps1.3.5.2.5.2.1.2.1.2.2.3.1.1 "><p><span><strong>StatefulSet</strong></span></p></td>
             <td class="cellrowborder" valign="top" width="70.13000000000001%" headers="mcps1.3.5.2.5.2.1.2.1.2.2.3.1.2 "><p>Select the StatefulSet in the namespace to which the backup copy is restored.</p></td>
            </tr>
            <tr>
             <td class="cellrowborder" valign="top" width="29.87%" headers="mcps1.3.5.2.5.2.1.2.1.2.2.3.1.1 "><p><span><strong>Volume Mappings</strong></span> (<span><strong>Target PVC</strong></span>)</p></td>
             <td class="cellrowborder" valign="top" width="70.13000000000001%" headers="mcps1.3.5.2.5.2.1.2.1.2.2.3.1.2 "><p>Select the target PVC in the StatefulSet to which the data is restored by using backup copies.</p> <p>The capacity of the target PVC volume must be greater than or equal to that of the source PVC volume in the backup copy. Otherwise, the restoration fails.</p></td>
            </tr>
           </tbody>
          </table>
         </div>
        </div></li>
      </ul> <p></p></li>
     <li><span>Click <span><strong>Advanced</strong></span> and set advanced parameters.</span><p></p><p><a href="#kubernetes_gud_00047__table17216329908">Table 3</a> describes the related parameters.</p>
      <div class="tablenoborder">
       <a name="kubernetes_gud_00047__table17216329908"></a><a name="table17216329908"></a>
       <table cellpadding="4" cellspacing="0" summary="" id="kubernetes_gud_00047__table17216329908" frame="border" border="1" rules="all">
        <caption>
         <b>Table 3 </b>Advanced parameters
        </caption>
        <colgroup>
         <col style="width:29.87%">
         <col style="width:70.13000000000001%">
        </colgroup>
        <thead align="left">
         <tr>
          <th align="left" class="cellrowborder" valign="top" width="29.87%" id="mcps1.3.5.2.6.2.2.2.3.1.1"><p>Parameter</p></th>
          <th align="left" class="cellrowborder" valign="top" width="70.13000000000001%" id="mcps1.3.5.2.6.2.2.2.3.1.2"><p>Description</p></th>
         </tr>
        </thead>
        <tbody>
         <tr>
          <td class="cellrowborder" valign="top" width="29.87%" headers="mcps1.3.5.2.6.2.2.2.3.1.1 "><p><span><strong>Agent Host</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="70.13000000000001%" headers="mcps1.3.5.2.6.2.2.2.3.1.2 "><p>The agent host used for restoration using copies.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="29.87%" headers="mcps1.3.5.2.6.2.2.2.3.1.1 "><p><span><strong>Script to Run Before Restoration</strong></span></p></td>
          <td class="cellrowborder" rowspan="3" valign="top" width="70.13000000000001%" headers="mcps1.3.5.2.6.2.2.2.3.1.2 "><p>Enter the absolute path of the script, for example, <strong id="kubernetes_gud_00047__kubernetes_gud_00018_b358113136237">/opt/prescript.sh</strong>. Ensure that the script has been stored in the pod corresponding to the StatefulSet.</p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" headers="mcps1.3.5.2.6.2.2.2.3.1.1 "><p><span><strong>Script to Run upon Restoration Success</strong></span></p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" headers="mcps1.3.5.2.6.2.2.2.3.1.1 "><p><span><strong>Script to Run upon Restoration Failure</strong></span></p></td>
         </tr>
         <tr>
          <td class="cellrowborder" valign="top" width="29.87%" headers="mcps1.3.5.2.6.2.2.2.3.1.1 "><p><span><strong>Copy Verification Before Restoration</strong></span></p></td>
          <td class="cellrowborder" valign="top" width="70.13000000000001%" headers="mcps1.3.5.2.6.2.2.2.3.1.2 "><p>If this option is enabled, the integrity of a copy is verified before the copy is restored. This operation affects the restoration performance. This option is disabled by default. If no copy verification file is generated, this option cannot be enabled.</p></td>
         </tr>
        </tbody>
       </table>
      </div> <p></p></li>
     <li><span>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</span></li>
     <li><span>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</span></li>
     <li><span>In the Kubernetes active/standby scenario, after the restoration is complete, run the <strong>zcloud recover as master</strong> command in the pod for data restoration to set the pod to the active node.</span></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="kubernetes_gud_00044.html">Restoration</a>
    </div>
   </div>
  </div>
 </body>
</html>