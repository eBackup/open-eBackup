<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Step 4: Registering the Database of a PostgreSQL Cluster Instance">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="postgresql-0010-2.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="postgresql-0012">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Step 4: Registering the Database of a PostgreSQL Cluster Instance</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="postgresql-0012"></a><a name="postgresql-0012"></a>

<h1 class="topictitle1">Step 4: Registering the Database of a PostgreSQL Cluster Instance</h1>
<div><div class="section"><h4 class="sectiontitle">Prerequisites</h4><ul><li>Before registering a PostgreSQL cluster instance, ensure that each host in the cluster has been registered.<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p>All hosts in the cluster must have registered with the same database.</p>
</div></div>
<p>For details, see <a href="postgresql-0011.html">Step 3: Registering the Database of a Single PostgreSQL Instance</a>.</p>
</li><li>Before registering an instance, check whether the owner of the <strong>bin/lib/share</strong> file in the database installation path is <strong>root</strong>. If the owner is <strong>root</strong>, set <strong>enable_root</strong> to <strong>1</strong> in the <strong>/opt/DataBackup/ProtectClient/Plugins/GeneralDBPlugin/bin/applications/postgresql/conf/switch.conf</strong> configuration file.</li><li>Before registering a PostgreSQL cluster, query the cluster type.<p><strong>Pgpool cluster</strong></p>
<ol><li>Use PuTTY to log in to the PostgreSQL database host.</li><li>Run the <strong>ps -ef | grep pgpool</strong> command to check whether the cluster is a Pgpool cluster. If the command output contains fields related to <strong>pgpool</strong>, the current cluster is a Pgpool cluster. Otherwise, the current cluster is not a Pgpool cluster.<ul><li>If the following command output is displayed, the current cluster is a Pgpool cluster.</li></ul>
<p><span><img src="en-us_image_0000002100843889.png"></span></p>
<ul><li>If only the following information is displayed, the current cluster is not a Pgpool cluster.</li></ul>
<p><span><img src="en-us_image_0000002053191312.png"></span></p>
</li></ol>
<p><strong>Patroni cluster</strong></p>
<ol><li>Use PuTTY to log in to the PostgreSQL database host.</li><li>Run the <strong>ps -ef | grep patroni</strong> command to check whether the cluster is a Patroni cluster. If command output contains fields related to <strong>patroni</strong>, the current cluster is a Patroni cluster. Otherwise, the current cluster is not a Patroni cluster.<ul><li>If the following command output is displayed, the current cluster is a Patroni cluster.</li></ul>
<p><span><img src="en-us_image_0000002101715833.png"></span></p>
<ul><li>If only the following information is displayed in the command output, the current cluster is not a Patroni cluster.</li></ul>
<p><span><img src="en-us_image_0000002089366285.png"></span></p>
</li></ol>
</li></ul>
</div>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li><span>Create a cluster.</span><p><ol type="a"><li>On the PostgreSQL page, click the <span class="uicontrol"><b>Cluster</b></span> tab.</li><li>Click <span class="uicontrol"><b>Register</b></span>.</li><li>Configure the cluster and its authentication information.<p><a href="#postgresql-0012__table415124821919">Table 1</a> describes related parameters.</p>

<div class="tablenoborder"><a name="postgresql-0012__table415124821919"></a><a name="table415124821919"></a><table cellpadding="4" cellspacing="0" summary="" id="postgresql-0012__table415124821919" frame="border" border="1" rules="all"><caption><b>Table 1 </b>PostgreSQL cluster registration information</caption><colgroup><col style="width:28.050000000000004%"><col style="width:71.95%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="28.050000000000004%" id="mcps1.3.2.2.1.2.1.3.2.2.3.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="71.95%" id="mcps1.3.2.2.1.2.1.3.2.2.3.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="28.050000000000004%" headers="mcps1.3.2.2.1.2.1.3.2.2.3.1.1 "><p><span><strong>Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="71.95%" headers="mcps1.3.2.2.1.2.1.3.2.2.3.1.2 "><p>Enter a custom cluster name.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="28.050000000000004%" headers="mcps1.3.2.2.1.2.1.3.2.2.3.1.1 "><p><span><strong>Cluster Type</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="71.95%" headers="mcps1.3.2.2.1.2.1.3.2.2.3.1.2 "><p>Available options are <strong>Pgpool</strong> and <strong>Patroni</strong>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="28.050000000000004%" headers="mcps1.3.2.2.1.2.1.3.2.2.3.1.1 "><p><span><strong>Type</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="71.95%" headers="mcps1.3.2.2.1.2.1.3.2.2.3.1.2 "><p>Select <span class="uicontrol"><b>Active/Standby Replication</b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="28.050000000000004%" headers="mcps1.3.2.2.1.2.1.3.2.2.3.1.1 "><p><span><strong>Virtual IP Address</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="71.95%" headers="mcps1.3.2.2.1.2.1.3.2.2.3.1.2 "><p>Enter a custom virtual IP address that is unused and in the same network segment as that of the host where the PostgreSQL instance is located.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="28.050000000000004%" headers="mcps1.3.2.2.1.2.1.3.2.2.3.1.1 "><p><span><strong>Nodes</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="71.95%" headers="mcps1.3.2.2.1.2.1.3.2.2.3.1.2 "><p>Select the host where the PostgreSQL instance is located.</p>
</td>
</tr>
</tbody>
</table>
</div>
</li><li>Click <span class="uicontrol"><b>OK</b></span>.</li></ol>
</p></li><li><span>Register the database of a cluster instance.</span><p><ol type="a"><li>On the PostgreSQL page, click the <span class="uicontrol"><b>Instance</b></span> tab.</li><li>Click <span class="uicontrol"><b>Register</b></span>.</li><li>Set <span class="uicontrol"><b><span><strong>Type</strong></span></b></span> to <span class="uicontrol"><b><span><strong>Cluster instance</strong></span></b></span>.</li><li>Configure the database instance and its authentication information.<p><a href="#postgresql-0012__table12398130131217">Table 2</a> describes related parameters.</p>

<div class="tablenoborder"><a name="postgresql-0012__table12398130131217"></a><a name="table12398130131217"></a><table cellpadding="4" cellspacing="0" summary="" id="postgresql-0012__table12398130131217" frame="border" border="1" rules="all"><caption><b>Table 2 </b>PostgreSQL cluster instance registration information</caption><colgroup><col style="width:28.050000000000004%"><col style="width:71.95%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="28.050000000000004%" id="mcps1.3.2.2.2.2.1.4.2.2.3.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="71.95%" id="mcps1.3.2.2.2.2.1.4.2.2.3.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="28.050000000000004%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.1 "><p><span><strong>Name</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="71.95%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.2 "><p>Username for installing the database.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="28.050000000000004%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.1 "><p><span><strong>Clusters</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="71.95%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.2 "><p>Select the database host to be registered.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="28.050000000000004%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.1 "><p><span><strong>Username</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="71.95%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.2 "><p>OS username of the database.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="28.050000000000004%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.1 "><p><span><strong>Pgpool Port</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="71.95%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.2 "><p>This parameter is not displayed when the database host of the Patroni cluster is selected.</p>
<p>Port number used by the Pgpool service to listen on TCP/IP connections.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="28.050000000000004%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.1 "><p><span><strong>Database Username</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="71.95%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.2 "><p>Name of the database administrator.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="28.050000000000004%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.1 "><p><span><strong>Database Password</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="71.95%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.2 "><p>Password of the database administrator.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="28.050000000000004%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.1 "><p><span><strong>Database Stream Replication Username</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="71.95%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.2 "><p>Username for streaming replication in the database.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="28.050000000000004%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.1 "><p><span><strong>Database Stream Replication Password</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="71.95%" headers="mcps1.3.2.2.2.2.1.4.2.2.3.1.2 "><p>Password for streaming replication in the database.</p>
</td>
</tr>
</tbody>
</table>
</div>
</li><li>Click <strong>Add</strong>. In the displayed <strong>Add</strong> dialog box, add all nodes of the cluster instance.<p><a href="#postgresql-0012__en-us_topic_0000001311214069_table241515964115">Table 3</a> describes related parameters.</p>

<div class="tablenoborder"><a name="postgresql-0012__en-us_topic_0000001311214069_table241515964115"></a><a name="en-us_topic_0000001311214069_table241515964115"></a><table cellpadding="4" cellspacing="0" summary="" id="postgresql-0012__en-us_topic_0000001311214069_table241515964115" frame="border" border="1" rules="all"><caption><b>Table 3 </b>Node information of a cluster instance</caption><colgroup><col style="width:25.629999999999995%"><col style="width:74.37%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="25.629999999999995%" id="mcps1.3.2.2.2.2.1.5.4.2.3.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="74.37%" id="mcps1.3.2.2.2.2.1.5.4.2.3.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.2.2.2.2.1.5.4.2.3.1.1 "><p><span><strong>Hosts</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.2.2.2.2.1.5.4.2.3.1.2 "><p>Host node in the cluster.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.2.2.2.2.1.5.4.2.3.1.1 "><p><span><strong>Database Installation Path</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.2.2.2.2.1.5.4.2.3.1.2 "><p>Path for installing the database to be registered.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.2.2.2.2.1.5.4.2.3.1.1 "><p><strong>Full Path of Patroni Configuration File</strong></p>
<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><p>This parameter is displayed only in 1.6.0 and later versions.</p>
</div></div>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.2.2.2.2.1.5.4.2.3.1.2 "><p>When configuring a cluster instance, set the cluster to the database host of the Patroni cluster. This parameter is displayed when you add a cluster instance node.</p>
<p>Full path of the Patroni configuration file.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.2.2.2.2.1.5.4.2.3.1.1 "><p><span><strong>Pgpool Installation Path</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.2.2.2.2.1.5.4.2.3.1.2 "><p>Installation path of the Pgpool service.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.2.2.2.2.1.5.4.2.3.1.1 "><p><span><strong>Service Plane IP Address</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.2.2.2.2.1.5.4.2.3.1.2 "><p>IP address of the host for the database service to receive TCP/IP connections.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.629999999999995%" headers="mcps1.3.2.2.2.2.1.5.4.2.3.1.1 "><p><span><strong>Database Port</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="74.37%" headers="mcps1.3.2.2.2.2.1.5.4.2.3.1.2 "><p>Port number used by the database service to listen on TCP/IP connections.</p>
</td>
</tr>
</tbody>
</table>
</div>
</li><li>Select the registered instance name of each cluster member in sequence.</li><li>Click <span class="uicontrol"><b>OK</b></span>.</li></ol>
</p></li></ol>
</div>
<p></p>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="postgresql-0010-2.html">Backing Up PostgreSQL</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>