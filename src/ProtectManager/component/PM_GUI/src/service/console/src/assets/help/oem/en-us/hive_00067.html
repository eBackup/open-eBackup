<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Managing Backup Sets">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="hive_00064.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="hive_00067">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Managing Backup Sets</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="hive_00067"></a><a name="hive_00067"></a>
  <h1 class="topictitle1">Managing Backup Sets</h1>
  <div>
   <p>The <span>product</span> allows you to modify, remove, activate, or disable protection for backup sets.</p>
   <div class="section">
    <h4 class="sectiontitle">Related Operations</h4>
    <p>Log in to the management page, choose <span class="uicontrol"><b><span id="hive_00067__en-us_topic_0000001839142377_text8651165012398"><strong>Protection</strong></span> &gt; Big Data &gt; Hive</b></span>, click the <span class="uicontrol"><b>Backup Sets</b></span> tab, and locate the desired backup set.</p>
   </div>
   <p><a href="#hive_00067__table24934294919">Table 1</a> describes the related operations.</p>
   <div class="tablenoborder">
    <a name="hive_00067__table24934294919"></a><a name="table24934294919"></a>
    <table cellpadding="4" cellspacing="0" summary="" id="hive_00067__table24934294919" frame="border" border="1" rules="all">
     <caption>
      <b>Table 1 </b>Operations related to backup set protection
     </caption>
     <colgroup>
      <col style="width:25%">
      <col style="width:45%">
      <col style="width:30%">
     </colgroup>
     <thead align="left">
      <tr>
       <th align="left" class="cellrowborder" valign="top" width="25%" id="mcps1.3.4.2.4.1.1"><p>Operation</p></th>
       <th align="left" class="cellrowborder" valign="top" width="45%" id="mcps1.3.4.2.4.1.2"><p>Description</p></th>
       <th align="left" class="cellrowborder" valign="top" width="30%" id="mcps1.3.4.2.4.1.3"><p>Navigation Path</p></th>
      </tr>
     </thead>
     <tbody>
      <tr>
       <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.4.2.4.1.1 "><p>Modifying protection</p></td>
       <td class="cellrowborder" valign="top" width="45%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to modify a protection plan to associate another SLA with a backup set and modify the execution scripts before and after backup set protection. After the modification, the next backup will be executed based on the new protection plan.</p> <p><strong>Note</strong></p> <p>The modification does not affect the protection job that is being executed. The modified protection plan will take effect in the next protection period.</p></td>
       <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target backup set, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify Protection</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.4.2.4.1.1 "><p>Removing protection</p></td>
       <td class="cellrowborder" valign="top" width="45%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to remove the protection of a backup set when it does not need to be protected. After the removal, the system cannot execute protection jobs for the backup set. To protect the backup set again, associate an SLA with it again.</p> <p><strong>Note</strong></p> <p>Protection cannot be removed when a protection job of the backup set is running.</p></td>
       <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target backup set, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Remove Protection</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.4.2.4.1.1 "><p>Activating protection</p></td>
       <td class="cellrowborder" valign="top" width="45%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to re-activate a disabled protection plan of a backup set. After the protection plan is activated, the system backs up the backup set based on the protection plan.</p></td>
       <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target backup set, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Activate Protection</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.4.2.4.1.1 "><p>Disabling protection</p></td>
       <td class="cellrowborder" valign="top" width="45%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to disable the protection plan of a backup set when its periodic protection is not needed. After the protection plan is disabled, the system does not execute the automatic backup job of the backup set.</p> <p><strong>Note</strong></p> <p>Disabling protection does not affect ongoing backup jobs.</p></td>
       <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target backup set, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Disable Protection</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.4.2.4.1.1 "><p>Manual backup</p></td>
       <td class="cellrowborder" valign="top" width="45%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to perform a backup immediately.</p> <p><strong>Note</strong></p> <p>Copies generated by manual backup are retained for the duration defined in the SLA.</p></td>
       <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target backup set, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Manual Backup</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.4.2.4.1.1 "><p>Modifying a backup set</p></td>
       <td class="cellrowborder" valign="top" width="45%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to modify the name of, and the selected directory and table in a backup set.</p></td>
       <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target backup set, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.4.2.4.1.1 "><p>Deleting a backup set</p></td>
       <td class="cellrowborder" valign="top" width="45%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to delete a backup set if you do not need to protect it.</p> <p><strong>Note</strong></p> <p>A backup set that has been associated with an SLA cannot be deleted.</p></td>
       <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target backup set, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Delete</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.4.2.4.1.1 ">
        <div class="p">
         Adding a tag
         <div class="note">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <p>Only 1.6.0 and later versions support this function.</p>
          </div>
         </div>
        </div></td>
       <td class="cellrowborder" valign="top" width="45%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to quickly filter and manage resources.</p></td>
       <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.4.2.4.1.3 ">
        <div class="p">
         Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; Add Tag</b></span>.
         <div class="note">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <ul>
            <li>On the displayed <strong>Add Tag</strong> page, you can select existing tags or click <strong>Create</strong> to create a tag.</li>
            <li>To modify or delete a tag, click <strong>Go to Tag Management</strong> on the <strong>Add Tag</strong> page to go to the <strong>Tag Management</strong> page.</li>
           </ul>
          </div>
         </div>
        </div></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="25%" headers="mcps1.3.4.2.4.1.1 ">
        <div class="p">
         Removing a tag
         <div class="note">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <p id="hive_00067__en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
          </div>
         </div>
        </div></td>
       <td class="cellrowborder" valign="top" width="45%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to remove a tag of a resource when it is no longer needed.</p></td>
       <td class="cellrowborder" valign="top" width="30%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; Remove Tag</b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" colspan="3" valign="top" headers="mcps1.3.4.2.4.1.1 mcps1.3.4.2.4.1.2 mcps1.3.4.2.4.1.3 "><p>Note: The procedure for restoring Hive backup sets is similar to that in <a href="hive_00049.html">Restoration</a> and is not described here.</p></td>
      </tr>
     </tbody>
    </table>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="hive_00064.html">Hive Cluster Environment</a>
    </div>
   </div>
  </div>
 </body>
</html>