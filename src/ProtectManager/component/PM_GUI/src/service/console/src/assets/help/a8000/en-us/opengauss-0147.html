<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Restoring openGauss">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="opengauss-0144.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="opengauss-0147">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Restoring openGauss</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="opengauss-0147"></a><a name="opengauss-0147"></a>

<h1 class="topictitle1">Restoring openGauss</h1>
<div><p>This section describes how to restore a backed-up openGauss backup set to the original or a new location.</p>
<div class="section"><h4 class="sectiontitle">Context</h4><div class="p">Backup copies, replication copies, archive copies, and imported copies can be used for restoration. Restoration to the original or a new location is supported.<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p>Replication copies and their archive copies cannot be used for restoration to the original location.</p>
</div></div>
</div>
</div>
<div class="section"><h4 class="sectiontitle">Precautions</h4><ul><li>During instance restoration based on openGauss 3.0, if a restoration job fails and the cluster is in the degraded state (the active node is started but the standby node fails to be started and is in the degraded state), manually rebuild the standby node.</li><li>Log in to the standby node as the database system user and run the <strong>gs_ctl build</strong> <em>Data directory</em> command to rebuild the standby node.</li></ul>
<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p>The data directory is specified upon instance running. If no data directory is specified, the data directory configured on the database active node during installation and deployment is used by default.</p>
</div></div>
</div>
<div class="section"><h4 class="sectiontitle">Prerequisites</h4><ul><li>Before restoration, ensure that the remaining space of the data directory at the target location for restoration is greater than the size of the copy used for restoration before reduction. Otherwise, restoration will fail.</li></ul>
<ul><li>Before restoring data to a new location, ensure that the database installation user has the read and write permissions on the target path for restoration.</li><li>Before restoration, ensure that the target database system is normal.<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><p>On a database single-node system or any node in the cluster, run <strong>gs_om -t status</strong> as the OS account for running the database. If the value of <strong>cluster_state</strong> is <strong>Normal</strong> in the command output, the database system is normal.</p>
</div></div>
</li></ul>
</div>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li><span>Choose <span class="uicontrol" id="opengauss-0147__en-us_topic_0000001839142377_uicontrol13535171992714"><b><span id="opengauss-0147__en-us_topic_0000001839142377_text19535171918274"><strong>Explore</strong></span> &gt; <span id="opengauss-0147__en-us_topic_0000001839142377_text175351119172711"><strong>Copy Data</strong></span> &gt; <span id="opengauss-0147__en-us_topic_0000001839142377_text59261952124911"><strong>Databases</strong></span> &gt; <span id="opengauss-0147__en-us_topic_0000001839142377_text11575567321"><strong>openGauss</strong></span></b></span>.</span></li><li><span>You can search for copies by openGauss resource or copy. This section describes how to search for copies by resource.</span><p><p>On the <span class="uicontrol"><b><span><strong>Resources</strong></span></b></span> tab page, find the resource to be restored by resource name and click the resource name.</p>
</p></li><li><span>Select the year, month, and day in sequence to find the copy.</span><p><p><span><img src="en-us_image_0000001792390552.png"></span> above the time indicates that copies exist in the month or on the day.</p>
</p></li><li><span>Specify a copy for restoration.</span><p><ul><li>Restoration using a specified copy<p>On the <strong>Copy Data</strong> page, click <span><img src="en-us_image_0000002044857604.png"></span>. In the row where the copy used for restoration resides, choose <span class="menucascade"><b><span class="uicontrol"><span><strong>More</strong></span></span></b> &gt; <b><span class="uicontrol"><span><strong>Restore</strong></span></span></b></span>.</p>
</li></ul>
<div class="p"><a href="#opengauss-0147__en-us_topic_0000001385658689_table194961441141219">Table 1</a> describes the related parameters.
<div class="tablenoborder"><a name="opengauss-0147__en-us_topic_0000001385658689_table194961441141219"></a><a name="en-us_topic_0000001385658689_table194961441141219"></a><table cellpadding="4" cellspacing="0" summary="" id="opengauss-0147__en-us_topic_0000001385658689_table194961441141219" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Restoring an openGauss backup set</caption><colgroup><col style="width:32.019999999999996%"><col style="width:67.97999999999999%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="32.019999999999996%" id="mcps1.3.5.2.4.2.2.2.2.3.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="67.97999999999999%" id="mcps1.3.5.2.4.2.2.2.2.3.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="32.019999999999996%" headers="mcps1.3.5.2.4.2.2.2.2.3.1.1 "><p><span><strong>Restore To</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="67.97999999999999%" headers="mcps1.3.5.2.4.2.2.2.2.3.1.2 "><p>Select <span class="uicontrol"><b><span><strong>Original location</strong></span></b></span> or <span class="uicontrol"><b><span><strong>New location</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="32.019999999999996%" headers="mcps1.3.5.2.4.2.2.2.2.3.1.1 "><p><span><strong>Location</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="67.97999999999999%" headers="mcps1.3.5.2.4.2.2.2.2.3.1.2 "><p>If you select restoration to <span class="uicontrol"><b><span><strong>Original location</strong></span></b></span>, the target location is displayed by default.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="32.019999999999996%" headers="mcps1.3.5.2.4.2.2.2.2.3.1.1 "><p><span><strong>Target Cluster</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="67.97999999999999%" headers="mcps1.3.5.2.4.2.2.2.2.3.1.2 "><p>If you select restoration to <span class="uicontrol"><b><span><strong>New location</strong></span></b></span>, you can select a target cluster.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="32.019999999999996%" headers="mcps1.3.5.2.4.2.2.2.2.3.1.1 "><p><span><strong>Target Instance</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="67.97999999999999%" headers="mcps1.3.5.2.4.2.2.2.2.3.1.2 "><p>If you select restoration to <span class="uicontrol"><b><span><strong>New location</strong></span></b></span>, you can select a target instance.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="32.019999999999996%" headers="mcps1.3.5.2.4.2.2.2.2.3.1.1 "><p><span><strong>Rename Database</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="67.97999999999999%" headers="mcps1.3.5.2.4.2.2.2.2.3.1.2 "><p>This parameter is displayed only for database restoration. After this function is enabled, you can rename the restored database.</p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</p></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="opengauss-0144.html">Restoration</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>