<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-cn" xml:lang="zh-cn">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="恢复Oracle数据库中的单个表或多个表（适用于1.6.0及后续版本）">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="oracle_gud_0055.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect 备份一体机 1.5.0-1.6.0 帮助中心">
  <meta name="DC.Publisher" content="20240320">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="oracle_gud_0131">
  <meta name="DC.Language" content="zh-cn">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>恢复Oracle数据库中的单个表或多个表（适用于1.6.0及后续版本）</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="oracle_gud_0131"></a><a name="oracle_gud_0131"></a>
  <h1 class="topictitle1">恢复Oracle数据库中的单个表或多个表（适用于1.6.0及后续版本）</h1>
  <div>
   <p>当需要恢复Oracle数据库中的单个表或多个表时，请参考本节恢复数据库中的表至原位置或新位置。</p>
   <div class="section">
    <h4 class="sectiontitle">背景信息</h4>
    <ul>
     <li>不同版本的操作界面可能会有少许差异，操作时请以实际情况为准。</li>
     <li>当前仅支持Linux主机的Oracle数据库实现表级恢复。</li>
     <li>表级恢复仅恢复表中的数据，恢复后的表的相关权限以及外键等约束需要用户重新配置。</li>
    </ul>
   </div>
   <div class="section">
    <h4 class="sectiontitle">前提条件</h4>
    <p>恢复至的目标主机已安装ProtectAgent及其他相关软件。</p>
    <p>具体操作可参考《ProtectAgent安装指南》。</p>
   </div>
   <div class="section">
    <h4 class="sectiontitle">操作步骤</h4>
    <ol>
     <li><span>检查并配置Oracle数据库的Open状态，具体操作请参见<a href="oracle_gud_0016.html">检查并配置Oracle数据库的Open状态</a>。</span></li>
     <li><span>检查目标数据库是否为归档模式，具体操作请参见<a href="oracle_gud_0017.html">检查并配置Oracle数据库的归档模式</a>。</span></li>
     <li><span>检查目标数据库是否为读写模式。</span><p></p>
      <ol type="a">
       <li>登录Oracle数据库主机，以Linux操作系统为例。<p>请使用PuTTY登录Oracle数据库主机，并执行<strong>su - oracle</strong>命令，切换到<strong>oracle</strong>账户。</p></li>
       <li>登录Oracle数据库实例。
        <ul>
         <li>若为OS认证，请执行以下命令：<pre class="screen">export ORACLE_SID<strong>=</strong><em>数据库实例名称</em>
sqlplus / as sysdba</pre>
          <div class="note">
           <img src="public_sys-resources/note_3.0-zh-cn.png"><span class="notetitle"> </span>
           <div class="notebody">
            <p>当数据库采用OS认证方式时，进入“<em>ORACLE_HOME</em><em>的路径</em>\network\admin”路径下查看<strong>sqlnet.ora</strong>配置文件，若文件中存在SQLNET.AUTHENTICATION_SERVICES= (ALL) 内容，请删除该内容。否则会导致表级恢复任务失败。</p>
           </div>
          </div></li>
         <li>若为数据库认证，请执行以下命令：<pre class="screen">export ORACLE_SID<strong>=</strong><em>数据库实例名称</em>
sqlplus</pre> <p>根据提示输入具备<strong>sysdba</strong>权限的用户：</p> <pre class="screen"><em>username </em>as sysdba</pre> <p>根据提示输入密码。</p></li>
        </ul></li>
       <li>执行以下命令查看是否开启读写模式。<pre class="screen">select OPEN_MODE from v$database;</pre> <p>回显类似如下表示已开启读写模式：</p> <pre class="screen">SQL&gt; select OPEN_MODE from v$database;
<strong>OPEN_MODE</strong>
--------------------
READ WRITE</pre> <p>如果未开启读写模式时，依次执行以下命令开启。</p> <pre class="screen">shutdown immediate;
startup mount;
alter database open read write;</pre>
        <div class="note">
         <img src="public_sys-resources/note_3.0-zh-cn.png"><span class="notetitle"> </span>
         <div class="notebody">
          <p>在开启数据库读写模式过程中会关闭数据库。此操作会有一定的停机时间，需要注意停机时间再进行处理。</p>
         </div>
        </div></li>
      </ol> <p></p></li>
     <li><span>检查目标数据库实例中的COMPATIBLE初始化参数是否设置为12.<em>x.x</em>或后续版本。</span><p></p><pre class="screen">show parameter compatible;</pre> <p>如果回显版本不是12或后续版本，请执行以下命令修改版本号。</p> <pre class="screen">ALTER SYSTEM SET COMPATIBLE = '<em>版本号</em>' SCOPE = SPFILE;
shutdown immediate;
startup;</pre>
      <div class="note">
       <img src="public_sys-resources/note_3.0-zh-cn.png"><span class="notetitle"> </span>
       <div class="notebody">
        <p>命令中的<em>版本号</em>请替换为12或后续版本号，建议与数据库版本保持一致。</p>
       </div>
      </div> <p></p></li>
     <li><span>选择<span class="uicontrol" id="oracle_gud_0131__zh-cn_topic_0000001839142377_uicontrol1186171611461">“<span id="oracle_gud_0131__zh-cn_topic_0000001839142377_text6186101615464">数据利用</span> &gt; <span id="oracle_gud_0131__zh-cn_topic_0000001839142377_text74373665812">副本数据</span> &gt; <span id="oracle_gud_0131__zh-cn_topic_0000001839142377_text12413710445">数据库</span> &gt; <span id="oracle_gud_0131__zh-cn_topic_0000001839142377_text151861216124615">Oracle</span>”</span>。</span></li>
     <li><span>您可以以数据库资源维度或以副本维度查找副本，本节以资源维度为例进行说明。</span><p></p><p>在<span class="wintitle">“<span>资源</span>”</span>页签，根据数据库名称查找到需要恢复的数据库，并单击数据库名称。</p> <p></p></li>
     <li><span>指定副本进行恢复。</span><p></p>
      <ol type="a">
       <li>在“<span>时间选择</span>”页签依次选择年、月、天查找副本。<p>当时间上显示<span><img src="zh-cn_image_0000001839229166.png"></span>，即表示该月、该天存在副本。</p></li>
       <li>单击右侧<span><img src="zh-cn_image_0000002014401216.png"></span>，在副本所在行选择<span class="uicontrol">“更多 &gt; 表级恢复”</span>，指定某个副本进行表级恢复。<p></p></li>
       <li>在“表级恢复”界面，选择需要恢复的表恢复至原位置或新位置。
        <ol class="substepthirdol">
         <li>当选择恢复至<span class="uicontrol">“新位置”</span>时，需选择目标主机以及数据库。</li>
         <li>在“可选表”页签选择需要恢复的单个或多个表，可在“已选表”页签查看已选择待恢复的表。</li>
         <li>在待恢复的表所在行，可选择表恢复后相关参数如<a href="#oracle_gud_0131__table192027327712">表1</a>所示。 
          <div class="tablenoborder">
           <a name="oracle_gud_0131__table192027327712"></a><a name="table192027327712"></a>
           <table cellpadding="4" cellspacing="0" summary="" id="oracle_gud_0131__table192027327712" frame="border" border="1" rules="all">
            <caption>
             <b>表1 </b>恢复任务参数说明
            </caption>
            <colgroup>
             <col style="width:29.299999999999997%">
             <col style="width:70.7%">
            </colgroup>
            <thead align="left">
             <tr>
              <th align="left" class="cellrowborder" valign="top" width="29.299999999999997%" id="mcps1.3.4.2.7.2.1.3.1.3.2.2.3.1.1"><p>参数</p></th>
              <th align="left" class="cellrowborder" valign="top" width="70.7%" id="mcps1.3.4.2.7.2.1.3.1.3.2.2.3.1.2"><p>说明</p></th>
             </tr>
            </thead>
            <tbody>
             <tr>
              <td class="cellrowborder" valign="top" width="29.299999999999997%" headers="mcps1.3.4.2.7.2.1.3.1.3.2.2.3.1.1 "><p>恢复后所属用户名</p></td>
              <td class="cellrowborder" valign="top" width="70.7%" headers="mcps1.3.4.2.7.2.1.3.1.3.2.2.3.1.2 "><p>恢复后的表所属的目标数据库用户名。</p> <p>默认为待恢复的表所属的数据库用户名。</p></td>
             </tr>
             <tr>
              <td class="cellrowborder" valign="top" width="29.299999999999997%" headers="mcps1.3.4.2.7.2.1.3.1.3.2.2.3.1.1 "><p>恢复后所属表空间</p></td>
              <td class="cellrowborder" valign="top" width="70.7%" headers="mcps1.3.4.2.7.2.1.3.1.3.2.2.3.1.2 "><p>恢复后的表所属的目标数据库表空间。</p> <p>默认为待恢复的表所属的数据库表空间。</p></td>
             </tr>
             <tr>
              <td class="cellrowborder" valign="top" width="29.299999999999997%" headers="mcps1.3.4.2.7.2.1.3.1.3.2.2.3.1.1 "><p>恢复后表名称</p></td>
              <td class="cellrowborder" valign="top" width="70.7%" headers="mcps1.3.4.2.7.2.1.3.1.3.2.2.3.1.2 "><p>恢复后的表名称。</p> <p>默认为待恢复的表名称。</p></td>
             </tr>
            </tbody>
           </table>
          </div>
          <div class="note">
           <img src="public_sys-resources/note_3.0-zh-cn.png"><span class="notetitle"> </span>
           <div class="notebody">
            <p>当选择待恢复的表归属于PDB时，请执行以下操作修改目标主机中Oracle软件的网络配置文件。</p>
            <ol>
             <li>登录目标Oracle数据库主机。</li>
             <li>执行<strong>su - oracle</strong>命令，切换到<strong>oracle</strong>账户，打开<strong>tnsnames.ora</strong>配置文件。<pre class="screen">vi $ORACLE_HOME/network/admin/tnsnames.ora</pre></li>
             <li>在配置文件中为每个待恢复表所属原PDB与目标PDB新增以下内容，其中<em>原PDB名称</em>，<em>目标PDB名称</em>以及<em>目标数据库主机IP</em>需根据实际情况替换。<p>以恢复一张表为例：</p>
              <ul>
               <li>若该表的<em>原PDB名称</em>和<em>目标PDB名称</em>不相同，请新增以下内容。<pre class="screen"><em><strong>原PDB名称</strong></em>=
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = <em><strong>目标数据库主机IP</strong></em>)(PORT = 1521))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME =<strong> <em>原PDB名称</em></strong>)
    )
  )
<em><strong>目标PDB名称</strong></em>=
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = <em><strong>目标数据库主机IP</strong></em>)(PORT = 1521))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME =<strong> </strong><em><strong>目标PDB名称</strong></em>)
    )
  )</pre></li>
               <li>若该表的<em>原PDB名称</em>和<em>目标PDB名称</em>相同，请新增以下内容。<pre class="screen"><em><strong>原PDB名称</strong></em>=
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = <em><strong>目标数据库主机IP</strong></em>)(PORT = 1521))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME =<strong> <em>原PDB名称</em></strong>)
    )
  )</pre></li>
              </ul></li>
            </ol>
           </div>
          </div></li>
         <li>当目标恢复位置存在重名表时，需勾选<span class="uicontrol">“删除重名表”</span>功能，否则将导致恢复任务失败。
          <div class="note">
           <img src="public_sys-resources/note_3.0-zh-cn.png"><span class="notetitle"> </span>
           <div class="notebody">
            <p>登录目标Oracle数据库主机，在数据库实例中执行以下命令检查目标数据库是否存在同名表。</p>
            <pre class="screen">select count(*) count from all_tables where owner = UPPER('<em>用户名</em>') and TABLE_NAME = UPPER('<em>表名</em>');</pre>
            <ul>
             <li>如果回显数量为0，则表示不存在同名表。</li>
             <li>如果不为0，则表示存在同名表。</li>
            </ul>
           </div>
          </div></li>
         <li>配置恢复高级参数。<p>相关参数如<a href="#oracle_gud_0131__table2019615469249">表2</a>所示。</p>
          <div class="tablenoborder">
           <a name="oracle_gud_0131__table2019615469249"></a><a name="table2019615469249"></a>
           <table cellpadding="4" cellspacing="0" summary="" id="oracle_gud_0131__table2019615469249" frame="border" border="1" rules="all">
            <caption>
             <b>表2 </b>恢复任务高级参数说明
            </caption>
            <colgroup>
             <col style="width:32.86%">
             <col style="width:67.14%">
            </colgroup>
            <thead align="left">
             <tr>
              <th align="left" class="cellrowborder" valign="top" width="32.86%" id="mcps1.3.4.2.7.2.1.3.1.5.2.2.3.1.1"><p>参数</p></th>
              <th align="left" class="cellrowborder" valign="top" width="67.14%" id="mcps1.3.4.2.7.2.1.3.1.5.2.2.3.1.2"><p>说明</p></th>
             </tr>
            </thead>
            <tbody>
             <tr>
              <td class="cellrowborder" valign="top" width="32.86%" headers="mcps1.3.4.2.7.2.1.3.1.5.2.2.3.1.1 "><p><span>通道数</span></p></td>
              <td class="cellrowborder" valign="top" width="67.14%" headers="mcps1.3.4.2.7.2.1.3.1.5.2.2.3.1.2 "><p>控制恢复任务的并行度，通过该参数指定RMAN与数据库实例之间的最大连接数。如果主机性能一般，建议保持默认值；如果主机性能良好，可以适量增大通道数，提高并发度，提升恢复效率，建议通道数与数据文件的数量保持一致。</p> <p>取值范围：1~254。</p></td>
             </tr>
             <tr>
              <td class="cellrowborder" valign="top" width="32.86%" headers="mcps1.3.4.2.7.2.1.3.1.5.2.2.3.1.1 "><p><span>执行脚本</span></p></td>
              <td class="cellrowborder" valign="top" width="67.14%" headers="mcps1.3.4.2.7.2.1.3.1.5.2.2.3.1.2 "><p>您可以根据实际需求，在恢复任务执行前、执行成功后、执行失败后，执行自定义脚本。</p></td>
             </tr>
             <tr>
              <td class="cellrowborder" valign="top" width="32.86%" headers="mcps1.3.4.2.7.2.1.3.1.5.2.2.3.1.1 "><p><span>恢复前执行脚本</span></p></td>
              <td class="cellrowborder" rowspan="3" valign="top" width="67.14%" headers="mcps1.3.4.2.7.2.1.3.1.5.2.2.3.1.2 ">
               <ul>
                <li id="oracle_gud_0131__oracle_gud_0029_li1460341514374"><span>如果是Linux操作系统，请输入脚本名称，脚本名称以</span><strong id="oracle_gud_0131__oracle_gud_0029_b1732018117386">.sh</strong><span>结尾。确保脚本已存放在</span><span>ProtectAgent安装目录下的</span><span class="filepath" id="oracle_gud_0131__oracle_gud_0029_filepath560914216351">“DataBackup/ProtectClient/ProtectClient-E/sbin/thirdparty”</span><span>目录中，且登录数据库主机的用户（默认为</span><strong id="oracle_gud_0131__oracle_gud_0029_b95043281423">root</strong><span>）对该脚本有可执行权限。如果无执行权限请依次执行以下命令授权：</span><p id="oracle_gud_0131__oracle_gud_0029_p16108247123612"><strong id="oracle_gud_0131__oracle_gud_0029_b81136193713">chown root:root</strong> <em id="oracle_gud_0131__oracle_gud_0029_i16108134713615">脚本名称</em></p> <p id="oracle_gud_0131__oracle_gud_0029_p161081847163612"><strong id="oracle_gud_0131__oracle_gud_0029_b911119173711">chmod 500</strong><strong id="oracle_gud_0131__oracle_gud_0029_b87241853163611"> </strong><em id="oracle_gud_0131__oracle_gud_0029_i107241353203616">脚本名称</em></p></li>
                <li id="oracle_gud_0131__oracle_gud_0029_li9864508377"><span>如果是Windows操作系统，请输入脚本名称，脚本名称以</span><strong id="oracle_gud_0131__oracle_gud_0029_b6561111433814">.bat</strong><span>结尾。确保脚本已存放在</span><span>ProtectAgent安装目录下</span><span>的</span><span class="filepath" id="oracle_gud_0131__oracle_gud_0029_filepath178625093713">“DataBackup\ProtectClient\ProtectClient-E\bin\thirdparty”</span><span>目录中，且登录数据库主机的用户（默认为</span><strong id="oracle_gud_0131__oracle_gud_0029_b46223114018">Administrator</strong><span>）对该脚本有可执行权限。如果无执行权限请依次执行以下操作授权：</span><p id="oracle_gud_0131__oracle_gud_0029_p13417214119">选择该脚本，单击鼠标右键，选择“属性 &gt; 安全”，在弹出的对话框中选择<strong id="oracle_gud_0131__oracle_gud_0029_b234413843619">Administrator</strong>用户，单击“编辑”更改权限。</p></li>
               </ul>
               <div class="note">
                <span class="notetitle"> 说明： </span>
                <div class="notebody">
                 <p>当配置了<span class="uicontrol">“<span>恢复成功执行脚本</span>”</span>时，即使该脚本执行失败，管理界面上也会显示恢复任务的状态为<span class="uicontrol">“<span>成功</span>”</span>。请您留意任务详情中是否有后置脚本执行失败的相关提示，如有请及时修正脚本。</p>
                </div>
               </div></td>
             </tr>
             <tr>
              <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.7.2.1.3.1.5.2.2.3.1.1 "><p><span>恢复成功执行脚本</span></p></td>
             </tr>
             <tr>
              <td class="cellrowborder" valign="top" headers="mcps1.3.4.2.7.2.1.3.1.5.2.2.3.1.1 "><p><span>恢复失败执行脚本</span></p></td>
             </tr>
             <tr>
              <td class="cellrowborder" valign="top" width="32.86%" headers="mcps1.3.4.2.7.2.1.3.1.5.2.2.3.1.1 "><p>内存资源</p></td>
              <td class="cellrowborder" valign="top" width="67.14%" headers="mcps1.3.4.2.7.2.1.3.1.5.2.2.3.1.2 "><p>执行恢复任务时创建辅助数据库实例所占用的内存资源，建议填写该数据库主机剩余内存资源的70%。</p> <p>默认值为1GB。</p></td>
             </tr>
            </tbody>
           </table>
          </div></li>
         <li>单击<span class="uicontrol">“<span>确定</span>”</span>。
          <div class="note">
           <img src="public_sys-resources/note_3.0-zh-cn.png"><span class="notetitle"> </span>
           <div class="notebody">
            <p>当恢复任务详情中存在“恢复任务可能存在资源残留”内容时，可参见<a href="zh-cn_topic_0000002053451505.html">恢复任务提示存在资源残留（适用于1.6.0及后续版本）</a>进行操作。</p>
           </div>
          </div></li>
        </ol></li>
      </ol> <p></p></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>父主题：</strong> <a href="oracle_gud_0055.html">恢复</a>
    </div>
   </div>
  </div>
 </body>
</html>