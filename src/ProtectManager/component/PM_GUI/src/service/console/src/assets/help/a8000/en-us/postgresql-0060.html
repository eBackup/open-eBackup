<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Managing PostgreSQL Database Clusters">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="postgresql-0057.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="postgresql-0060">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Managing PostgreSQL Database Clusters</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="postgresql-0060"></a><a name="postgresql-0060"></a>

<h1 class="topictitle1">Managing PostgreSQL Database Clusters</h1>
<div><p>The <span>OceanProtect</span> allows you to perform operations, such as deletion and resource authorization or reclaiming, on the cluster where the database resides.</p>
<div class="section"><h4 class="sectiontitle">Related Operations</h4><p>You need to log in to the management page, choose <span class="uicontrol"><b><span id="postgresql-0060__en-us_topic_0000001839142377_text1089610893610"><strong>Protection</strong></span> &gt; Databases &gt; PostgreSQL</b></span>, click the <span class="uicontrol"><b>Cluster</b></span> tab, and locate the desired cluster.</p>
<p><a href="#postgresql-0060__en-us_topic_0000001349267161_en-us_topic_0000001263615462_table24934294919">Table 1</a> describes the related operations.</p>
</div>

<div class="tablenoborder"><a name="postgresql-0060__en-us_topic_0000001349267161_en-us_topic_0000001263615462_table24934294919"></a><a name="en-us_topic_0000001349267161_en-us_topic_0000001263615462_table24934294919"></a><table cellpadding="4" cellspacing="0" summary="" id="postgresql-0060__en-us_topic_0000001349267161_en-us_topic_0000001263615462_table24934294919" frame="border" border="1" rules="all"><caption><b>Table 1 </b>Operations related to a host or cluster</caption><colgroup><col style="width:14.360000000000001%"><col style="width:57.91%"><col style="width:27.73%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="14.360000000000003%" id="mcps1.3.3.2.4.1.1"><p>Operation</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="57.910000000000004%" id="mcps1.3.3.2.4.1.2"><p>Description</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="27.730000000000004%" id="mcps1.3.3.2.4.1.3"><p>Navigation Path</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.3.2.4.1.1 "><p>Deleting a cluster</p>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.3.2.4.1.2 "><p><strong>Scenario</strong> </p>
<p>You can delete a host or cluster if its databases do not require protection. After the host or cluster is deleted, its information disappears from the management page.</p>
<p><strong>Note</strong></p>
<p>An ongoing backup or restore job cannot be deleted. Delete the job after it is completed.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.3.2.4.1.3 "><p>In the row of the target cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Delete</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.3.2.4.1.1 "><p>Modifying a cluster</p>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.3.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to modify the cluster name and node information in the cluster.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.3.2.4.1.3 "><p>In the row of the target cluster, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.3.2.4.1.1 "><div class="p">Authorizing resources<div class="note" id="postgresql-0060__en-us_topic_0000001839223137_note1819152144411"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="postgresql-0060__en-us_topic_0000001839223137_p72172194413">Only version 1.5.0 supports this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.3.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>By default, only the system administrator has the permission to protect resources. If you want to assign this permission to a data protection administrator, authorize the data protection administrator as the system administrator to protect resources.</p>
<p><strong>Note</strong></p>
<ul><li id="postgresql-0060__en-us_topic_0000001839223137_li1491782615119">The protection permissions on a resource that has been associated with an SLA cannot be granted. Before permission granting, remove the protection.</li><li id="postgresql-0060__en-us_topic_0000001839223137_li14274329155112">The protection permissions on a single resource can be granted only to a single data protection administrator.</li></ul>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.3.2.4.1.3 "><p>In the row of the target resource, choose <span class="uicontrol" id="postgresql-0060__en-us_topic_0000001839223137_uicontrol9137201042215"><b><span id="postgresql-0060__en-us_topic_0000001839223137_text15137171062219"><strong>More</strong></span> &gt; <span id="postgresql-0060__en-us_topic_0000001839223137_text12137010162219"><strong>Authorize Resource</strong></span></b></span>. Then, select the desired data protection administrator.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.3.2.4.1.1 "><div class="p">Reclaiming resources<div class="note" id="postgresql-0060__en-us_topic_0000001839223137_note9135337194416"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="postgresql-0060__en-us_topic_0000001839223137_en-us_topic_0000001839223137_p72172194413">Only version 1.5.0 supports this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.3.2.4.1.2 "><p><strong>Scenario</strong></p>
<p>This operation enables you to reclaim resources authorized to the data protection administrator. The data protection administration is not permitted to protect the resources that have been reclaimed.</p>
<p><strong>Note</strong></p>
<p>The protection permissions on a resource that has been associated with an SLA cannot be reclaimed. Before reclamation, remove the protection.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.3.2.4.1.3 "><p>In the row of the target resource, choose <span class="uicontrol" id="postgresql-0060__en-us_topic_0000001839223137_uicontrol37971414213"><b><span id="postgresql-0060__en-us_topic_0000001839223137_text92931039714"><strong>More</strong></span> &gt; <span id="postgresql-0060__en-us_topic_0000001839223137_text118771624064"><strong>Reclaim Resource</strong></span></b></span>.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.3.2.4.1.1 "><div class="p">Adding a tag<div class="note" id="postgresql-0060__en-us_topic_0000001792344090_note161679211561"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="postgresql-0060__en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.3.2.4.1.2 "><p><strong id="postgresql-0060__en-us_topic_0000001792344090_b1495945913127">Scenario</strong></p>
<p>This operation enables you to quickly filter and manage resources.</p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.3.2.4.1.3 "><div class="p">Choose <span class="uicontrol" id="postgresql-0060__en-us_topic_0000001792344090_uicontrol84421111904"><b><span id="postgresql-0060__en-us_topic_0000001792344090_text6442710012"><strong>More</strong></span> &gt; Add Tag</b></span>.<div class="note" id="postgresql-0060__en-us_topic_0000001792344090_note164681316118"><span class="notetitle"> NOTE: </span><div class="notebody"><ul id="postgresql-0060__en-us_topic_0000001792344090_ul22969251338"><li id="postgresql-0060__en-us_topic_0000001792344090_li142961925139">On the displayed <strong id="postgresql-0060__en-us_topic_0000001792344090_b171967427617">Add Tag</strong> dialog box, you can select existing tags or click <strong id="postgresql-0060__en-us_topic_0000001792344090_b1711194810818">Create</strong> to create a tag.</li><li id="postgresql-0060__en-us_topic_0000001792344090_li135161927738">To modify or delete a tag, click <strong id="postgresql-0060__en-us_topic_0000001792344090_b17675195912164">Go to Tag Management</strong> on the <strong id="postgresql-0060__en-us_topic_0000001792344090_b13536183171711">Add Tag</strong> page to go to the <strong id="postgresql-0060__en-us_topic_0000001792344090_b1386161351713">Tag Management</strong> page.</li></ul>
</div></div>
</div>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.360000000000003%" headers="mcps1.3.3.2.4.1.1 "><div class="p">Removing a tag<div class="note" id="postgresql-0060__en-us_topic_0000001792344090_note4957114117575"><span class="notetitle"> NOTE: </span><div class="notebody"><p id="postgresql-0060__en-us_topic_0000001792344090_en-us_topic_0000001792344090_p1816719214566">Only 1.6.0 and later versions support this function.</p>
</div></div>
</div>
</td>
<td class="cellrowborder" valign="top" width="57.910000000000004%" headers="mcps1.3.3.2.4.1.2 "><p><strong id="postgresql-0060__en-us_topic_0000001792344090_b1495945913127_1">Scenario</strong></p>
<p>This operation enables you to remove the tag of a resource when it is no longer needed.</p>
<p></p>
</td>
<td class="cellrowborder" valign="top" width="27.730000000000004%" headers="mcps1.3.3.2.4.1.3 "><p>Choose <span class="uicontrol" id="postgresql-0060__en-us_topic_0000001792344090_uicontrol4993205113111"><b><span id="postgresql-0060__en-us_topic_0000001792344090_text6993751201118"><strong>More</strong></span> &gt; Remove Tag</b></span>.</p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="postgresql-0057.html">PostgreSQL Cluster Environment</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>