<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Configuring the Performance Statistics Switch">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="admin-00135.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="admin-00137">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Configuring the Performance Statistics Switch</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="admin-00137"></a><a name="admin-00137"></a>
  <h1 class="topictitle1">Configuring the Performance Statistics Switch</h1>
  <div>
   <p id="admin-00137__en-us_topic_0000001792505510_p1240682982812"></p>
   <p id="admin-00137__en-us_topic_0000001792505510_p02617141011">This section describes how to configure the performance statistics switch so that you can monitor and know about the device performance more intuitively and easily and make adjustments in a timely manner.</p>
   <div class="section" id="admin-00137__en-us_topic_0000001792505510_section1278520151017">
    <h4 class="sectiontitle">Precautions</h4>
    <ul id="admin-00137__en-us_topic_0000001792505510_ul11375313101">
     <li id="admin-00137__en-us_topic_0000001792505510_li8270734171017">You can view historical data only after performance monitoring is enabled.</li>
     <li id="admin-00137__en-us_topic_0000001792505510_li1313793161010">After performance monitoring is disabled, you can view only the historical data that has been reserved but cannot view the latest historical data.</li>
    </ul>
   </div>
   <div class="section" id="admin-00137__en-us_topic_0000001792505510_section1854653520541">
    <h4 class="sectiontitle">Procedure</h4>
    <ol id="admin-00137__en-us_topic_0000001792505510_ol11430185918359">
     <li id="admin-00137__en-us_topic_0000001792505510_li0198134611381"><span>Choose <span class="uicontrol" id="admin-00137__en-us_topic_0000001792505510_uicontrol9143133018394"><b><span id="admin-00137__en-us_topic_0000001792505510_text69621171585"><strong>Performance</strong></span></b></span>.</span><p></p>
      <div class="note" id="admin-00137__en-us_topic_0000001792505510_note20629519131915">
       <img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span>
       <div class="notebody">
        <p id="admin-00137__en-us_topic_0000001792505510_p26291019121918">For 1.5.0, choose <span class="uicontrol" id="admin-00137__en-us_topic_0000001792505510_uicontrol16272907974160"><b><span id="admin-00137__en-us_topic_0000001792505510_text17320556414160"><strong>Insight</strong></span> &gt; <span id="admin-00137__en-us_topic_0000001792505510_text8126339534160"><strong>Performance</strong></span></b></span>.</p>
       </div>
      </div> <p></p></li>
     <li id="admin-00137__en-us_topic_0000001792505510_li1777103019292"><span>Click <span id="admin-00137__en-us_topic_0000001792505510_text448213159306"><strong>Settings</strong></span>.</span></li>
     <li id="admin-00137__en-us_topic_0000001792505510_li105418279917"><span>Click <span><img id="admin-00137__en-us_topic_0000001792505510_image1439152116613" src="en-us_image_0000001839225017.png"></span> next to <span class="uicontrol" id="admin-00137__en-us_topic_0000001792505510_uicontrol14421168274"><b><span id="admin-00137__en-us_topic_0000001792505510_text1127320131273"><strong>Performance Monitoring</strong></span></b></span> to enable performance monitoring. If performance monitoring is disabled, subsequent monitoring data will not be saved, and the data transmission speed during backup or restoration of some applications cannot be obtained. <a href="#admin-00137__en-us_topic_0000001792505510_table5314421134513">Table 1</a> describes the involved applications.</span><p></p>
      <div class="tablenoborder">
       <a name="admin-00137__en-us_topic_0000001792505510_table5314421134513"></a><a name="en-us_topic_0000001792505510_table5314421134513"></a>
       <table cellpadding="4" cellspacing="0" summary="" id="admin-00137__en-us_topic_0000001792505510_table5314421134513" frame="border" border="1" rules="all">
        <caption>
         <b>Table 1 </b>Applications that depend on performance monitoring for displaying data transmission speed
        </caption>
        <colgroup>
         <col style="width:33.33%">
         <col style="width:33.33%">
         <col style="width:33.33%">
        </colgroup>
        <thead align="left">
         <tr id="admin-00137__en-us_topic_0000001792505510_row11314621194515">
          <th align="left" class="cellrowborder" valign="top" width="33.33333333333333%" id="mcps1.3.4.2.3.2.1.2.4.1.1"><p id="admin-00137__en-us_topic_0000001792505510_p3314162118455">Application</p></th>
          <th align="left" class="cellrowborder" valign="top" width="33.33333333333333%" id="mcps1.3.4.2.3.2.1.2.4.1.2"><p id="admin-00137__en-us_topic_0000001792505510_p031520218459">Backup</p></th>
          <th align="left" class="cellrowborder" valign="top" width="33.33333333333333%" id="mcps1.3.4.2.3.2.1.2.4.1.3"><p id="admin-00137__en-us_topic_0000001792505510_p17315162118456">Restoration</p></th>
         </tr>
        </thead>
        <tbody>
         <tr id="admin-00137__en-us_topic_0000001792505510_row113156218459">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p836134214135">Kubernetes CSI</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p5315921154516">Yes</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p3374534375">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row1631502119456">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p282034913467">ClickHouse</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p163343154716">Yes</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p04501338473">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row6315142164518">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p28203494465">Dameng</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p831514212457">Yes</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p8315192154515">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row1331512113456">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p4820174984619">DB2</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p173159215458">Yes</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p8315132134510">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row2315182154519">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p18208498466">GaussDB</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p7315192110458">No</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p4315162144515">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row031572174513">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p148205496468">GaussDB (DWS)</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p23152021124516">Yes</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p1315421194510">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row15315142114458">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p13820849144618">GaussDB T</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p631510216458">Yes</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p7315421104511">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row1821211250463">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p13820184910467">General database</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p162121625164618">Yes</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p16212132518464">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row6535537164615">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p138201049164613">GoldenDB</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p75351537114617">No</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p45355377466">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row145358373466">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p188201349104612">Informix</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p3535193794616">Yes</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p553573718464">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row2053513373466">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p8820174911467">Kingbase</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p95352037104615">Yes</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p9535123774610">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row10535937174615">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p682074915461">MongoDB</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p853510377461">Yes</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p9535203717464">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row17535103710461">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p9820104914610">MySQL/MariaDB</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p17536337144613">Yes</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p1053673715461">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row1853633719465">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p1682144934613">OceanBase</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p185361737104616">No</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p15361037124612">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row7536137164615">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p28211549104611">openGauss</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p05361371463">Yes</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p10536123715464">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row115361337134615">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p8821164954615">Oracle</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p17536183784615">Yes</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p125361037114618">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row453613716469">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p482154924618">PostgreSQL</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p8536737134611">Yes</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p54941959776">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row7212132504617">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p1482114915466">SQL Server</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p1621282517468">Yes</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p621352516461">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row2213172519464">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p5821184914613">TDSQL</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p7213142594617">No</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p5213152519462">Yes</p></td>
         </tr>
         <tr id="admin-00137__en-us_topic_0000001792505510_row12168122916464">
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.1 "><p id="admin-00137__en-us_topic_0000001792505510_p14821049134618">TiDB</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.2 "><p id="admin-00137__en-us_topic_0000001792505510_p1116892984614">No</p></td>
          <td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.3.4.2.3.2.1.2.4.1.3 "><p id="admin-00137__en-us_topic_0000001792505510_p11168122914466">Yes</p></td>
         </tr>
        </tbody>
       </table>
      </div> <p id="admin-00137__en-us_topic_0000001792505510_p19635171515110">Yes: After performance monitoring is disabled, the data transmission speed during backup or restoration cannot be displayed.</p> <p id="admin-00137__en-us_topic_0000001792505510_p161931938313">No: After performance monitoring is disabled, the data transmission speed during backup or restoration can be displayed.</p> <p></p></li>
    </ol>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="admin-00135.html">Managing Performance Statistics</a>
    </div>
   </div>
  </div>
 </body>
</html>