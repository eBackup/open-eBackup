<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->


<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="DC.Type" content="topic">
<meta name="DC.Title" content="Restoring FusionCompute VMs">
<meta name="product" content="">
<meta name="DC.Relation" scheme="URI" content="fc_gud_0045_0.html">
<meta name="prodname" content="">
<meta name="version" content="">
<meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
<meta name="DC.Publisher" content="20241029">
<meta name="prodname" content="csbs">
<meta name="documenttype" content="usermanual">
<meta name="DC.Format" content="XHTML">
<meta name="DC.Identifier" content="fc_gud_0048_0">
<meta name="DC.Language" content="en-us">
<link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
<title>Restoring FusionCompute VMs</title>
</head>
<body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px"><a name="fc_gud_0048_0"></a><a name="fc_gud_0048_0"></a>

<h1 class="topictitle1">Restoring FusionCompute VMs</h1>
<div><p>This section describes how to restore a VM that has been backed up to the original location or a new location.</p>
<div class="section"><h4 class="sectiontitle">Context</h4><p>The <span>OceanProtect</span> allows you to restore VMs to the original location or a new location using backup copies, archive copies (restoration to the original location is not supported after a replication copy is archived) or replication copies (data cannot be restored to the original location).</p>
</div>
<div class="section"><h4 class="sectiontitle">Procedure</h4><ol><li><span>Choose <span class="uicontrol" id="fc_gud_0048_0__en-us_topic_0000001839142377_uicontrol9143133018394"><b><span id="fc_gud_0048_0__en-us_topic_0000001839142377_text154711237125112"><strong>Explore</strong></span> &gt; <span id="fc_gud_0048_0__en-us_topic_0000001839142377_text3405121755116"><strong>Copy Data</strong></span> &gt; <span id="fc_gud_0048_0__en-us_topic_0000001839142377_text13859104734315"><strong>Virtualization</strong></span> &gt; <span id="fc_gud_0048_0__en-us_topic_0000001839142377_text1825442418308"><strong>FusionCompute</strong></span></b></span>.</span></li><li><span>You can search for a copy by VM or copy. This section uses a VM as an example.</span><p><p>On the <strong>Resources</strong> tab page, search for the VM to be restored by name, and click the VM name.</p>
</p></li><li><span>Click <span class="uicontrol"><b><span><strong>Copy Data</strong></span></b></span> and select the year, month, and day in sequence to locate the copy.</span><p><p>If <span><img src="en-us_image_0000001792547264.png"></span> is displayed below a month or day, a copy is generated in the month or on the day.</p>
</p></li><li><span>In the row of the target copy, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Restore</strong></span></b></span>.</span></li><li><span>Restore a VM to the original location or a new location.</span><p><ul><li>Restore the VM to the original location.<ol type="a"><li>Select <span class="uicontrol"><b><span><strong>Original location</strong></span></b></span> for restoring the VM.</li><li><span class="uicontrol"><b><span><strong>Target Compute Location</strong></span></b></span> is the original location of the VM by default.</li><li>Determine whether to enable <span><strong>Rename VM</strong></span> or <span><strong>Rewrite Original VM</strong></span>.<div class="note"><img src="public_sys-resources/note_3.0-en-us.png"><span class="notetitle"> </span><div class="notebody"><ul><li><span><strong>Rename VM</strong></span> and <span><strong>Rewrite Original VM</strong></span> are mutually exclusive, but you can disable both of them.</li><li>After <span><strong>Rename VM</strong></span> is enabled, enter a new name.</li></ul>
</div></div>
</li><li id="fc_gud_0048_0__li166952038174915">Select an agent host used for restoration using copies.<p id="fc_gud_0048_0__p1569583811493"><a name="fc_gud_0048_0__li166952038174915"></a><a name="li166952038174915"></a>If the agent host is not selected, the system selects one or multiple agent hosts from the agent hosts configured during resource registration to perform restoration.</p>
</li><li>Select whether to automatically start the VM after the restoration.</li><li id="fc_gud_0048_0__li35166416337">Select whether to perform copy verification before restoration.<p id="fc_gud_0048_0__p21611424203315"><a name="fc_gud_0048_0__li35166416337"></a><a name="li35166416337"></a>If this option is enabled, the integrity of a copy is verified before restoration using the copy. Copy verification affects the restoration performance and is disabled by default. If no copy verification file is generated, this option cannot be enabled.</p>
</li><li>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</li></ol>
</li><li>Restore the VM to a new location.<ol type="a"><li>Select <span class="uicontrol"><b><span><strong>New location</strong></span></b></span> for restoring the VM.</li><li>Set information about a new location to which the VM is restored.<p><a href="#fc_gud_0048_0__table1313612152351">Table 1</a> describes the related parameters.</p>

<div class="tablenoborder"><a name="fc_gud_0048_0__table1313612152351"></a><a name="table1313612152351"></a><table cellpadding="4" cellspacing="0" summary="" id="fc_gud_0048_0__table1313612152351" frame="border" border="1" rules="all"><caption><b>Table 1 </b>New location information</caption><colgroup><col style="width:29.87%"><col style="width:70.13000000000001%"></colgroup><thead align="left"><tr><th align="left" class="cellrowborder" valign="top" width="29.87%" id="mcps1.3.3.2.5.2.1.2.1.2.2.2.3.1.1"><p>Parameter</p>
</th>
<th align="left" class="cellrowborder" valign="top" width="70.13000000000001%" id="mcps1.3.3.2.5.2.1.2.1.2.2.2.3.1.2"><p>Description</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="29.87%" headers="mcps1.3.3.2.5.2.1.2.1.2.2.2.3.1.1 "><p><span><strong>Target Compute Location</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="70.13000000000001%" headers="mcps1.3.3.2.5.2.1.2.1.2.2.2.3.1.2 "><p>Compute resources used by the VM after being restored.</p>
<p>Select a cluster or host as the target.</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="29.87%" headers="mcps1.3.3.2.5.2.1.2.1.2.2.2.3.1.1 "><p><span><strong>Storage Location</strong></span></p>
</td>
<td class="cellrowborder" valign="top" width="70.13000000000001%" headers="mcps1.3.3.2.5.2.1.2.1.2.2.2.3.1.2 "><p>Datastore to which the VM configuration files and disk data are restored.</p>
<ul><li><span><strong>Same Datastore</strong></span><p>All disks of a VM are restored to the same datastore.</p>
</li></ul>
<ul><li><span><strong>Different Datastores</strong></span><div class="p"><span><strong>VM Disk Files</strong></span>: datastore where the VM disk data is stored.<div class="note"><span class="notetitle"> NOTE: </span><div class="notebody"><p>To improve data security, you can store the VM configuration files on an independent datastore and disk data on another datastore.</p>
</div></div>
</div>
</li></ul>
</td>
</tr>
</tbody>
</table>
</div>
</li><li>Set whether to <span class="uicontrol"><b><span><strong>Rename VM</strong></span></b></span>. After this function is enabled, you can enter a new name.</li><li>Select an agent host used for restoration using copies.<p id="fc_gud_0048_0__fc_gud_0048_0_p1569583811493">If the agent host is not selected, the system selects one or multiple agent hosts from the agent hosts configured during resource registration to perform restoration.</p>
</li><li>Select whether to automatically start the VM after the restoration.</li><li>Select whether to perform copy verification before restoration.<p id="fc_gud_0048_0__fc_gud_0048_0_p21611424203315">If this option is enabled, the integrity of a copy is verified before restoration using the copy. Copy verification affects the restoration performance and is disabled by default. If no copy verification file is generated, this option cannot be enabled.</p>
</li></ol>
</li></ul>
</p></li><li><span>Click <span class="uicontrol"><b><span><strong>OK</strong></span></b></span>.</span></li></ol>
</div>
</div>
<div>
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a href="fc_gud_0045_0.html">Restoration</a></div>
</div>
</div>

<div class="hrcopyright"><hr size="2"></div><div class="hwcopyright">Copyright &copy; Huawei Technologies Co., Ltd.</div></body>
</html>