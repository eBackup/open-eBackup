<!--
  This file is a part of the open-eBackup project.
  This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
  If a copy of the MPL was not distributed with this file, You can obtain one at
  http://mozilla.org/MPL/2.0/.
  
  Copyright (c) [2024] Huawei Technologies Co.,Ltd.
  
  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-us" xml:lang="en-us">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="DC.Type" content="topic">
  <meta name="DC.Title" content="Managing Clusters/Hosts/VMs/VM Groups">
  <meta name="product" content="">
  <meta name="DC.Relation" scheme="URI" content="fc_gud_0060.html">
  <meta name="prodname" content="">
  <meta name="version" content="">
  <meta name="brand" content="30-OceanProtect Appliance 1.5.0-1.6.0 Help Center">
  <meta name="DC.Publisher" content="20241029">
  <meta name="prodname" content="csbs">
  <meta name="documenttype" content="usermanual">
  <meta name="DC.Format" content="XHTML">
  <meta name="DC.Identifier" content="fc_gud_0063">
  <meta name="DC.Language" content="en-us">
  <link rel="stylesheet" type="text/css" href="public_sys-resources/commonltr.css">
  <title>Managing Clusters/Hosts/VMs/VM Groups</title>
 </head>
 <body style="clear:both; padding-left:10px; padding-top:5px; padding-right:5px; padding-bottom:5px">
  <a name="fc_gud_0063"></a><a name="fc_gud_0063"></a>
  <h1 class="topictitle1">Managing Clusters/Hosts/VMs/VM Groups</h1>
  <div>
   <p>The <span>product</span> allows you to perform operations (such as to modify, remove, activate, or disable protection) for clusters, hosts, VMs, or VM groups. This section uses a VM as an example to describe the related operations. The operations for a cluster, host, or VM group are the same.</p>
   <p>You need to log in to the management page, choose <span class="uicontrol"><b><span id="fc_gud_0063__en-us_topic_0000001839142377_text1833943234612"><strong>Protection</strong></span> &gt; Virtualization &gt; FusionOne Compute</b></span>, and find the target VM.</p>
   <p><a href="#fc_gud_0063__table24934294919">Table 1</a> describes the related operations.</p>
   <div class="tablenoborder">
    <a name="fc_gud_0063__table24934294919"></a><a name="table24934294919"></a>
    <table cellpadding="4" cellspacing="0" summary="" id="fc_gud_0063__table24934294919" frame="border" border="1" rules="all">
     <caption>
      <b>Table 1 </b>Operations related to managing VMs
     </caption>
     <colgroup>
      <col style="width:14.44%">
      <col style="width:57.78%">
      <col style="width:27.779999999999998%">
     </colgroup>
     <thead align="left">
      <tr>
       <th align="left" class="cellrowborder" valign="top" width="14.44%" id="mcps1.3.4.2.4.1.1"><p>Operation</p></th>
       <th align="left" class="cellrowborder" valign="top" width="57.78%" id="mcps1.3.4.2.4.1.2"><p>Description</p></th>
       <th align="left" class="cellrowborder" valign="top" width="27.779999999999998%" id="mcps1.3.4.2.4.1.3"><p>Navigation Path</p></th>
      </tr>
     </thead>
     <tbody>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Modify Protection</strong></span></p></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>You can modify a protection plan of a VM to associate the SLA with the VM again, add or remove a protected VM disk, or modify a ProtectAgent host. After the modification, the next backup will be executed based on the new protection plan.</p> <p><strong>Note</strong></p>
        <ul>
         <li id="fc_gud_0063__hcs_gud_0064_li11841562511">The modification does not affect the protection job that is being executed. The modified protection plan will take effect in the next protection period.</li>
         <li id="fc_gud_0063__hcs_gud_0064_li18622582511">If a backup job has been performed, reconfigure the SLA policy (change to the SLA with copy verification enabled) when modifying the protection plan. After the modification, the initial backup job will be converted to a full backup job if it is a non-full backup job.</li>
         <li id="fc_gud_0063__hcs_gud_0064_li12711557142017">If the SLA parameter associated with the resource changes (from copy verification disabled to copy verification enabled) after a backup job has been performed, when the protection plan for the resource is implemented after the change, the initial backup job will be converted to a full backup job if it is a non-full backup job.</li>
        </ul></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target VM, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Modify Protection</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Remove Protection</strong></span></p></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>You can remove the protection if the VM no longer needs to be protected. After the removal, the system cannot execute protection jobs for the VM. To protect the VM again, you need to associate an SLA with the VM again.</p> <p><strong>Note</strong></p> <p>When the protection job of the VM is running, the protection cannot be removed.</p></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target VM, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Remove Protection</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Activate Protection</strong></span></p></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>If the protection plan of a VM is disabled, you can perform this operation to activate it again. After the protection plan is activated, the system backs up or replicates the VM data based on the protection plan.</p></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target VM, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Activate Protection</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Disable Protection</strong></span></p></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation allows you to disable the protection plan of a VM when periodic protection on the VM is not required. After the protection plan is disabled, the system does not automatically back up or replicate the VM.</p> <p><strong>Note</strong></p> <p>Disabling a protection plan does not affect protection jobs that are running.</p></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target VM, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Disable Protection</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Restoration</strong></span></p></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to restore a VM using a copy.</p></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target VM, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Restoration</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p><span><strong>Manual Backup</strong></span></p></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to perform a backup immediately.</p> <p><strong>Note</strong></p> <p>Copies generated by manual backup are retained for the duration defined in the SLA.</p></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 "><p>In the row of the target VM, choose <span class="uicontrol"><b><span><strong>More</strong></span> &gt; <span><strong>Manual Backup</strong></span></b></span>.</p></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p><strong>Add Tag</strong></p></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to quickly filter and manage resources.</p></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 ">
        <div class="p">
         Choose <span class="uicontrol" id="fc_gud_0063__en-us_topic_0000001792344090_uicontrol84421111904"><b><span id="fc_gud_0063__en-us_topic_0000001792344090_text6442710012"><strong>More</strong></span> &gt; Add Tag</b></span>.
         <div class="note" id="fc_gud_0063__en-us_topic_0000001792344090_note164681316118">
          <span class="notetitle"> NOTE: </span>
          <div class="notebody">
           <ul id="fc_gud_0063__en-us_topic_0000001792344090_ul22969251338">
            <li id="fc_gud_0063__en-us_topic_0000001792344090_li142961925139">On the displayed <strong id="fc_gud_0063__en-us_topic_0000001792344090_b171967427617">Add Tag</strong> dialog box, you can select existing tags or click <strong id="fc_gud_0063__en-us_topic_0000001792344090_b1711194810818">Create</strong> to create a tag.</li>
            <li id="fc_gud_0063__en-us_topic_0000001792344090_li135161927738">To modify or delete a tag, click <strong id="fc_gud_0063__en-us_topic_0000001792344090_b17675195912164">Go to Tag Management</strong> on the <strong id="fc_gud_0063__en-us_topic_0000001792344090_b13536183171711">Add Tag</strong> page to go to the <strong id="fc_gud_0063__en-us_topic_0000001792344090_b1386161351713">Tag Management</strong> page.</li>
           </ul>
          </div>
         </div>
        </div></td>
      </tr>
      <tr>
       <td class="cellrowborder" valign="top" width="14.44%" headers="mcps1.3.4.2.4.1.1 "><p><strong>Remove Tag</strong></p></td>
       <td class="cellrowborder" valign="top" width="57.78%" headers="mcps1.3.4.2.4.1.2 "><p><strong>Scenario</strong></p> <p>This operation enables you to remove the tag of a resource when it is no longer needed.</p></td>
       <td class="cellrowborder" valign="top" width="27.779999999999998%" headers="mcps1.3.4.2.4.1.3 "><p>Choose <span class="uicontrol" id="fc_gud_0063__en-us_topic_0000001792344090_uicontrol4993205113111"><b><span id="fc_gud_0063__en-us_topic_0000001792344090_text6993751201118"><strong>More</strong></span> &gt; Remove Tag</b></span>.</p></td>
      </tr>
     </tbody>
    </table>
   </div>
  </div>
  <div>
   <div class="familylinks">
    <div class="parentlink">
     <strong>Parent topic:</strong> <a href="fc_gud_0060.html">FusionOne Compute Virtualization Environment</a>
    </div>
   </div>
  </div>
 </body>
</html>